<?php
class Pages_model extends MY_Model
{
	public function __construct()
	{
		parent::__construct();
		$this->meta['table'] = 'pages';
		$this->meta['columns']['id'] = 'page_id';
		$this->meta['columns']['deleted'] = 'deleted';
	}
	public function get_information_pages($options = [])
	{
		$this->db->select('link, title');
		$this->db->from($this->meta['table']);
		$this->db->where($this->meta['columns']['deleted'], '0');
		$this->db->where('link !=', 'home');
		$this->db->order_by('sort', 'asc');
		$rowSql = $this->db->get_compiled_select();
		$this->db->reset_query();
		if($rowSql) {
			$query = $this->db->query($rowSql);
		}
		else {
			throw new Exception('Can not complete rowSql operation.');
		}
		if($query->num_rows() == 0) {
			$rows = null;
		}
		else {
			if(isset($options['result_type']) && $options['result_type'] == 'array') {
				$rows = $query->row_array();
			} 
			else {
				$rows = $query->result();
			}
		}
		return $rows;
	}
	public function get_all_pages($options = [])
	{
		$this->db->select('*');
		$this->db->from($this->meta['table']);
		//$this->db->where('link !=', 'home');
		$this->db->order_by('sort', 'asc');
		$rowSql = $this->db->get_compiled_select();
		$this->db->reset_query();
		if($rowSql) {
			$query = $this->db->query($rowSql);
		}
		else {
			throw new Exception('Can not complete rowSql operation.');
		}
		if($query->num_rows() == 0) {
			$rows = null;
		}
		else {
			if(isset($options['result_type']) && $options['result_type'] == 'array') {
				$rows = $query->row_array();
			} 
			else {
				$rows = $query->result();
			}
		}
		return $rows;
	}
}