<?php
class Settings_model extends MY_Model
{
	public function __construct()
	{
		parent::__construct();
		$this->meta['table'] = 'settings';
		$this->meta['columns']['id'] = 'setting_id';
	}
	public function list_all($options = [])
	{
		$query = $this->db->get($this->meta['table']);
		if(isset($options['result_type']) && $options['result_type'] == 'array') {
			$rows = $query->row_array();
		} 
		else {
			$rows = $query->result();
		}
		return $rows;
	}
	public function get_anketa($id)
	{
		$this->db->select('*');
		$this->db->from('users_anketa');
		$this->db->where('deleted', '0');
		$this->db->where('user_id', $id);
		$rowSql = $this->db->get_compiled_select();
		$this->db->reset_query();
		if($rowSql) {
			$query = $this->db->query($rowSql);
		}
		else {
			throw new Exception('Can not complete rowSql operation.');
		}
		if($query->num_rows() == 0) {
			$rows = null;
		}
		else {
			if(isset($options['result_type']) && $options['result_type'] == 'array') {
				$rows = $query->row_array();
			} 
			else {
				$rows = $query->result();
			}
		}
		return $rows[0];
	}
	public function add_anketa($data)
	{
		return $this->db->insert('users_anketa', $data);
	}
}