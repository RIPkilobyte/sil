<?php
class All_Model extends CI_Model
{
	public function __construct()
	{
		parent::__construct();
	}
	public function save_feedback($data)
	{
		$this->db->insert('feedback', $data);
		return $this->db->insert_id();
	}
	public function get_feedback_by_id($id)
	{
		$this->db->select('*');
		$this->db->from('feedback');
		$this->db->where('feedback_id', $id);
		$rowSql = $this->db->get_compiled_select();
		$this->db->reset_query();
		if($rowSql) {
			$query = $this->db->query($rowSql);
		}
		else {
			throw new Exception('Can not complete rowSql operation.');
		}
		if($query->num_rows() == 0) {
			$rows = null;
		}
		else {
			if(isset($options['result_type']) && $options['result_type'] == 'array') {
				$rows = $query->row_array();
			} 
			else {
				$rows = $query->result();
			}
		}
		return $rows[0];
	}
	public function count_moderate_feebacks()
	{
		$rowSql = "SELECT count(*) as count FROM feedback WHERE deleted='0' AND moderated='0' ";
		$this->db->reset_query();
		if($rowSql) {
			$query = $this->db->query($rowSql);
		}
		else {
			throw new Exception('Can not complete rowSql operation.');
		}
		if($query->num_rows() == 0) {
			$row = null;
		}
		else {
			$row = $query->result();
		}
		return $row[0]->count;
	}
	public function get_all_feebacks($options = [])
	{
		$this->db->select('*');
		$this->db->from('feedback');
		$rowSql = $this->db->get_compiled_select();
		$this->db->reset_query();
		if($rowSql) {
			$query = $this->db->query($rowSql);
		}
		else {
			throw new Exception('Can not complete rowSql operation.');
		}
		if($query->num_rows() == 0) {
			$rows = null;
		}
		else {
			if(isset($options['result_type']) && $options['result_type'] == 'array') {
				$rows = $query->row_array();
			} 
			else {
				$rows = $query->result();
			}
		}
		return $rows;
	}
	public function get_moderate_feebacks($options = [])
	{
		$this->db->select('*');
		$this->db->from('feedback');
		$this->db->where('moderated', 0);
		$this->db->where('deleted', 0);
		$rowSql = $this->db->get_compiled_select();
		$this->db->reset_query();
		if($rowSql) {
			$query = $this->db->query($rowSql);
		}
		else {
			throw new Exception('Can not complete rowSql operation.');
		}
		if($query->num_rows() == 0) {
			$rows = null;
		}
		else {
			if(isset($options['result_type']) && $options['result_type'] == 'array') {
				$rows = $query->row_array();
			} 
			else {
				$rows = $query->result();
			}
		}
		return $rows;
	}
	public function count_users_on_group($id)
	{
		$rowSql = "SELECT count(*) as count FROM users_groups WHERE group_id = ".$id;
		$this->db->reset_query();
		if($rowSql) {
			$query = $this->db->query($rowSql);
		}
		else {
			throw new Exception('Can not complete rowSql operation.');
		}
		if($query->num_rows() == 0) {
			$row = null;
		}
		else {
			$row = $query->result();
		}
		return $row[0]->count;
	}
	public function count_active_users()
	{
		$rowSql = "SELECT count(*) as count FROM users WHERE active=1";
		$this->db->reset_query();
		if($rowSql) {
			$query = $this->db->query($rowSql);
		}
		else {
			throw new Exception('Can not complete rowSql operation.');
		}
		if($query->num_rows() == 0) {
			$row = null;
		}
		else {
			$row = $query->result();
		}
		return $row[0]->count;
	}
	public function delete_feedback($id)
	{
		$query = $this->db->query("UPDATE feedback SET deleted = '1' WHERE feedback_id = ".$id);
	}
	public function undelete_feedback($id)
	{
		$query = $this->db->query("UPDATE feedback SET deleted = '0' WHERE feedback_id = ".$id);
	}
	public function update_feedback($id, $data)
	{
		$this->db->where('feedback_id', $id);
		$this->db->update('feedback', $data);
		return $this->db->affected_rows();
	}
	public function count_all_users()
	{
		$rowSql = "SELECT count(*) as count FROM users ";
		$this->db->reset_query();
		if($rowSql) {
			$query = $this->db->query($rowSql);
		}
		else {
			throw new Exception('Can not complete rowSql operation.');
		}
		if($query->num_rows() == 0) {
			$row = null;
		}
		else {
			$row = $query->result();
		}
		return $row[0]->count;
	}
	public function count_all_categories()
	{
		$rowSql = "SELECT count(*) as count FROM blog_category WHERE deleted='0'";
		$this->db->reset_query();
		if($rowSql) {
			$query = $this->db->query($rowSql);
		}
		else {
			throw new Exception('Can not complete rowSql operation.');
		}
		if($query->num_rows() == 0) {
			$row = null;
		}
		else {
			$row = $query->result();
		}
		return $row[0]->count;
	}
	public function count_all_solutions()
	{
		$rowSql = "SELECT count(*) as count FROM blog WHERE type='solution' AND deleted='0'";
		$this->db->reset_query();
		if($rowSql) {
			$query = $this->db->query($rowSql);
		}
		else {
			throw new Exception('Can not complete rowSql operation.');
		}
		if($query->num_rows() == 0) {
			$row = null;
		}
		else {
			$row = $query->result();
		}
		return $row[0]->count;
	}
	public function count_all_techfail()
	{
		$rowSql = "SELECT count(*) as count FROM blog WHERE type='techfail' AND deleted='0'";
		$this->db->reset_query();
		if($rowSql) {
			$query = $this->db->query($rowSql);
		}
		else {
			throw new Exception('Can not complete rowSql operation.');
		}
		if($query->num_rows() == 0) {
			$row = null;
		}
		else {
			$row = $query->result();
		}
		return $row[0]->count;
	}
	public function count_all_comments()
	{
		$rowSql = "SELECT count(*) as count FROM blog_comments WHERE deleted='0'";
		$this->db->reset_query();
		if($rowSql) {
			$query = $this->db->query($rowSql);
		}
		else {
			throw new Exception('Can not complete rowSql operation.');
		}
		if($query->num_rows() == 0) {
			$row = null;
		}
		else {
			$row = $query->result();
		}
		return $row[0]->count;
	}
	public function count_all_voting()
	{
		$rowSql = "SELECT count(*) as count FROM blog_category_voting WHERE deleted='0'";
		$this->db->reset_query();
		if($rowSql) {
			$query = $this->db->query($rowSql);
		}
		else {
			throw new Exception('Can not complete rowSql operation.');
		}
		if($query->num_rows() == 0) {
			$row = null;
		}
		else {
			$row = $query->result();
		}
		return $row[0]->count;
	}
	public function count_all_feedbacks()
	{
		$rowSql = "SELECT count(*) as count FROM feedback WHERE deleted='0'";
		$this->db->reset_query();
		if($rowSql) {
			$query = $this->db->query($rowSql);
		}
		else {
			throw new Exception('Can not complete rowSql operation.');
		}
		if($query->num_rows() == 0) {
			$row = null;
		}
		else {
			$row = $query->result();
		}
		return $row[0]->count;
	}
}