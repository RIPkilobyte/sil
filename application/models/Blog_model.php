<?php
class Blog_model extends MY_Model
{
	public function __construct()
	{
		parent::__construct();
		$this->meta['table'] = 'blog';
		$this->meta['table_category'] = 'blog_category';
		$this->meta['table_relation'] = 'blog_to_category';
		$this->meta['columns']['id'] = 'blog_id';
		$this->meta['columns']['category_id'] = 'category_id';
		$this->meta['columns']['deleted'] = 'deleted';
	}
	public function get_all_solutions($options = [])
	{
		$this->db->select('*');
		$this->db->where('type', 'solution');
		$this->db->from('blog');
		$rowSql = $this->db->get_compiled_select();
		$this->db->reset_query();
		if($rowSql) {
			$query = $this->db->query($rowSql);
		}
		else {
			throw new Exception('Can not complete rowSql operation.');
		}
		if($query->num_rows() == 0) {
			$rows = null;
		}
		else {
			if(isset($options['result_type']) && $options['result_type'] == 'array') {
				$rows = $query->row_array();
			} 
			else {
				$rows = $query->result();
			}
		}
		return $rows;
	}
	public function get_all_techfail($options = [])
	{
		$this->db->select('*');
		$this->db->where('type', 'techfail');
		$this->db->from('blog');
		$rowSql = $this->db->get_compiled_select();
		$this->db->reset_query();
		if($rowSql) {
			$query = $this->db->query($rowSql);
		}
		else {
			throw new Exception('Can not complete rowSql operation.');
		}
		if($query->num_rows() == 0) {
			$rows = null;
		}
		else {
			if(isset($options['result_type']) && $options['result_type'] == 'array') {
				$rows = $query->row_array();
			} 
			else {
				$rows = $query->result();
			}
		}
		return $rows;
	}
	public function get_solutions_title($options = [])
	{
		$this->db->select('blog_id, title');
		$this->db->where('type', 'solution');
		$this->db->from('blog');
		$this->db->where('deleted', '0');
		$rowSql = $this->db->get_compiled_select();
		$this->db->reset_query();
		if($rowSql) {
			$query = $this->db->query($rowSql);
		}
		else {
			throw new Exception('Can not complete rowSql operation.');
		}
		if($query->num_rows() == 0) {
			$rows = null;
		}
		else {
			if(isset($options['result_type']) && $options['result_type'] == 'array') {
				$rows = $query->row_array();
			} 
			else {
				$rows = $query->result();
			}
		}
		return $rows;
	}
	public function get_moderate_solutions($options = [])
	{
		$this->db->select('*');
		$this->db->where('type', 'solution');
		$this->db->from('blog');
		$this->db->where('moderated', '0');
		$this->db->where('deleted', '0');
		$rowSql = $this->db->get_compiled_select();
		$this->db->reset_query();
		if($rowSql) {
			$query = $this->db->query($rowSql);
		}
		else {
			throw new Exception('Can not complete rowSql operation.');
		}
		if($query->num_rows() == 0) {
			$rows = null;
		}
		else {
			if(isset($options['result_type']) && $options['result_type'] == 'array') {
				$rows = $query->row_array();
			} 
			else {
				$rows = $query->result();
			}
		}
		return $rows;
	}
	public function get_moderate_techfail($options = [])
	{
		$this->db->select('*');
		$this->db->where('type', 'techfail');
		$this->db->from('blog');
		$this->db->where('moderated', '0');
		$this->db->where('deleted', '0');
		$rowSql = $this->db->get_compiled_select();
		$this->db->reset_query();
		if($rowSql) {
			$query = $this->db->query($rowSql);
		}
		else {
			throw new Exception('Can not complete rowSql operation.');
		}
		if($query->num_rows() == 0) {
			$rows = null;
		}
		else {
			if(isset($options['result_type']) && $options['result_type'] == 'array') {
				$rows = $query->row_array();
			} 
			else {
				$rows = $query->result();
			}
		}
		return $rows;
	}
	public function get_moderate_comments()
	{
		$this->db->select('*');
		$this->db->from('blog_comments');
		$this->db->where('moderated', '0');
		$this->db->where('deleted', '0');
		$rowSql = $this->db->get_compiled_select();
		$this->db->reset_query();
		if($rowSql) {
			$query = $this->db->query($rowSql);
		}
		else {
			throw new Exception('Can not complete rowSql operation.');
		}
		if($query->num_rows() == 0) {
			$rows = null;
		}
		else {
			if(isset($options['result_type']) && $options['result_type'] == 'array') {
				$rows = $query->row_array();
			} 
			else {
				$rows = $query->result();
			}
		}
		return $rows;
	}
	public function count_moderate_comments()
	{
		$rowSql = "SELECT count(*) as count FROM blog_comments WHERE moderated = '0' AND deleted='0'";
		$this->db->reset_query();
		if($rowSql) {
			$query = $this->db->query($rowSql);
		}
		else {
			throw new Exception('Can not complete rowSql operation.');
		}
		if($query->num_rows() == 0) {
			$row = null;
		}
		else {
			$row = $query->result();
		}
		return $row[0]->count;
	}
	public function count_moderate_solutions()
	{
		$rowSql = "SELECT count(*) as count FROM blog WHERE moderated = '0' AND deleted='0' AND type='solution' ";
		$this->db->reset_query();
		if($rowSql) {
			$query = $this->db->query($rowSql);
		}
		else {
			throw new Exception('Can not complete rowSql operation.');
		}
		if($query->num_rows() == 0) {
			$row = null;
		}
		else {
			$row = $query->result();
		}
		return $row[0]->count;
	}
	public function count_moderate_techfail()
	{
		$rowSql = "SELECT count(*) as count FROM blog WHERE moderated = '0' AND deleted='0' AND type='techfail' ";
		$this->db->reset_query();
		if($rowSql) {
			$query = $this->db->query($rowSql);
		}
		else {
			throw new Exception('Can not complete rowSql operation.');
		}
		if($query->num_rows() == 0) {
			$row = null;
		}
		else {
			$row = $query->result();
		}
		return $row[0]->count;
	}
	public function get_all_categories_by_blog_id($id)
	{
		$rowSql = "SELECT bc.*, btc.blog_id FROM blog_category bc 
		left join blog_to_category btc ON bc.blog_category_id = btc.category_id
		WHERE btc.blog_id IN(".$id.")";
		$this->db->reset_query();
		if($rowSql) {
			$query = $this->db->query($rowSql);
		}
		else {
			throw new Exception('Can not complete rowSql operation.');
		}
		if($query->num_rows() == 0) {
			$rows = null;
		}
		else {
			if(isset($options['result_type']) && $options['result_type'] == 'array') {
				$rows = $query->row_array();
			} 
			else {
				$rows = $query->result();
			}
		}
		return $rows;
	}
	public function get_all_categories($with_deleted = 0, $options = [])
	{
		$this->db->select('*');
		$this->db->from('blog_category');
		if(!$with_deleted) {
			$this->db->where('deleted', '0');
		}
		$this->db->order_by('sort, title', 'asc');
		$rowSql = $this->db->get_compiled_select();
		$this->db->reset_query();
		if($rowSql) {
			$query = $this->db->query($rowSql);
		}
		else {
			throw new Exception('Can not complete rowSql operation.');
		}
		if($query->num_rows() == 0) {
			$rows = null;
		}
		else {
			if(isset($options['result_type']) && $options['result_type'] == 'array') {
				$rows = $query->row_array();
			} 
			else {
				$rows = $query->result();
			}
		}
		return $rows;
	}
	public function get_categories_with_posts($options = [])
	{
		$this->db->select('*');
		$this->db->from('blog_category');
		$this->db->where('deleted', '0');
		$this->db->where('count >', 0);
		$this->db->order_by('sort, title', 'asc');
		$rowSql = $this->db->get_compiled_select();
		$this->db->reset_query();
		if($rowSql) {
			$query = $this->db->query($rowSql);
		}
		else {
			throw new Exception('Can not complete rowSql operation.');
		}
		if($query->num_rows() == 0) {
			$rows = [];
		}
		else {
			if(isset($options['result_type']) && $options['result_type'] == 'array') {
				$rows = $query->row_array();
			} 
			else {
				$rows = $query->result();
			}
		}
		return $rows;
	}
	public function get_techfails_with_posts($options = [])
	{
		$this->db->select('*');
		$this->db->from('blog_category');
		$this->db->where('deleted', '0');
		$this->db->where('count_tech >', 0);
		$this->db->order_by('sort, title', 'asc');
		$rowSql = $this->db->get_compiled_select();
		$this->db->reset_query();
		if($rowSql) {
			$query = $this->db->query($rowSql);
		}
		else {
			throw new Exception('Can not complete rowSql operation.');
		}
		if($query->num_rows() == 0) {
			$rows = [];
		}
		else {
			if(isset($options['result_type']) && $options['result_type'] == 'array') {
				$rows = $query->row_array();
			} 
			else {
				$rows = $query->result();
			}
		}
		return $rows;
	}
	public function get_categories_with_post_by_parent($blog_category_id, $options = [])
	{
		$this->db->select('*');
		$this->db->from('blog_category');
		$this->db->where('deleted', '0');
		$this->db->where('parent_id', $blog_category_id);
		$this->db->where('count >', '0');
		$this->db->order_by('sort, title', 'asc');
		$rowSql = $this->db->get_compiled_select();
		$this->db->reset_query();
		if($rowSql) {
			$query = $this->db->query($rowSql);
		}
		else {
			throw new Exception('Can not complete rowSql operation.');
		}
		if($query->num_rows() == 0) {
			$rows = [];
		}
		else {
			if(isset($options['result_type']) && $options['result_type'] == 'array') {
				$rows = $query->row_array();
			} 
			else {
				$rows = $query->result();
			}
		}
		return $rows;
	}
	public function add_blog_to_category($blog_id, $category_id, $options = [])
	{
		$data = [
			'blog_id' => $blog_id,
			'category_id' => $category_id
		];
		$this->db->insert('blog_to_category', $data);
		return $this->db->insert_id();
	}
	public function remove_blog_from_category($blog_id, $category_id, $options = [])
	{
		$data = [
			'blog_id' => $blog_id,
			'category_id' => $category_id
		];
		$this->db->delete('blog_to_category', $data);
	}
	public function increase_count_category($category_id)
	{
		$query = $this->db->query("UPDATE blog_category SET count = count + 1 WHERE blog_category_id = ".$category_id);
	}
	public function increase_count_tech_category($category_id)
	{
		$query = $this->db->query("UPDATE blog_category SET count_tech = count_tech + 1 WHERE blog_category_id = ".$category_id);
	}
	public function reduce_count_category($category_id)
	{
		$query = $this->db->query("UPDATE blog_category SET count = count - 1 WHERE blog_category_id = ".$category_id);
	}
	public function reduce_count_tech_category($category_id)
	{
		$query = $this->db->query("UPDATE blog_category SET count_tech = count_tech - 1 WHERE blog_category_id = ".$category_id);
	}
	public function get_category_by_alias($alias, $options = [])
	{
		$this->db->select('*');
		$this->db->from('blog_category');
		$this->db->where('deleted', '0');
		$this->db->where('alias', $alias);
		$this->db->order_by('sort, title', 'asc');
		$rowSql = $this->db->get_compiled_select();
		$this->db->reset_query();
		if($rowSql) {
			$query = $this->db->query($rowSql);
		}
		else {
			throw new Exception('Can not complete rowSql operation.');
		}
		if($query->num_rows() == 0) {
			$rows = null;
		}
		else {
			if(isset($options['result_type']) && $options['result_type'] == 'array') {
				$rows = $query->row_array();
			} 
			else {
				$rows = $query->result();
			}
		}
		return $rows;
	}
	public function get_category_by_id($id, $with_deleted=0, $options = [])
	{
		$this->db->select('*');
		$this->db->from('blog_category');
		if(!$with_deleted) {
			$this->db->where('deleted', '0');
		}
		$this->db->where('blog_category_id', $id);
		$this->db->order_by('sort, title', 'asc');
		$rowSql = $this->db->get_compiled_select();
		$this->db->reset_query();
		if($rowSql) {
			$query = $this->db->query($rowSql);
		}
		else {
			throw new Exception('Can not complete rowSql operation.');
		}
		if($query->num_rows() == 0) {
			$rows = null;
		}
		else {
			if(isset($options['result_type']) && $options['result_type'] == 'array') {
				$rows = $query->row_array();
			} 
			else {
				$rows = $query->result();
			}
		}
		return $rows[0];
	}
	public function delete_category($id)
	{
		$query = $this->db->query("UPDATE blog_category SET deleted = '1' WHERE blog_category_id = ".$id);
	}
	public function undelete_category($id)
	{
		$query = $this->db->query("UPDATE blog_category SET deleted = '0' WHERE blog_category_id = ".$id);
	}
	public function update_category($id, $data)
	{
		$this->db->where('blog_category_id', $id);
		$this->db->update('blog_category', $data);
		return $this->db->affected_rows();
	}
	public function add_category($data)
	{
		$this->db->insert('blog_category', $data);
		return $this->db->insert_id();
	}
	public function get_categories_by_post_id($id, $options = [])
	{
		$this->db->select('*');
		$this->db->from('blog_to_category');
		$this->db->where('blog_id', $id);
		$rowSql = $this->db->get_compiled_select();
		$this->db->reset_query();
		if($rowSql) {
			$query = $this->db->query($rowSql);
		}
		else {
			throw new Exception('Can not complete rowSql operation.');
		}
		if($query->num_rows() == 0) {
			$rows = null;
		}
		else {
			if(isset($options['result_type']) && $options['result_type'] == 'array') {
				$rows = $query->row_array();
			} 
			else {
				$rows = $query->result();
			}
		}
		return $rows;
	}
	public function get_categories_by_parent($id, $options = [])
	{
		$this->db->select('*');
		$this->db->from('blog_category');
		$this->db->where('deleted', '0');
		$this->db->where('parent_id', $id);
		$this->db->order_by('sort, title', 'asc');
		$rowSql = $this->db->get_compiled_select();
		$this->db->reset_query();
		if($rowSql) {
			$query = $this->db->query($rowSql);
		}
		else {
			throw new Exception('Can not complete rowSql operation.');
		}
		if($query->num_rows() == 0) {
			$rows = null;
		}
		else {
			if(isset($options['result_type']) && $options['result_type'] == 'array') {
				$rows = $query->row_array();
			} 
			else {
				$rows = $query->result();
			}
		}
		return $rows;
	}
	public function get_solution_blogs_by_category_id($id, $options = [])
	{
		$rowSql = "SELECT btc.* FROM blog_to_category btc
		LEFT JOIN blog b ON b.blog_id = btc.blog_id
		WHERE btc.category_id='".$id."' AND b.type='solution'
		";
		$this->db->reset_query();
		if($rowSql) {
			$query = $this->db->query($rowSql);
		}
		else {
			throw new Exception('Can not complete rowSql operation.');
		}
		if($query->num_rows() == 0) {
			$rows = null;
		}
		else {
			if(isset($options['result_type']) && $options['result_type'] == 'array') {
				$rows = $query->row_array();
			} 
			else {
				$rows = $query->result();
			}
		}
		return $rows;
	}
	public function get_techfail_blogs_by_category_id($id, $options = [])
	{
		$rowSql = "SELECT btc.* FROM blog_to_category btc
		LEFT JOIN blog b ON b.blog_id = btc.blog_id
		WHERE btc.category_id='".$id."' AND b.type='techfail'
		";
		$this->db->reset_query();
		if($rowSql) {
			$query = $this->db->query($rowSql);
		}
		else {
			throw new Exception('Can not complete rowSql operation.');
		}
		if($query->num_rows() == 0) {
			$rows = null;
		}
		else {
			if(isset($options['result_type']) && $options['result_type'] == 'array') {
				$rows = $query->row_array();
			} 
			else {
				$rows = $query->result();
			}
		}
		return $rows;
	}
	public function get_blog_posts_by_ids($id, $options = [])
	{
		$rowSql = "SELECT * FROM blog WHERE blog_id IN(".$id.") and deleted=0";
		$this->db->reset_query();
		if($rowSql) {
			$query = $this->db->query($rowSql);
		}
		else {
			throw new Exception('Can not complete rowSql operation.');
		}
		if($query->num_rows() == 0) {
			$rows = null;
		}
		else {
			if(isset($options['result_type']) && $options['result_type'] == 'array') {
				$rows = $query->row_array();
			} 
			else {
				$rows = $query->result();
			}
		}
		return $rows;
	}
	public function count_blog_comments_by_id($id)
	{
		$rowSql = "SELECT count(*) as count FROM blog_comments WHERE blog_id = ".$id." and deleted=0";
		$this->db->reset_query();
		if($rowSql) {
			$query = $this->db->query($rowSql);
		}
		else {
			throw new Exception('Can not complete rowSql operation.');
		}
		if($query->num_rows() == 0) {
			$row = null;
		}
		else {
			$row = $query->result();
		}
		return $row[0]->count;
	}
	public function add_comment($data)
	{
		$this->db->insert('blog_comments', $data);
		return $this->db->insert_id();
	}
	public function get_all_comments()
	{
		$this->db->select('*');
		$this->db->from('blog_comments');
		$rowSql = $this->db->get_compiled_select();
		$this->db->reset_query();
		if($rowSql) {
			$query = $this->db->query($rowSql);
		}
		else {
			throw new Exception('Can not complete rowSql operation.');
		}
		if($query->num_rows() == 0) {
			$rows = null;
		}
		else {
			if(isset($options['result_type']) && $options['result_type'] == 'array') {
				$rows = $query->row_array();
			} 
			else {
				$rows = $query->result();
			}
		}
		return $rows;
	}
	public function delete_comment($id)
	{
		$query = $this->db->query("UPDATE blog_comments SET deleted = '1' WHERE blog_comment_id = ".$id);
	}
	public function undelete_comment($id)
	{
		$query = $this->db->query("UPDATE blog_comments SET deleted = '0' WHERE blog_comment_id = ".$id);
	}
	public function get_blog_comments_by_id($id)
	{
		$this->db->select('*');
		$this->db->from('blog_comments');
		$this->db->where('blog_id', $id);
		//$this->db->where('deleted', '0');
		$rowSql = $this->db->get_compiled_select();
		$this->db->reset_query();
		if($rowSql) {
			$query = $this->db->query($rowSql);
		}
		else {
			throw new Exception('Can not complete rowSql operation.');
		}
		if($query->num_rows() == 0) {
			$rows = null;
		}
		else {
			if(isset($options['result_type']) && $options['result_type'] == 'array') {
				$rows = $query->row_array();
			} 
			else {
				$rows = $query->result();
			}
		}
		return $rows;
	}
	public function count_blogs_by_user_id($id)
	{
		$rowSql = "SELECT count(*) as count FROM blog WHERE created_id = ".$id." and deleted=0";
		$this->db->reset_query();
		if($rowSql) {
			$query = $this->db->query($rowSql);
		}
		else {
			throw new Exception('Can not complete rowSql operation.');
		}
		if($query->num_rows() == 0) {
			$row = null;
		}
		else {
			$row = $query->result();
		}
		return $row[0]->count;
	}
	public function get_blogs_by_user_id($id, $with_deleted=0)
	{
		$rowSql = "SELECT * FROM blog WHERE created_id = ".$id;
		if(!$with_deleted) {
			$rowSql .= " and deleted=0";
		}
		$this->db->reset_query();
		if($rowSql) {
			$query = $this->db->query($rowSql);
		}
		else {
			throw new Exception('Can not complete rowSql operation.');
		}
		if($query->num_rows() == 0) {
			$rows = null;
		}
		else {
			$rows = $query->result();
		}
		return $rows;
	}
	public function count_blog_comments_by_user_id($id)
	{
		$rowSql = "SELECT count(*) as count FROM blog_comments WHERE created_id = ".$id." and deleted=0";
		$this->db->reset_query();
		if($rowSql) {
			$query = $this->db->query($rowSql);
		}
		else {
			throw new Exception('Can not complete rowSql operation.');
		}
		if($query->num_rows() == 0) {
			$row = null;
		}
		else {
			$row = $query->result();
		}
		return $row[0]->count;
	}
	public function get_all_votings()
	{
		$this->db->select('*');
		$this->db->from('blog_category_voting');
		$this->db->where('deleted', '0');
		$this->db->where('votes < max_votes');
		$rowSql = $this->db->get_compiled_select();
		$this->db->reset_query();
		if($rowSql) {
			$query = $this->db->query($rowSql);
		}
		else {
			throw new Exception('Can not complete rowSql operation.');
		}
		if($query->num_rows() == 0) {
			$rows = null;
		}
		else {
			if(isset($options['result_type']) && $options['result_type'] == 'array') {
				$rows = $query->row_array();
			} 
			else {
				$rows = $query->result();
			}
		}
		return $rows;
	}
	public function count_scored_votings()
	{
		$rowSql = "SELECT count(*) as count FROM blog_category_voting WHERE votes = max_votes AND deleted='0' ";
		$this->db->reset_query();
		if($rowSql) {
			$query = $this->db->query($rowSql);
		}
		else {
			throw new Exception('Can not complete rowSql operation.');
		}
		if($query->num_rows() == 0) {
			$row = null;
		}
		else {
			$row = $query->result();
		}
		return $row[0]->count;
	}
	public function get_scored_votings()
	{
		$this->db->select('*');
		$this->db->from('blog_category_voting');
		$this->db->where('votes = max_votes');
		$this->db->or_where('votes > max_votes');
		$rowSql = $this->db->get_compiled_select();
		$this->db->reset_query();
		if($rowSql) {
			$query = $this->db->query($rowSql);
		}
		else {
			throw new Exception('Can not complete rowSql operation.');
		}
		if($query->num_rows() == 0) {
			$rows = null;
		}
		else {
			if(isset($options['result_type']) && $options['result_type'] == 'array') {
				$rows = $query->row_array();
			} 
			else {
				$rows = $query->result();
			}
		}
		return $rows;
	}
	public function get_not_scored_votings()
	{
		$this->db->select('*');
		$this->db->from('blog_category_voting');
		$this->db->where('votes < max_votes');
		$rowSql = $this->db->get_compiled_select();
		$this->db->reset_query();
		if($rowSql) {
			$query = $this->db->query($rowSql);
		}
		else {
			throw new Exception('Can not complete rowSql operation.');
		}
		if($query->num_rows() == 0) {
			$rows = null;
		}
		else {
			if(isset($options['result_type']) && $options['result_type'] == 'array') {
				$rows = $query->row_array();
			} 
			else {
				$rows = $query->result();
			}
		}
		return $rows;
	}
	public function add_voting_category($data)
	{
		$this->db->insert('blog_category_voting', $data);
		return $this->db->insert_id();
	}
	public function vote($id, $user_id)
	{
		$this->db->query("UPDATE blog_category_voting SET votes = votes + 1 WHERE voting_id = ".$id);
		$this->db->insert('blog_category_voting_users', ['user_id'=>$user_id, 'category_id'=>$id]);
		return $this->db->insert_id();
	}
	public function get_voting_by_id($id)
	{
		$this->db->select('*');
		$this->db->from('blog_category_voting');
		$this->db->where('voting_id', $id);
		$rowSql = $this->db->get_compiled_select();
		$this->db->reset_query();
		if($rowSql) {
			$query = $this->db->query($rowSql);
		}
		else {
			throw new Exception('Can not complete rowSql operation.');
		}
		if($query->num_rows() == 0) {
			$rows = null;
		}
		else {
			if(isset($options['result_type']) && $options['result_type'] == 'array') {
				$rows = $query->row_array();
			} 
			else {
				$rows = $query->result();
			}
		}
		return $rows[0];
	}
	public function update_voting($id, $data)
	{
		$this->db->where('voting_id', $id);
		$this->db->update('blog_category_voting', $data);
		return $this->db->affected_rows();
	}
	public function real_delete_voting($id)
	{
		$this->db->delete('blog_category_voting', array('voting_id' => $id));
	}
	public function delete_voting($id)
	{
		$query = $this->db->query("UPDATE blog_category_voting SET deleted = '1' WHERE voting_id = ".$id);
	}
	public function undelete_voting($id)
	{
		$query = $this->db->query("UPDATE blog_category_voting SET deleted = '0' WHERE voting_id = ".$id);
	}
	public function check_vote($user_id, $category_id)
	{
		$this->db->select('*');
		$this->db->from('blog_category_voting_users');
		$this->db->where('user_id', $user_id);
		$this->db->where('category_id', $category_id);
		$rowSql = $this->db->get_compiled_select();
		$this->db->reset_query();
		if($rowSql) {
			$query = $this->db->query($rowSql);
		}
		else {
			throw new Exception('Can not complete rowSql operation.');
		}
		if($query->num_rows() == 0) {
			return false;
		}
		else {
			return true;
		}
		return true;
	}
	public function get_comment($id, $with_deleted=0)
	{
		$this->db->select('*');
		$this->db->from('blog_comments');
		$this->db->where('blog_comment_id', $id);
		if(!$with_deleted) {
			$this->db->where('deleted', '0');
		}
		$rowSql = $this->db->get_compiled_select();
		$this->db->reset_query();
		if($rowSql) {
			$query = $this->db->query($rowSql);
		}
		else {
			throw new Exception('Can not complete rowSql operation.');
		}
		if($query->num_rows() == 0) {
			$rows = null;
		}
		else {
			if(isset($options['result_type']) && $options['result_type'] == 'array') {
				$rows = $query->row_array();
			} 
			else {
				$rows = $query->result();
			}
		}
		return $rows[0];
	}
	public function update_comment($id, $data)
	{
		$this->db->where('blog_comment_id', $id);
		$this->db->update('blog_comments', $data);
		return $this->db->affected_rows();
	}
	public function get_file($id)
	{
		$this->db->select('*');
		$this->db->from('attached_files');
		$this->db->where('attached_file_id', $id);
		$rowSql = $this->db->get_compiled_select();
		$this->db->reset_query();
		if($rowSql) {
			$query = $this->db->query($rowSql);
		}
		else {
			throw new Exception('Can not complete rowSql operation.');
		}
		if($query->num_rows() == 0) {
			$rows = null;
		}
		else {
			if(isset($options['result_type']) && $options['result_type'] == 'array') {
				$rows = $query->row_array();
			} 
			else {
				$rows = $query->result();
			}
		}
		return $rows[0];
	}
	public function get_avatar($id)
	{
		$this->db->select('*');
		$this->db->from('attached_files');
		$this->db->where('attached_file_id', $id);
		$this->db->where('deleted = 0');
		$rowSql = $this->db->get_compiled_select();
		$this->db->reset_query();
		if($rowSql) {
			$query = $this->db->query($rowSql);
		}
		else {
			throw new Exception('Can not complete rowSql operation.');
		}
		if($query->num_rows() == 0) {
			$rows = null;
		}
		else {
			if(isset($options['result_type']) && $options['result_type'] == 'array') {
				$rows = $query->row_array();
			} 
			else {
				$rows = $query->result();
			}
		}
		return $rows[0];
	}
	public function save_file($data)
	{
		$this->db->insert('attached_files', $data);
		return $this->db->insert_id();
	}
	public function update_file($id, $data)
	{
		$this->db->where('attached_file_id', $id);
		$this->db->update('attached_files', $data);
		return $this->db->affected_rows();
	}
	public function get_files_by_entity($id, $entity = 'blog')
	{
		$this->db->select('*');
		$this->db->where('entity', $entity);
		$this->db->where('entity_id', $id);
		$this->db->from('attached_files');
		$this->db->where('deleted', '0');
		$rowSql = $this->db->get_compiled_select();
		$this->db->reset_query();
		if($rowSql) {
			$query = $this->db->query($rowSql);
		}
		else {
			throw new Exception('Can not complete rowSql operation.');
		}
		if($query->num_rows() == 0) {
			$rows = null;
		}
		else {
			if(isset($options['result_type']) && $options['result_type'] == 'array') {
				$rows = $query->row_array();
			} 
			else {
				$rows = $query->result();
			}
		}
		return $rows;
	}
	public function get_comments_files($id)
	{
		$this->db->select('*');
		$this->db->where('entity', 'comment');
		$this->db->where('entity_id', $id);
		$this->db->from('attached_files');
		$this->db->where('deleted', '0');
		$rowSql = $this->db->get_compiled_select();
		$this->db->reset_query();
		if($rowSql) {
			$query = $this->db->query($rowSql);
		}
		else {
			throw new Exception('Can not complete rowSql operation.');
		}
		if($query->num_rows() == 0) {
			$rows = null;
		}
		else {
			if(isset($options['result_type']) && $options['result_type'] == 'array') {
				$rows = $query->row_array();
			} 
			else {
				$rows = $query->result();
			}
		}
		return $rows;
	}
	public function get_last_blogs($count = 10)
	{
		$this->db->select('*');
		$this->db->from('blog');
		$this->db->where('deleted', '0');
		$this->db->limit($count);
		$this->db->order_by('updated_at', 'desc');
		$rowSql = $this->db->get_compiled_select();
		$this->db->reset_query();
		if($rowSql) {
			$query = $this->db->query($rowSql);
		}
		else {
			throw new Exception('Can not complete rowSql operation.');
		}
		if($query->num_rows() == 0) {
			$rows = null;
		}
		else {
			if(isset($options['result_type']) && $options['result_type'] == 'array') {
				$rows = $query->row_array();
			} 
			else {
				$rows = $query->result();
			}
		}
		return $rows;
	}
	public function get_last_comments($count = 10)
	{
		$this->db->select('*');
		$this->db->from('blog_comments');
		$this->db->where('deleted', '0');
		$this->db->limit($count);
		$this->db->order_by('updated_at', 'desc');
		$rowSql = $this->db->get_compiled_select();
		$this->db->reset_query();
		if($rowSql) {
			$query = $this->db->query($rowSql);
		}
		else {
			throw new Exception('Can not complete rowSql operation.');
		}
		if($query->num_rows() == 0) {
			$rows = null;
		}
		else {
			if(isset($options['result_type']) && $options['result_type'] == 'array') {
				$rows = $query->row_array();
			} 
			else {
				$rows = $query->result();
			}
		}
		return $rows;
	}
}