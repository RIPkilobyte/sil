<script type="text/javascript">
document.addEventListener('DOMContentLoaded', function(){
chart = new AmCharts.AmSerialChart();
var chartData = [{
"Отделение": "1", 
  "СвободныеКойки": 12, 
  "ФактическоеКоличествоКоек": 12
}
,{
"Отделение": "2", 
  "СвободныеКойки": 8, 
  "ФактическоеКоличествоКоек": 12
}];
chart.dataProvider=chartData;
chart.type= "serial";
//chart.theme= "light";
chart.categoryField="Отделение";

var categoryAxis = new AmCharts.AmLegend();
categoryAxis.gridPosition="start";
chart.addLegend(categoryAxis);

var graph2 = new AmCharts.AmGraph();
graph2.fillAlphas=0.3;
graph2.lineAlpha=1;
graph2.type="column";
graph2.lineColor="#2b00ff";
graph2.valueField="ФактическоеКоличествоКоек";
graph2.clustered=false;
graph2.labelText="[[value]]";
graph2.balloonText="Фактическое количество коек: [[value]]";
chart.addGraph(graph2);

var graph1 = new AmCharts.AmGraph();
graph1.fillAlphas=0.3;
graph1.lineAlpha=1;
graph1.type="column";
graph1.lineColor="#ff0000";
graph1.valueField="СвободныеКойки";
graph1.clustered=false;
graph1.labelText="[[value]]";
graph1.balloonText="Свободные койки: [[value]]";
chart.addGraph(graph1);



chart.write("chartdiv");
console.log(chart);
});
</script>
<style type="text/css">
html, body {
  width: 100%;
  height: 100%;
  margin: 0px;
}

#chartdiv {
	width: 100%;
	height: 100%;
}
</style>
<script src="https://www.amcharts.com/lib/3/amcharts.js"></script>
<script src="https://www.amcharts.com/lib/3/serial.js"></script>
<div id="chartdiv"></div>