<div class="seif">
	<a href="/categories"><img title="Показать все темы" src="/assets/images/seif.png"/></a>
</div>
<div class="carusel" id="carusel">
	<?php foreach ($categories as $k => $v): ?>
	<div class="carusel-item">
		<div class="carusel-item-blok">
			<a href="/categories/category/<?php echo $v->alias; ?>"><?php echo $v->title; ?></a>
		</div>
	</div>
	<?php endforeach; ?>
</div>
<script>
$('#carusel').owlCarousel({
	loop:true,
	rtl:true,
	margin:5,
	nav:false,
	dots:false,
	items:4,
	smartSpeed:250,
	autoplay:true,
	autoplayTimeout:1800,
	autoplayHoverPause:true,
	responsive:{
		0:{
			items:4
		},
		980:{
			items:4
		},
		1000:{
			items:4
		}
	}
})
</script>