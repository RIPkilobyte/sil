<?php $this->load->view('layout/header'); ?>

<div class="main-kontent">
<?php $this->load->view('layout/information'); ?>

	<div class="main-kontent-center">
		<div class="main-kontent-center-left">
			<div class="main-kontent-center-left-title">Карта сайта</div>
			<div class="main-kontent-center-left-text" style="margin-left: 15px;">
				<ul style="float: left; min-width: 350px;">
					<li><a href="/">Главная страница</a></li>
					<?php foreach($information_pages as $page) {
						echo '<li><a href="/'.$page->link.'">'.$page->title.'</a></li>';
					} ?>
					<li><a href="/feedback">Обратная связь</a></li>
					<li><a href="/sitemap">Карта сайта</a></li>
				</ul>
				<ul style="float: left; min-width: 350px;">
					<li><a href="/cabinet">Личный кабинет</a></li>
					<li><a href="/solutions">Как красиво решено!</a></li>
					<li><a href="/techfail">Техноляп</a></li>
					<li><a href="/voting">Новые темы</a></li>
					<li><a href="/solutions/add">Добавить решение</a></li>
					<li><a href="/techfail/add">Добавить техноляп</a></li>
					<li><a href="/voting/add">Добавить новую тему</a></li>
				</ul>
			</div>
		</div>

<?php $this->load->view('layout/banners'); ?>
		<div class="clearfix"></div>
	</div>
	<div class="clearfix"></div>
<script type="text/javascript">
$().ready(function(){
}); 
</script>
<style type="text/css">
.main-kontent-center-left {
	font-size:180%;
}
</style>
<?php $this->load->view('layout/footer'); ?>