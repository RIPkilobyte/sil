<?php if(isset($information_pages) && $information_pages): ?>
<div class="main-kontent-left">
	<div class="left-kolon">
		<div class="left-kolon-title">Информация</div>
		<div class="left-kolon-text">
			<ul>
				<?php foreach($information_pages as $page) {
					echo '<li><a href="/'.$page->link.'">'.$page->title.'</a></li>';
				} ?>
				<li><a href="/feedback">Обратная связь</a></li>
			</ul>
		</div>
	</div>
</div>
<?php endif; ?>