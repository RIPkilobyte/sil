<?php $this->load->view('layout/header'); ?>

<div class="main-kontent">
<?php $this->load->view('layout/information'); ?>

	<div class="main-kontent-center">
		<div class="main-kontent-center-left">
			<div class="main-kontent-center-left-title">Отправить сообщение</div>
			<div class="main-kontent-center-left-text" style="color:red">
				<div id="infoMessage" class="red"><?php echo $message;?></div>
			</div>
			<div class="main-kontent-center-left-text">
			<form method="post" autocomplete="off" action="/feedback">

						<div class="row">
							<label class="block clearfix col-sm-12">
								<?php echo form_input($name);?>
							</label>
						</div>
						<div class="row">
							<label class="block clearfix col-sm-12">
								<?php echo form_input($email);?>
							</label>
						</div>
						<div class="row">
							<label class="block clearfix col-sm-12">
								<?php echo form_textarea($body);?>
							</label>
						</div>
				
				<input type="submit" class="button-blok col-lg-12 col-md-12 col-sm-12 col-xs-12" name="submit" value="Отправить" />
			</form>
			</div>
		</div>

<?php $this->load->view('layout/banners'); ?>
		<div class="clearfix"></div>
	</div>
	<div class="clearfix"></div>
<script type="text/javascript">
$().ready(function(){
}); 
</script>

<?php $this->load->view('layout/footer'); ?>