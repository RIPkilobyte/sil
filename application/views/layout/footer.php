<div class="footer">
	<div class="footer-blok">
		<div class="footer-blok-left col-lg-3 col-md-3 col-sm-3 col-xs-3">
			© <?php echo date('Y'); ?> Союз Изобретательных Людей <br>
		</div>
		<div class="footer-blok-center col-lg-6 col-md-6 col-sm-6 col-xs-6">
			<a href="/sitemap">Карта сайта</a>
		</div>
		<div class="footer-blok-right col-lg-3 col-md-3 col-sm-3 col-xs-3">
<!--LiveInternet counter--><script type="text/javascript">
document.write('<a href="//www.liveinternet.ru/click" '+
'target="_blank"><img src="//counter.yadro.ru/hit?t21.2;r'+
escape(document.referrer)+((typeof(screen)=='undefined')?'':
';s'+screen.width+'*'+screen.height+'*'+(screen.colorDepth?
screen.colorDepth:screen.pixelDepth))+';u'+escape(document.URL)+
';h'+escape(document.title.substring(0,150))+';'+Math.random()+
'" alt="" title="LiveInternet: показано число просмотров за 24'+
' часа, посетителей за 24 часа и за сегодня" '+
'border="0" width="88" height="31"><\/a>')
</script><!--/LiveInternet-->
		</div>
		<div class="clearfix"></div>
	</div>
</div>

<script type="text/javascript"> 
$(document).ready(function() { 
	$("#search_form").submit(function(){
		$("#search_input").value($.trim($("#search_input").value()));
		return true;
	});
}); 
</script>

</div>
</div>
</body>
</html>