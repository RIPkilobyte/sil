<!doctype html>
<html>
<head>
	<meta charset="utf-8">
	<title><?php echo $site_title; ?></title>
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<meta name="yandex-verification" content="8da83b6138dcb9df" />

	<link rel="stylesheet" type="text/css" media="all" href="/assets/css/style.css" />
	<link rel="stylesheet" type="text/css" media="all" href="/assets/css/ace.min.css" />
	<link rel="stylesheet" type="text/css" media="all" href="/assets/css/mediacss.css" />
	<link rel="stylesheet" type="text/css" media="all" href="/assets/css/animate.css" />
	<link rel="stylesheet" type="text/css" media="all" href="/assets/css/colorbox.css" />
	<link rel="stylesheet" type="text/css" media="all" href="/assets/css/bootstrap.css" />
	<link rel="stylesheet" type="text/css" media="all" href="/assets/css/bootstrap-theme.css" />
	<link rel="stylesheet" type="text/css" media="all" href="/assets/css/font-awesome.css" />
	<link rel="stylesheet" type="text/css" media="all" href="/assets/css/owl.carousel.css" />
	<link rel="stylesheet" type="text/css" media="all" href="/assets/css/owl.theme.default.css" />
	<link rel="stylesheet" type="text/css" media="all" href="/assets/css/owl.theme.css" />
	<link rel="stylesheet" type="text/css" media="all" href="/assets/css/select2.min.css" />
	<link rel="stylesheet" type="text/css" media="all" href="/assets/css/bootstrap-datepicker3.min.css" />
	<link rel="stylesheet" type="text/css" media="all" href="/assets/css/bootstrap-editable.min.css" />
	<link rel="stylesheet" type="text/css" media="all" href="/assets/css/jquery-ui.custom.min.css" />
	<link rel="stylesheet" type="text/css" media="all" href="/assets/css/jquery.gritter.min.css" />
	<link rel="stylesheet" type="text/css" media="all" href="/assets/css/ace-skins.min.css" />
	<link rel="stylesheet" type="text/css" media="all" href="/assets/css/ace-rtl.min.css" />
	<link rel="stylesheet" type="text/css" media="all" href="/assets/css/ekko-lightbox.css" />

	<script src="/assets/js/ace-extra.min.js"></script>
	<script src="/assets/js/jquery1.9.1.min.js"></script>
	<script src="/assets/js/jquery.form.min.js"></script>
	<script src="/assets/js/jquery.colorbox.js"></script>
	<script src="/assets/js/bootstrap.js"></script>
	<script src="/assets/js/owl.carousel.js"></script>
	<script src="/assets/js/jquery.maskedinput.min.js"></script>
	<script src="/assets/js/bootstrap-datepicker.min.js"></script>
	<script src="/assets/js/jquery-ui.custom.min.js"></script>
	<script src="/assets/js/select2.min.js"></script>
	<script src="/assets/js/bootstrap-editable.min.js"></script>
	<script src="/assets/js/ace-editable.min.js"></script>

	<link rel="stylesheet" href="/assets/css/jquery.fancybox.css" type="text/css" media="screen" /> 
	<script type="text/javascript" src="/assets/js/jquery.fancybox.pack.js"></script> 
</head>
<body>
<div class="main-blok">
<div class="header-blok">
	<div class="logotip col-lg-3 col-md-3 col-sm-3 col-xs-3">
		<a href="/">
			<img title="Вернуться на главную страницу" src="/assets/images/logo.png"/> 
		</a>
	</div>
	<div class="header-kontent col-lg-9 col-md-9 col-sm-9 col-xs-9">
		<div class="lozung">«СОЮЗ ИЗОБРЕТАТЕЛЬНЫХ ЛЮДЕЙ»</div>

<?php $this->load->view('layout/carousel'); ?>

		<div class="user-bar">

<?php //$this->load->view('layout/cities'); ?>

			<div class="search-bar">
				<div class="search-bar-blok">
					<div class="ya-site-form ya-site-form_inited_no" data-bem="{&quot;action&quot;:&quot;http://xn--h1agq.xn--c1avg/search&quot;,&quot;arrow&quot;:false,&quot;bg&quot;:&quot;transparent&quot;,&quot;fontsize&quot;:16,&quot;fg&quot;:&quot;#000000&quot;,&quot;language&quot;:&quot;ru&quot;,&quot;logo&quot;:&quot;rb&quot;,&quot;publicname&quot;:&quot;Поиск по сайту сил.орг&quot;,&quot;suggest&quot;:true,&quot;target&quot;:&quot;_self&quot;,&quot;tld&quot;:&quot;ru&quot;,&quot;type&quot;:3,&quot;usebigdictionary&quot;:true,&quot;searchid&quot;:2360808,&quot;input_fg&quot;:&quot;#000000&quot;,&quot;input_bg&quot;:&quot;#ffffff&quot;,&quot;input_fontStyle&quot;:&quot;normal&quot;,&quot;input_fontWeight&quot;:&quot;normal&quot;,&quot;input_placeholder&quot;:&quot;Поиск&quot;,&quot;input_placeholderColor&quot;:&quot;#000000&quot;,&quot;input_borderColor&quot;:&quot;#7f9db9&quot;}">
						<form action="https://yandex.ru/search/site/" method="get" target="_self" accept-charset="utf-8">
							<input type="hidden" name="searchid" value="2360808"/>
							<input type="hidden" name="l10n" value="ru"/>
							<input type="hidden" name="reqenc" value=""/>
							<input type="search" id="search_input" class="form-control" name="text" style="width: 330px; margin-left: -60px !important;" value=""/>
							<input type="submit" value="Найти" style="background:url('/assets/images/search.png') no-repeat !important; width: 30px; height:30px;" />
						</form>
					</div>
					<style type="text/css">.ya-page_js_yes .ya-site-form_inited_no { display: none; }</style><script type="text/javascript">(function(w,d,c){var s=d.createElement('script'),h=d.getElementsByTagName('script')[0],e=d.documentElement;if((' '+e.className+' ').indexOf(' ya-page_js_yes ')===-1){e.className+=' ya-page_js_yes';}s.type='text/javascript';s.async=true;s.charset='utf-8';s.src=(d.location.protocol==='https:'?'https:':'http:')+'//site.yandex.net/v2.0/js/all.js';h.parentNode.insertBefore(s,h);(w[c]||(w[c]=[])).push(function(){Ya.Site.Form.init()})})(window,document,'yandex_site_callbacks');</script>
				</div>
			</div>
			<div class="kabinet">
				<?php if($this->ion_auth->logged_in()): ?>
					<a href="/cabinet"><i class="fa fa-user-o" aria-hidden="true"></i> <?php echo $this->ion_auth->user($this->ion_auth->get_user_id())->row()->username; ?></a> | <a href="/logout">Выход</a>
					<?php if($this->ion_auth->is_admin()): ?>
						| <a href="/admin">Администрирование</a>
					<?php endif; ?>
				<?php else: ?>
					<a href="/login">Вход</a> | <a href="/register">Регистрация</a>
				<?php endif; ?>
			</div>
		</div>
	</div>
</div>

<?php //$this->load->view('layout/breadcrumb'); ?>