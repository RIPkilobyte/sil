<?php $this->load->view('layout/header'); ?>

<div class="main-kontent">
<?php $this->load->view('layout/information'); ?>

	<div class="main-kontent-center">
		<div class="main-kontent-center-left">
			<?php if(isset($text_title) && $text_title) echo '<div class="main-kontent-center-left-title">'.$text_title.'</div>'; ?>
			<?php echo '<div class="main-kontent-center-left-text">'.$content.'</div>'; ?>
		</div>

<?php $this->load->view('layout/banners'); ?>
		<div class="clearfix"></div>
	</div>
	<div class="clearfix"></div>
<script type="text/javascript">
$().ready(function(){
}); 
</script>

<?php $this->load->view('layout/footer'); ?>