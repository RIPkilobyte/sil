<?php $this->load->view('layout/header'); ?>
<div class="main-kontent">
<div class="kontent-left-blok w100">
		<div class="forum">
			<div class="forum-title">
				<a href="/solutions/category/<?php echo $category->alias; ?>"><?php echo $category->title; ?>
			</div>
			<?php if($category->description): ?>
			<div class="forum-text">
				<div class="forum-spisok-tem">
					<div class="forum-tema">
						<?php echo $category->description; ?>
					</div>
				</div>
			</div>
			<?php endif; ?>
			<?php if(isset($childs) && $childs): ?>
				<?php
					$none = 1;
					foreach($childs as $k=>$v) {
						if($v->has_posts) {
							$none = 0;
						}
					}
				?>
				<?php if(!$none): ?>
					<div class="forum-text">
						<div class="forum-spisok-tem">
							<?php foreach ($childs as $k=>$v): ?>
								<?php if($v->has_posts): ?>
									<div class="forum-tema">
										<a href="/solutions/category/<?php echo $v->alias; ?>">
											<i class="fa fa-list-ul" aria-hidden="true"></i> 
											<?php echo $v->title; ?>
										</a>
									</div>
								<?php endif; ?>
							<?php endforeach; ?>
						</div>
					</div>
				<?php endif; ?>
			<?php endif; ?>

			<?php if(isset($posts) && $posts && $category->has_posts): ?>
				<div class="forum-text">
					<div class="forum-spisok-tem">
						<?php foreach ($category->posts as $k=>$v): ?>
							<?php if(isset($posts) && isset($category->posts[$k]) && $category->posts[$k] && isset($posts[$k])): ?>
								<div class="forum-tema">
									<a href="/solutions/post/<?php echo $posts[$k]->blog_id; ?>"></i> <?php echo $posts[$k]->title; ?></a>
								</div>
							<?php endif; ?>
						<?php endforeach; ?>
					</div>
				</div>
			<?php endif; ?>
			<?php 
				$none = 1;
				if($posts) {
					$none = 0;
				}
				if($childs) {
					foreach($childs as $k=>$v) {
						if($v->has_posts) {
							$none = 0;
						}
					}
				}
			?>

			<?php if($none): ?>
				<div class="forum-text">
					<div class="forum-spisok-tem">
						<div class="forum-tema"><i class="fa fa-list-ul" aria-hidden="true"></i> Здесь пока не размещено ни одной записи</div>
					</div>
				</div>
			<?php endif; ?>


		</div>

		<form action="/solutions/add" method="post" id="add_form">
			<input type="hidden" name="category_id" value="<?php echo $category->blog_category_id; ?>" />
			<div class="bordered forum-text ta_center"><a href="javascript:void(0)" style="font-size:27px;" class="interesno-blok" onclick="$('#add_form').submit();">Добавить запись в эту категорию</a></div>
		</form>
</div>

<?php //$this->load->view('layout/banners'); ?>
	<div class="clearfix"></div>
	</div>
	<div class="clearfix"></div>  

<?php $this->load->view('layout/footer'); ?>