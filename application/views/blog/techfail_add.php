<?php $this->load->view('layout/header'); ?>

<div class="main-kontent">
<?php $this->load->view('layout/information'); ?>

	<div class="main-kontent-center">
		<div class="main-kontent-center-left w100">
			<div class="main-kontent-center-left-title">Добавление нового техноляпа</div>
			<div class="main-kontent-center-left-text" style="color:red">
				<?php echo validation_errors(); ?>
			</div>
			<div class="main-kontent-center-left-text">
			<form method="post" autocomplete="off" action="/techfail/add" id="add_form" enctype="multipart/form-data">
				<label>Заголовок <span class="red"><a href="javascript:void(0)" data-toggle="tooltip" title="Это поле обязательно">*</a></span></label><input type="text" name="title" value="<?php echo $title; ?>" class="w100" /><br />

				<label>Главная тема <span class="red"><a href="javascript:void(0)" data-toggle="tooltip" title="Это поле обязательно">*</a></span></label><select class="select_category w100" name="category_id1">
				<?php foreach ($categories as $k => $v) {
					$selected = '';
					if($v->blog_category_id == $category_id1) {
						$selected="selected='selected'";
					}
					echo '<option '.$selected.' value="'.$v->blog_category_id.'">'.$v->title.'</option>';
					if(isset($v->childs) && $v->childs) {
						foreach($v->childs as $k1=>$v1) {
							$selected = '';
							if($v1->blog_category_id == $category_id1) {
								$selected="selected='selected'";
							}
							echo '<option '.$selected.' value="'.$v1->blog_category_id.'">-'.$v1->title.'</option>';
							if(isset($v1->childs) && $v1->childs) {
								foreach($v1->childs as $k2=>$v2) {
									$selected = '';
									if($v2->blog_category_id == $category_id1) {
										$selected="selected='selected'";
									}
									echo '<option '.$selected.' value="'.$v2->blog_category_id.'">--'.$v2->title.'</option>';
									if(isset($v2->childs) && $v2->childs) {
										foreach($v2->childs as $k3=>$v3) {
											$selected = '';
											if($v3->blog_category_id == $category_id1) {
												$selected="selected='selected'";
											}
											echo '<option '.$selected.' value="'.$v3->blog_category_id.'">---'.$v3->title.'</option>';
										}
									}
								}
							}
						}
					}
				} ?>
				</select><br />
				<label>Подтема 1 (необязательно)</label><select class="select_category w100" name="category_id2">
				<option value="0">Нет дополнительной темы</option>
				<?php foreach ($categories as $k => $v) {
					$selected = '';
					if($v->blog_category_id == $category_id2) {
						$selected="selected='selected'";
					}
					echo '<option '.$selected.' value="'.$v->blog_category_id.'">'.$v->title.'</option>';
					if(isset($v->childs) && $v->childs) {
						foreach($v->childs as $k1=>$v1) {
							$selected = '';
							if($v1->blog_category_id == $category_id2) {
								$selected="selected='selected'";
							}
							echo '<option '.$selected.' value="'.$v1->blog_category_id.'">-'.$v1->title.'</option>';
							if(isset($v1->childs) && $v1->childs) {
								foreach($v1->childs as $k2=>$v2) {
									$selected = '';
									if($v2->blog_category_id == $category_id2) {
										$selected="selected='selected'";
									}
									echo '<option '.$selected.' value="'.$v2->blog_category_id.'">--'.$v2->title.'</option>';
									if(isset($v2->childs) && $v2->childs) {
										foreach($v2->childs as $k3=>$v3) {
											$selected = '';
											if($v3->blog_category_id == $category_id2) {
												$selected="selected='selected'";
											}
											echo '<option '.$selected.' value="'.$v3->blog_category_id.'">---'.$v3->title.'</option>';
										}
									}
								}
							}
						}
					}
				} ?>
				</select><br />
				<label>Подтема 2 (необязательно)</label><select class="select_category w100" name="category_id3">
				<option value="0">Нет дополнительной темы</option>
				<?php foreach ($categories as $k => $v) {
					$selected = '';
					if($v->blog_category_id == $category_id3) {
						$selected="selected='selected'";
					}
					echo '<option '.$selected.' value="'.$v->blog_category_id.'">'.$v->title.'</option>';
					if(isset($v->childs) && $v->childs) {
						foreach($v->childs as $k1=>$v1) {
							$selected = '';
							if($v1->blog_category_id == $category_id3) {
								$selected="selected='selected'";
							}
							echo '<option '.$selected.' value="'.$v1->blog_category_id.'">-'.$v1->title.'</option>';
							if(isset($v1->childs) && $v1->childs) {
								foreach($v1->childs as $k2=>$v2) {
									$selected = '';
									if($v2->blog_category_id == $category_id3) {
										$selected="selected='selected'";
									}
									echo '<option '.$selected.' value="'.$v2->blog_category_id.'">--'.$v2->title.'</option>';
									if(isset($v2->childs) && $v2->childs) {
										foreach($v2->childs as $k3=>$v3) {
											$selected = '';
											if($v3->blog_category_id == $category_id3) {
												$selected="selected='selected'";
											}
											echo '<option '.$selected.' value="'.$v3->blog_category_id.'">---'.$v3->title.'</option>';
										}
									}
								}
							}
						}
					}
				} ?>
				</select><br />

				<label>Описание <span class="red"><a href="javascript:void(0)" data-toggle="tooltip" title="Это поле обязательно">*</a></span></label><br />
				<textarea name="description" rows="15" id="editor1" class="form-control w100"><?php echo $description;?></textarea><br />


				<label>Приложите файлы (осталось <span id="count_upload"><?php echo $files_count; ?></span>):</label>
				<div id="loading_result"></div>
				<div id="loading_files"></div>
				<div id="loading_img" style="display:none;"><img src="/assets/images/loading.gif" height="40px" /></div>
				<div id="loading_btn">
					<input id="fileupload" type="file" name="fileupload" class="uploaded_files_input" value="Выберите файл" /><br />
					<p>или вставьте ссылку на файл</p>
					<input type="text" name="fileupload_url" value="" id="fileupload_url" class="w75" />
					<a href="javascript:void(0)" class="w15" onclick="upload_video()">Сохранить ссылку</a>
				</div>
				<div id="loading_filename" style="display:none;"></div>
				<br />
				
				<input type="submit" name="submit" value="Отправить" class="button-blok col-lg-12 col-md-12 col-sm-12 col-xs-12" />
			</form>
			</div>
		</div>

<?php //$this->load->view('layout/banners'); ?>
		<div class="clearfix"></div>
	</div>
	<div class="clearfix"></div>
<script src="/assets/ckeditor/ckeditor.js"></script>
<script src="/assets/js/ekko-lightbox.js"></script>
<script type="text/javascript">
function remove_file($id)
{
	if(confirm("Действительно удалить файл?")) {
		$("#file_"+$id).remove();
		count++;
	}
	return false;
}
function upload_video()
{
	if(count == 0) {
		$("#loading_btn").css('display', 'none');
		alert('Вы не можете прикреплять больше <?php echo $files_count; ?> файлов');
		return false;
	}
	var $url = $("#fileupload_url").val();
	if($url == '') {
		alert('Введите ссылку!');
		return false;
	}

	$.post("/ajax/ajax_upload_url/", {url:$url}, function(data){
			if(data.status == 'ok') {
				$("#loading_img").css('display', 'none');
				if(count > 0) {
					$("#loading_btn").css('display', 'block');
				}
				var $result = '<div id="file_'+data.file.file_id+'" class="uploaded_files">';
					$result += '<a href="'+data.file.fileNameOnDisk+'" data-toggle="lightbox" data-gallery="post">';
					if(data.file.is_image) {
						$result += '<img src="'+data.file.fileNameOnDisk+'" height="100px" />';
					}
					$result += data.file.file_name;
					$result += '</a>';
					$result += ' &nbsp; ';
					$result += '<a href="javascript:void(0)" onclick="remove_file('+data.file.file_id+')">';
					$result += '<img src="/assets/images/delete.png" height="16px" />';
					$result += '</a>';
					$result += '<input type="hidden" id="file_id_'+data.file.file_id+'" name="files[]" value="'+data.file.file_id+'" />';
					$result += '<br /></div>';
				$("#loading_files").append($result);
				count--;
				$("#count_upload").html(count);
			}
			else {
				$("#loading_result").addClass('red');
				$("#loading_result").html(data.error.message);
				setTimeout(function(){
					$("#loading_result").removeClass('red');
					$("#loading_result").html('');
				}, 2500);
			}
	},'json');
	$("#count_upload").html(count);

	return false;
}
$().ready(function(){
	var count = <?php echo $files_count; ?>;
	if($("#editor1").length) {
		CKEDITOR.replace('editor1', {
			height: 600
		});
	}
	$('#fileupload').change(function(){
		if(count == 0) {
			$("#loading_btn").css('display', 'none');
			alert('Вы не можете прикреплять больше <?php echo $files_count; ?> файлов');
			return false;
		}
		var file_data = $('#fileupload').prop('files')[0];
		var form_data = new FormData();
		form_data.append('fileupload', file_data);
		$.ajax({
			url: "/ajax/ajax_upload_file/",
			type: "POST",
			data: form_data,
			contentType: false,
			cache: false,
			processData:false,
			success: function(data){
				if(data.status == 'ok') {
					$("#loading_img").css('display', 'none');
					if(count > 0) {
						$("#loading_btn").css('display', 'block');
					}
					var $result = '<div id="file_'+data.file.file_id+'" class="uploaded_files">';
						$result += '<a href="'+data.file.fileNameOnDisk+'" data-toggle="lightbox" data-gallery="post">';
						if(data.file.is_image) {
							$result += '<img src="'+data.file.fileNameOnDisk+'" height="100px" />';
						}
						$result += data.file.file_name;
						$result += '</a>';
						$result += ' &nbsp; ';
						$result += '<a href="javascript:void(0)" onclick="remove_file('+data.file.file_id+')">';
						$result += '<img src="/assets/images/delete.png" height="16px" />';
						$result += '</a>';
						$result += '<input type="hidden" id="file_id_'+data.file.file_id+'" name="files[]" value="'+data.file.file_id+'" />';
						$result += '<br /></div>';
					$("#loading_files").append($result);
					count--;
					$("#count_upload").html(count);
				}
				else {
					$("#loading_result").html('Произошла ошибка при загрузке файла. Повторите попытку позднее.');
				}
			}
		});
	});
});
$(document).on('click', '[data-toggle="lightbox"]', function(event){
	event.preventDefault();
	$(this).ekkoLightbox();
});
</script>
<style type="text/css">

</style>
<?php $this->load->view('layout/footer'); ?>