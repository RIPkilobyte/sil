<?php $this->load->view('layout/header'); ?>
<div class="main-kontent">
<div class="kontent-left-blok w100">
	<div class="forum">
		<div class="forum-title">
			<h1 style="color:#FE0101; font-weight: bold;text-align: center;">Это Техноляпы!!! Заметил их — предложи Решение</h1>
		</div>
	</div>
	<?php foreach ($all_categories as $k => $v): ?>
		<div class="forum">
			<div class="forum-title">
				<a href="/techfail_categories/category/<?php echo $v->alias; ?>"><?php echo $v->title; ?></a> 
				<?php $has_posts = 0; ?>
				<?php if(isset($v->childs) && $v->childs): ?>
					<?php foreach($v->childs as $c): ?>
						<?php if($c->count_tech): ?>
							<?php $has_posts = 1; ?>
						<?php break; endif; ?>

						<?php if(isset($c->childs) && $c->childs): ?>
							<?php foreach($c->childs as $d): ?>
								<?php if($d->count_tech): ?>
									<?php $has_posts = 1; ?>
								<?php break; endif; ?>

								<?php if(isset($d->childs) && $d->childs): ?>
									<?php foreach($d->childs as $e): ?>
										<?php if($e->count_tech): ?>
											<?php $has_posts = 1; ?>
										<?php break; endif; ?>
									<?php endforeach; ?>
								<?php endif; ?>

							<?php endforeach; ?>
						<?php endif; ?>

					<?php endforeach; ?>
				<?php elseif($v->count_tech): ?>
					<?php $has_posts = 1; ?>
				<?php endif; ?>
				<?php if($v->count_tech): ?>
					<?php $has_posts = 1; ?>
				<?php endif; ?>
				<?php if($has_posts): ?>
					<a href="#" data-toggle="tooltip" title="В данной категории есть записи"><span class="plus_tooltip">(+)</span></a>
				<?php else: ?>
					<a href="#" data-toggle="tooltip" title="В данной категории нет записей"><span class="minus_tooltip">(-)</span></a>
				<?php endif; ?>
			</div>
			<?php if($v->description): ?>
			<div class="forum-text">
				<div class="forum-spisok-tem">
					<div class="forum-tema">
						<?php echo $v->description; ?>
					</div>
				</div>
			</div>
			<?php endif; ?>
			<?php if(isset($v->childs) && $v->childs): ?>
				<div class="forum-text">
					<div class="forum-spisok-tem">
						<?php foreach ($v->childs as $k1=>$v1): ?>
							<?php $has_posts = 0; ?>
							<?php if(isset($v1->childs) && $v1->childs): ?>
								<?php foreach ($v1->childs as $a): ?>
									<?php if($a->count_tech): ?>
										<?php $has_posts = 1; ?>
									<?php break; endif; ?>

									<?php if(isset($a->childs) && $a->childs): ?>
										<?php foreach($a->childs as $b): ?>
											<?php if($b->count_tech): ?>
												<?php $has_posts = 1; ?>
											<?php break; endif; ?>
										<?php endforeach; ?>
									<?php endif; ?>
								<?php endforeach; ?>
							<?php elseif($v1->count_tech): ?>
								<?php $has_posts = 1; ?>
							<?php endif; ?>
							<div class="forum-tema">
								<a href="/techfail_categories/category/<?php echo $v1->alias; ?>"><i class="fa fa-list-ul" aria-hidden="true"></i> <?php echo $v1->title; ?></a>
								<?php if($has_posts): ?>
									<a href="#" data-toggle="tooltip" title="В данной категории есть записи"><span class="plus_tooltip">(+)</span></a>
								<?php else: ?>
									<a href="#" data-toggle="tooltip" title="В данной категории нет записей"><span class="minus_tooltip">(-)</span></a>
								<?php endif; ?>
							</div>
						<?php endforeach; ?>

					</div>
				</div>
			<?php endif; ?>

			<?php if(isset($posts) && $posts && $category->has_posts): ?>
				<div class="forum-text">
					<div class="forum-spisok-tem">
						<?php foreach ($category->posts as $k=>$v): ?>
							<?php if(isset($posts) && isset($category->posts[$k]) && $category->posts[$k] && isset($posts[$k])): ?>
								<div class="forum-tema">
									<a href="/techfail/post/<?php echo $posts[$k]->blog_id; ?>"></i> <?php echo $posts[$k]->title; ?></a>
								</div>
							<?php endif; ?>
						<?php endforeach; ?>
					</div>
				</div>
			<?php endif; ?>
			<?php 
				$none = 1;
				if(isset($posts) && $posts) {
					$none = 0;
				}
				if(isset($category) && isset($category->childs) && $category->childs) {
					$none = 0;
				}
			?>

			<?php if(isset($posts) && isset($category) && $none): ?>
				<div class="forum-text">
					<div class="forum-spisok-tem">
						<div class="forum-tema"><i class="fa fa-list-ul" aria-hidden="true"></i> Здесь пока не размещено ни одной записи</div>
					</div>
				</div>
			<?php endif; ?>
		</div>
	<?php endforeach; ?>
	<?php if(isset($category) && $category): ?>
		<form action="/techfail/add" method="get" id="add_form">
			<input type="hidden" name="category_id" value="<?php echo $category->blog_category_id; ?>" />
			<div class="bordered forum-text ta_center"><a href="javascript:void(0)" style="font-size:27px;" class="interesno-blok" onclick="$('#add_form').submit();">Добавить техноляп в эту тему</a></div>
		</form>
	<?php endif; ?>
</div>
<?php //$this->load->view('layout/banners'); ?>
	<div class="clearfix"></div>
	</div>
	<div class="clearfix"></div>  
<script type="text/javascript">
$().ready(function(){
	$('[data-toggle="tooltip"]').tooltip();
});
</script>
<style type="text/css">
a[data-toggle="tooltip"] {
	color:gray;
	text-decoration: none;
}
</style>
<?php $this->load->view('layout/footer'); ?>