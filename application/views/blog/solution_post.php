<?php $this->load->view('layout/header'); ?>
<div class="main-kontent">
<div class="w100">
	<div class="kontent-left-blok-text row-flex">

		<div class="button-blok col-lg-4 col-md-4 col-sm-4 col-xs-4">
			<?php if($this->ion_auth->logged_in()): ?>
				<button class="button-style" onclick="location.href='#post_form'"><i class="fa fa-share" aria-hidden="true"></i> Ответить</button>
			<?php endif; ?>
		</div>

		<div class="button-blok-pagination col-lg-8 col-md-8 col-sm-8 col-xs-8">
			<!--
			<span class="pagination-blok">
				<ul>
					<li class="active"><a href="/">1</a></li>
					<li><a href="/forum">2</a></li>
					<li><a href="/forum/topic">3</a></li>
				</ul>
			</span>
			-->
			<span class="soobsheniya-chislo">Комментариев: <span><?php echo $count_comments; ?></span></span>
		</div>

		<div class="soobshenie row-flex w100">
			<div class="soobshenie-image col-lg-2 col-md-2 col-sm-2 col-xs-2">
				<?php if(isset($author->avatar) && $author->avatar): ?>
					<img src="<?php echo $author->avatar->fileNameOnDisk; ?>" width="125" height="125" alt=""/> 
				<?php else: ?>
					<img src="/assets/images/ava.png" width="125" height="125" alt=""/> 
				<?php endif;?>
				<div class="soobshenie-name"><?php echo $author->username; ?></div>
				<div class="soobshenie-rega">Зарегистрирован: <?php echo date("d.m.Y H:i", $author->created_on); ?></div>
				<div class="soobshenie-chislo">Сообщений: <?php echo $author->comments; ?></div>
			</div>
			<div class="soobshenie-text col-lg-10 col-md-10 col-sm-10 col-xs-10">
				<div class="soobshenie-text-title">
					<?php echo $post->title; ?>
					<?php if($post->created_id == $this->ion_auth->get_user_id() || $this->ion_auth->is_admin()): ?>
						<div class="soobshenie-text-edit-link"><a href="/solutions/edit_post/<?php echo $post->blog_id; ?>">Редактировать</a></div><br />
						<div class="soobshenie-text-edit-link"><a onclick="delete_post(<?php echo $post->blog_id; ?>)" href="javascript:void(0)">Удалить</a></div>
					<?php endif; ?>
				</div>
				<div class="soobshenie-text-date"><?php echo date("d.m.Y H:i", strtotime($post->created_at)); ?></div>
				<br />
				<div class="soobshenie-text-kontent">
					<?php echo $post->description; ?>
				</div>
				<div class="soobshenie-text-date">
					<?php if($post->owner): ?>
						<a href="javascript:void(0)" data-toggle="tooltip" title="Это придумал я, и, если Вам понравилось, то при распространении этих материалов прошу ссылаться на меня, как на автора. Мои авторские права ПОДТВЕРЖДЕНЫ">(2)</a>
					<?php else: ?>
						<a href="javascript:void(0)" data-toggle="tooltip" title="Кто придумал - не знаю, откуда у меня такая информация - точно не помню , на авторские права НЕ ПРЕТЕНДУЮ. Найдёте автора, буду вам благодарен, а ему - мое уважение" >(1)</a>
					<?php endif; ?>
				</div>
				<?php if(isset($post->files) && $post->files) : ?>
					<hr />
					<div class="soobshenie-text-kontent">
						<p>Прикрепленные файлы:</p>
						<?php foreach($post->files as $file): ?>
							<span class="post_images">
								<a href="<?php echo $file->fileNameOnDisk; ?>" data-toggle="lightbox" data-gallery="post-<?php echo $post->blog_id; ?>" <?php if($file->is_image): ?>data-type="image"<?php endif; ?>>
									<?php if($file->is_image): ?>
										<img src="<?php echo $file->fileNameOnDisk; ?>" height="100px" />
									<?php else: ?>
										<?php echo $file->file_name; ?>
									<?php endif; ?>
								</a>
							</span>
						<?php endforeach; ?>
					</div>
				<?php endif; ?>

				<?php if(isset($post->edited) && $post->edited): ?>
					<hr />
					<div class="soobshenie-text-edit">Отредактировано пользователем <?php echo $post->edited->username; ?> <?php echo date("d.m.Y H:i", strtotime($post->updated_at)); ?></div>
				<?php endif; ?>
			</div>
		</div>
		<?php if(isset($blog_comments) && $blog_comments): ?>
			<?php foreach($blog_comments as $k=>$v): ?>
				<div class="soobshenie row-flex w100" id="comment_<?php echo $v->blog_comment_id; ?>">
					<div class="soobshenie-image col-lg-2 col-md-2 col-sm-2 col-xs-2">
						<?php if(isset($v->user->avatar) && $v->user->avatar): ?>
							<img src="<?php echo $v->user->avatar->fileNameOnDisk; ?>" width="125" height="125" alt=""/> 
						<?php else: ?>
							<img src="/assets/images/ava.png" width="125" height="125" alt=""/> 
						<?php endif;?>
						<div class="soobshenie-name"><?php echo $v->user->username; ?></div>
						<div class="soobshenie-rega">Зарегистрирован: <?php echo date("d.m.Y H:i", $v->user->created_on); ?></div>
						<div class="soobshenie-chislo">Сообщений: <?php echo $v->user->comments; ?></div>
					</div>
					<div class="soobshenie-text col-lg-10 col-md-10 col-sm-10 col-xs-10">
						<div class="soobshenie-text-title">
							<a href="/solutions/post/<?php echo $post->blog_id; ?>/#comment_<?php echo $v->blog_comment_id; ?>">#<?php echo $v->blog_comment_id; ?></a>
							<?php if(!$v->deleted): ?>
							<?php if($v->created_id == $this->ion_auth->get_user_id() || $this->ion_auth->is_admin()): ?>
								<div class="soobshenie-text-edit-link"><a href="/solutions/edit_comment/<?php echo $v->blog_comment_id; ?>">Редактировать</a></div><br />
								<div class="soobshenie-text-edit-link"><a onclick="delete_comment(<?php echo $v->blog_comment_id; ?>)" href="javascript:void(0)">Удалить</a></div>
							<?php endif; ?>
							<?php endif; ?>
						</div>
						<?php if($v->deleted): ?>
							<div class="soobshenie-text-date">
								<p>Сообщение удалено</p>
							</div>
						<?php else: ?>
							<div class="soobshenie-text-date"><?php echo date("d.m.Y H:i", strtotime($v->created_at)); ?></div>
							<div class="soobshenie-text-kontent">
								<?php echo $v->text; ?>
							</div>
							<?php if(isset($v->files) && $v->files) : ?>
								<hr />
								<div class="soobshenie-text-kontent">
									<p>Прикрепленные файлы:</p>
									<?php foreach($v->files as $file): ?>
										<span class="post_images">
											<a href="<?php echo $file->fileNameOnDisk; ?>" data-toggle="lightbox" data-gallery="comment-<?php echo $v->blog_comment_id; ?>">
												<?php if($file->is_image): ?>
													<img src="<?php echo $file->fileNameOnDisk; ?>" height="100px" />
												<?php else: ?>
													<?php echo $file->file_name; ?>
												<?php endif; ?>
											</a>
										</span>
									<?php endforeach; ?>
								</div>
							<?php endif; ?>
							<?php if(isset($v->user->edited) && $v->user->edited): ?>
								<hr />
								<div class="soobshenie-text-edit">Отредактировано пользователем <?php echo $v->user->edited->username; ?> <?php echo date("d.m.Y H:i", strtotime($v->updated_at)); ?></div>
							<?php endif; ?>
						<?php endif; ?>
					</div>
				</div>
			<?php endforeach;?>
		<?php endif; ?>
		<?php if($this->ion_auth->logged_in()): ?>
			<?php if($post->blog_id != '31'): ?>
			<div class="soobshenie row-flex w100" id="post_form">
				<div class="soobshenie-text col-lg-12 col-md-12 col-sm-12 col-xs-12">
					<div class="soobshenie-text-title">Написать ответ &nbsp; <a href="/solutions/post/31" class="link_hint" target="_blank">Как вставлять изображения</a></div>
					<div class="soobshenie-text-kontent" style="color: red">
						<?php echo validation_errors(); ?>
					</div>
					<form method="post" enctype="multipart/form-data">
						<textarea name="comment" rows="15" id="editor1" class="form-control w100"></textarea><br />
						<label>Приложите файлы (осталось <span id="count_upload"><?php echo $files_count; ?></span>):</label>
						<div id="loading_result"></div>
						<div id="loading_files"></div>
						<div id="loading_img" style="display:none;"><img src="/assets/images/loading.gif" height="40px" /></div>
						<div id="loading_btn">
							<input id="fileupload" type="file" name="fileupload" class="uploaded_files_input" value="Выберите файл" /><br />
							<p>или вставьте ссылку на файл</p>
							<input type="text" name="fileupload_url" value="" id="fileupload_url" class="w75" />
							<a href="javascript:void(0)" class="w15" onclick="upload_video()">Сохранить ссылку</a>
						</div>
						<div id="loading_filename" style="display:none;"></div>
						<br />
						<input type="submit" value="Ответить" class="button-blok col-lg-12 col-md-12 col-sm-12 col-xs-12">
					</form>
					
				</div>
			</div>
			<?php endif; ?>
		<?php endif; ?>
	</div>

	
	<div class="button-blok-pagination">
		<!--
		<span class="pagination-blok">
			<ul>
				<li class="active"><a href="/">1</a></li>
				<li><a href="/forum">2</a></li>
				<li><a href="/forum/topic">3</a></li>
			</ul>
		</span>
		-->
		<span class="soobsheniya-chislo">Комментариев: <span><?php echo $count_comments; ?></span></span>
		
	</div>
	
</div>

<?php //$this->load->view('layout/banners'); ?>
	<div class="clearfix"></div>
	</div>
	<div class="clearfix"></div>
<style type="text/css">
a[data-toggle="tooltip"] {
	color:gray;
	text-decoration: none;
}
</style>
<script src="/assets/ckeditor/ckeditor.js"></script>
<script src="/assets/js/ekko-lightbox.js"></script>
<script type="text/javascript">
var count = <?php echo $files_count; ?>;
function remove_file($id)
{
	if(confirm("Действительно удалить файл?")) {
		$("#file_"+$id).remove();
		count++;
		$("#count_upload").html(count);
	}
	return false;
}
function delete_comment($id)
{
	if(confirm("Действительно удалить сообщение?")) {
		location.assign("http://"+window.location.hostname+"/solutions/delete_comment/"+$id);
	}
	return false;
}
function delete_post($id)
{
	if(confirm("Действительно удалить сообщение?")) {
		location.assign("http://"+window.location.hostname+"/solutions/delete_post/"+$id);
	}
	return false;
}
function upload_video()
{
	if(count == 0) {
		$("#loading_btn").css('display', 'none');
		alert('Вы не можете прикреплять больше <?php echo $files_count; ?> файлов');
		return false;
	}
	var $url = $("#fileupload_url").val();
	if($url == '') {
		alert('Введите ссылку!');
		return false;
	}

	$.post("/ajax/ajax_upload_url/", {url:$url}, function(data){
			if(data.status == 'ok') {
				$("#loading_img").css('display', 'none');
				if(count > 0) {
					$("#loading_btn").css('display', 'block');
				}
				var $result = '<div id="file_'+data.file.file_id+'" class="uploaded_files">';
					$result += '<a href="'+data.file.fileNameOnDisk+'" data-toggle="lightbox" data-gallery="post">';
					if(data.file.is_image) {
						$result += '<img src="'+data.file.fileNameOnDisk+'" height="100px" />';
					}
					$result += data.file.file_name;
					$result += '</a>';
					$result += ' &nbsp; ';
					$result += '<a href="javascript:void(0)" onclick="remove_file('+data.file.file_id+')">';
					$result += '<img src="/assets/images/delete.png" height="16px" />';
					$result += '</a>';
					$result += '<input type="hidden" id="file_id_'+data.file.file_id+'" name="files[]" value="'+data.file.file_id+'" />';
					$result += '<br /></div>';
				$("#loading_files").append($result);
				count--;
				$("#count_upload").html(count);
			}
			else {
				$("#loading_result").addClass('red');
				$("#loading_result").html(data.error.message);
				setTimeout(function(){
					$("#loading_result").removeClass('red');
					$("#loading_result").html('');
				}, 2500);
			}
	},'json');
	$("#count_upload").html(count);

	return false;
}
$().ready(function(){
	$('[data-toggle="tooltip"]').tooltip();
	if($("#editor1").length) {
		CKEDITOR.replace('editor1', {
			height: 600
		});
	}
	$('#fileupload').change(function(){
		if(count == 0) {
			alert('Вы не можете прикреплять больше <?php echo $files_count; ?> файлов');
			return false;
		}
		var file_data = $('#fileupload').prop('files')[0];
		var form_data = new FormData();
		form_data.append('fileupload', file_data);
		$.ajax({
			url: "http://"+window.location.hostname+"/ajax/ajax_upload_file/",
			type: "POST",
			data: form_data,
			contentType: false,
			cache: false,
			processData:false,
			success: function(data){
				if(data.status == 'ok') {
					$("#loading_img").css('display', 'none');
					if(count > 0) {
						$("#loading_btn").css('display', 'block');
					}
					var $result = '<div id="file_'+data.file.file_id+'" class="uploaded_files">';
						$result += '<a href="'+data.file.fileNameOnDisk+'" data-toggle="lightbox" data-gallery="post">';
						$result += '<img src="'+data.file.fileNameOnDisk+'" height="100px" />';
						$result += '</a>';
						$result += data.file.file_name;
						$result += ' &nbsp; ';
						$result += '<a href="javascript:void(0)" onclick="remove_file('+data.file.file_id+')">';
						$result += '<img src="/assets/images/delete.png" height="16px" />';
						$result += '</a>';
						$result += '<input type="hidden" id="file_id_'+data.file.file_id+'" name="files[]" value="'+data.file.file_id+'" />';
						$result += '<br /></div>';
					$("#loading_files").append($result);
					count--;
					$("#count_upload").html(count);
				}
				else {
					$("#loading_result").html('Произошла ошибка при загрузке файла. Повторите попытку позднее.');
				}
			}
		});
	});
});
$(document).on('click', '[data-toggle="lightbox"]', function(event){
	event.preventDefault();
	$(this).ekkoLightbox();
});
</script>
<?php $this->load->view('layout/footer'); ?>