<?php $this->load->view('layout/header'); ?>

<div class="main-kontent">
<?php $this->load->view('layout/information'); ?>

	<div class="main-kontent-center">
		<div class="main-kontent-center-left w100">
			<div class="main-kontent-center-left-title">Редактирование успешно завершено</div>
			<div class="main-kontent-center-left-text">
				<a href="<?php echo $return_url; ?>">Вернуться к записи</a>
			</div>
		</div>

<?php //$this->load->view('layout/banners'); ?>
		<div class="clearfix"></div>
	</div>
	<div class="clearfix"></div>
<script type="text/javascript">
$().ready(function(){
}); 
</script>

<?php $this->load->view('layout/footer'); ?>