<?php $this->load->view('layout/header'); ?>
<div class="main-kontent">
<div class="kontent-left-blok w100">
	<?php if($categories_with_posts): ?>
	<?php foreach ($categories_with_posts as $k => $v): ?>
		<div class="forum">
			<div class="forum-title">
				<a href="/solutions/category/<?php echo $v->alias; ?>"><?php echo $v->title; ?></a>
			</div>
			<?php if($v->description): ?>
			<div class="forum-text">
				<div class="forum-spisok-tem">
					<div class="forum-tema">
						<?php echo $v->description; ?>
					</div>
				</div>
			</div>
			<?php endif; ?>
			<?php if(isset($v->childs) && $v->childs): ?>
				<div class="forum-text">
					<div class="forum-spisok-tem">
						<?php foreach ($v->childs as $k1=>$v1): ?>
							<div class="forum-tema">
								<a href="/solutions/category/<?php echo $v1->alias; ?>"><i class="fa fa-list-ul" aria-hidden="true"></i> <?php echo $v1->title; ?></a>
							</div>
						<?php endforeach; ?>

					</div>
				</div>
			<?php endif; ?>
		</div>
	<?php endforeach; ?>
	<?php else: ?>
		<div class="forum">
			<div class="forum-title">
				В настоящий момент красивые решения отсутствуют, но Вы можете <a href="/solutions/add">добавить</a> свое
			</div>
		</div>

	<?php endif; ?>
</div>

<?php //$this->load->view('layout/banners'); ?>
	<div class="clearfix"></div>
	</div>
	<div class="clearfix"></div>  
<script type="text/javascript">
$().ready(function(){
	$('[data-toggle="tooltip"]').tooltip();
});
</script>
<style type="text/css">
a[data-toggle="tooltip"] {
	color:gray;
	text-decoration: none;
}
</style>
<?php $this->load->view('layout/footer'); ?>