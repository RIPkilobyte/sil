<?php $this->load->view('layout/header'); ?>

<div class="main-kontent">
<?php $this->load->view('layout/information'); ?>

	<div class="main-kontent-center">
		<div class="main-kontent-center-left w100">
			<div class="main-kontent-center-left-title">Добавление новой темы на голосование</div>
			<div class="main-kontent-center-left-text" style="color:red">
				<?php echo validation_errors(); ?>
			</div>
			<div class="main-kontent-center-left-text">
			<form method="post" autocomplete="off" action="/voting/add">
				<label>Заголовок </label><input type="text" class="w100" name="title" value="<?php echo $title; ?>" size="100%" required="required" /><br />
				<label>Выберите категорию к которой хотите добавить тему или оставьте ее основной если она не подходит к существующим</label><br />
				<select class="select_category w100" name="category_id">
					<option value="0">Это основная категория</option>
				<?php foreach ($categories as $k => $v) {
					$selected = '';
					if($v->blog_category_id == $category_id) {
						$selected="selected='selected'";
					}
					echo '<option '.$selected.' value="'.$v->blog_category_id.'">'.$v->title.'</option>';
					if(isset($v->childs) && $v->childs) {
						foreach($v->childs as $k1=>$v1) {
							$selected = '';
							if($v1->blog_category_id == $category_id) {
								$selected="selected='selected'";
							}
							echo '<option '.$selected.' value="'.$v1->blog_category_id.'">-'.$v1->title.'</option>';
							if(isset($v1->childs) && $v1->childs) {
								foreach($v1->childs as $k2=>$v2) {
									$selected = '';
									if($v2->blog_category_id == $category_id) {
										$selected="selected='selected'";
									}
									echo '<option '.$selected.' value="'.$v2->blog_category_id.'">--'.$v2->title.'</option>';
									if(isset($v2->childs) && $v2->childs) {
										foreach($v2->childs as $k3=>$v3) {
											$selected = '';
											if($v3->blog_category_id == $category_id) {
												$selected="selected='selected'";
											}
											echo '<option '.$selected.' value="'.$v3->blog_category_id.'">---'.$v3->title.'</option>';
										}
									}
								}
							}
						}
					}
				} ?>
				</select><br />
				<label>Описание &nbsp; <a href="/solutions/post/31" class="link_hint" target="_blank">Как вставлять изображения</a></label><br />
				<textarea name="description" rows="15" id="editor1" class="form-control w100"><?php echo $description;?></textarea><br />

				
				<input type="submit" value="Отправить" class="button-blok col-lg-12 col-md-12 col-sm-12 col-xs-12">
			</form>
			</div>
		</div>

<?php //$this->load->view('layout/banners'); ?>
		<div class="clearfix"></div>
	</div>
	<div class="clearfix"></div>
<script src="/assets/ckeditor/ckeditor.js"></script>
<script type="text/javascript">
$().ready(function(){
	if($("#editor1").length) {
		CKEDITOR.replace('editor1', {
			height: 600
		});
	}
}); 
</script>
<style type="text/css">

</style>
<?php $this->load->view('layout/footer'); ?>