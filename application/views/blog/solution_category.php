<?php $this->load->view('layout/header'); ?>
<div class="main-kontent">
<div class="kontent-left-blok w100">
		<div class="forum">
			<div class="forum-title"><?php echo $category->title; ?></div>
			<?php if(isset($childs) && $childs): ?>
				<div class="forum-text">
					<div class="forum-spisok-tem">
						<?php foreach ($childs as $k1=>$v1): ?>
							<div class="forum-tema"><a href="/solutions/category/<?php echo $v1->alias; ?>"><i class="fa fa-list-ul" aria-hidden="true"></i> <?php echo $v1->title; ?></a></div>
						<?php endforeach; ?>
					</div>
				</div>
			<?php endif; ?>
			<?php if(isset($posts) && $posts): ?>
				<div class="forum-text">
					<div class="forum-spisok-tem">
						<?php foreach ($posts as $k1=>$v1): ?>
							<div class="forum-tema"><a href="/solutions/post/<?php echo $v1->blog_id; ?>"></i> <?php echo $v1->title; ?></a></div>
						<?php endforeach; ?>
					</div>
				</div>
			<?php endif; ?>
			<?php if(!$childs && !$posts): ?>
				<div class="forum-text">
					<div class="forum-spisok-tem">
						<div class="forum-tema"><i class="fa fa-list-ul" aria-hidden="true"></i> Здесь пока не размещено ни одной записи</div>
					</div>
				</div>
			<?php endif; ?>


		</div>

		<form action="/solutions/add" method="post" id="add_form">
			<input type="hidden" name="category_id" value="<?php echo $category->blog_category_id; ?>" />
			<div class="bordered forum-text ta_center"><a href="javascript:void(0)" style="font-size:27px;" class="interesno-blok" onclick="$('#add_form').submit();">Добавить запись в эту тему</a></div>
		</form>
</div>

<?php //$this->load->view('layout/banners'); ?>
	<div class="clearfix"></div>
	</div>
	<div class="clearfix"></div>  

<?php $this->load->view('layout/footer'); ?>