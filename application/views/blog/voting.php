<?php $this->load->view('layout/header'); ?>
<div class="main-kontent">
<div class="kontent-left-blok w100">
	<?php if(isset($voting) && $voting): ?>
		<?php foreach ($voting as $k => $v): ?>
			<div class="forum">
				<div class="forum-title">
					<?php echo $v->title; ?> &nbsp;
					Тема:&nbsp;
					<?php if(isset($v->category)): ?>
						<?php echo $v->category->title; ?>&nbsp;
					<?php else: ?>
						Основная
					<?php endif; ?>
				</div>
				<div class="forum-text">
					<div class="forum-spisok-tem">
						<div class="forum-tema">
							<?php if($this->ion_auth->logged_in()): ?>
								<a class="button-blok" href="/voting/vote/<?php echo $v->voting_id; ?>">Голосовать за эту тему</a> &nbsp;<br />
							<?php endif; ?>
							Всего голосов <?php echo $v->votes; ?> из <?php echo $v->max_votes; ?> необходимых<br />
							<?php if($v->description): ?>
								Описание:<br />
								<?php echo html_entity_decode($v->description); ?>
							<?php endif; ?>
						</div>
					</div>
				</div>
			</div>
		<?php endforeach; ?>
	<?php elseif($this->ion_auth->logged_in()): ?>
			<div class="forum">
				<div class="forum-title">Нет тем для голосования</div>
				<div class="forum-text">
					<div class="forum-spisok-tem">
						<div class="forum-tema">В настоящий момент темы для голосования отсутствуют, но вы можете добавить <a href="/voting/add">новую</a></div>
					</div>
				</div>
			</div>
	<?php else: ?>
			<div class="forum">
				<div class="forum-title">Нет тем для голосования</div>
				<div class="forum-text">
					<div class="forum-spisok-tem">
						<div class="forum-tema">В настоящий момент темы для голосования отсутствуют</div>
					</div>
				</div>
			</div>
	<?php endif; ?>
</div>

<?php //$this->load->view('layout/banners'); ?>
	<div class="clearfix"></div>
	</div>
	<div class="clearfix"></div>  

<?php $this->load->view('layout/footer'); ?>