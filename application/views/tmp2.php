<script type="text/javascript">
document.addEventListener('DOMContentLoaded', function(){
chart = new AmCharts.AmSerialChart();
var chartData = [{
"Отделение": "grdgtg", 
  "СвободныеКойки": 12, 
  "ФактическоеКоличествоКоек": 12
}
,{
"Отделение": "grdgtg", 
  "СвободныеКойки": 8, 
  "ФактическоеКоличествоКоек": 12
}];
chart.dataProvider=chartData;
chart.type= "serial";
chart.categoryField="Отделение";
chart.startDuration=0;

var legend = new Object();
legend.position="right";
legend.horizontalGap=10;
legend.marginTop=20;
legend.maxColumns=1;
legend.markerSize=10;
legend.useGraphSettings=true;
chart.addLegend(legend);

var categoryAxis = new AmCharts.AmLegend();
categoryAxis.axisAlpha=0;
categoryAxis.gridAlpha=0;
categoryAxis.gridPosition="start";
categoryAxis.position="left";
chart.addLegend(categoryAxis);

var valueAxis = new AmCharts.ValueAxis();
valueAxis.title="";
valueAxis.axisAlpha=0.3;
valueAxis.gridAlpha=0;
valueAxis.stackType="regular";
chart.addValueAxis(valueAxis);

var graph1 = new AmCharts.AmGraph();
graph1.lineAlpha=0.3;
graph1.valueField="СвободныеКойки";
graph1.title="";
graph1.labelText="[[value]]";
graph1.lineColor="#D23800";
graph1.fillAlphas=0.8;
graph1.type="column";
graph1.balloonText="Свободные койки: [[value]]";
chart.addGraph(graph1);

var graph2 = new AmCharts.AmGraph();
graph2.lineAlpha=0.3;
graph2.valueField="ФактическоеКоличествоКоек";
graph2.title="";
graph2.labelText="[[value]]";
graph2.lineColor="#00B3D2";
graph2.fillAlphas=0.8;
graph2.type="column";
graph2.balloonText="Фактическое количество коек: [[value]]";
chart.addGraph(graph2);

chart.addListener("drawn", function (event) {method="addLegendLabel"});
chart.write("chartdiv");

});
function addLegendLabel(e) {
  var title = document.createElement("div");
  title.innerHTML = "Countries";
  title.className = "legend-title";
  e.chart.legendDiv.appendChild(title)
}


</script>
<style type="text/css">

</style>
<script src="https://www.amcharts.com/lib/3/amcharts.js"></script>
<script src="https://www.amcharts.com/lib/3/serial.js"></script>
<div id="chartdiv"></div>