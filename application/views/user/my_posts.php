<?php $this->load->view('layout/header'); ?>
<script src="/assets/js/jquery.dataTables.min.js"></script>
<script src="/assets/js/jquery.dataTables.bootstrap.min.js"></script>
<script src="/assets/js/dataTables.buttons.min.js"></script>
<script src="/assets/js/buttons.flash.min.js"></script>
<script src="/assets/js/buttons.html5.min.js"></script>
<script src="/assets/js/buttons.print.min.js"></script>
<script src="/assets/js/buttons.colVis.min.js"></script>
<script src="/assets/js/dataTables.select.min.js"></script>
<div class="main-kontent">
<?php $this->load->view('layout/information'); ?>

	<div class="main-kontent-center">
		<div class="main-kontent-center-left w100">
			<div class="row center"><h1>Мои записи</h1></div>
			<div id="user-profile-1" class="user-profile row">
				<div class="col-xs-12 col-sm-12">
					<table id="dynamic-table" class="table table-striped table-bordered table-hover">
						<thead>
							<tr>
								<th>Название</th>
								<th>Тип записи</th>
								<th>Комментарии</th>
								<th>Основная тема</th>
								<th>Подтема 1</th>
								<th>Подтема 2</th>
								<th>Дата создания</th>
								<th>Дата изменения</th>
								<th></th>
							</tr>
						</thead>
						<tbody>
						</tbody>
					</table>
				</div>
			</div>
		</div>
<?php //$this->load->view('layout/banners'); ?>
		<div class="clearfix"></div>
	</div>
	<div class="clearfix"></div>
<script type="text/javascript">
$().ready(function(){
	var myTable = $('#dynamic-table').DataTable({
		ajax: {
			url: '/ajax/get_user_posts',
			dataSrc: '',
		},
		language: {
			url: '/assets/datatable_ru.json',
		},
		columns: [
			null,
			null,
			null,
			null,
			null,
			null,
			null,
			null,
			null,
		],
		searching: true,
	});
});
</script>
<style type="text/css">
#user-profile-1 {
	font-size: 50% !important;
}
</style>
<?php $this->load->view('layout/footer'); ?>