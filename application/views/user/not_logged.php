<?php $this->load->view('layout/header'); ?>
<div class="main-kontent">
<?php $this->load->view('layout/information'); ?>
	<div class="main-kontent-center">
		<div class="main-kontent-center-left">
			<div class="main-kontent-center-left-title">Вы не авторизованы</div>
			<div class="main-kontent-center-left-text">
				<div class="row">
					<label class="block clearfix col-sm-12">
						Чтобы добавить решение вам необходимо <a href="/login">авторизоваться</a> или <a href="/register">зарегистрироваться</a>
					</label>
				</div>
			</div>
		</div>
<?php $this->load->view('layout/banners'); ?>
		<div class="clearfix"></div>
	</div>
	<div class="clearfix"></div>
<script type="text/javascript">
$().ready(function(){
}); 
</script>
<?php $this->load->view('layout/footer'); ?>