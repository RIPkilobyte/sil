<?php $this->load->view('layout/header'); ?>
<div class="main-kontent">
<?php $this->load->view('layout/information'); ?>
	<div class="main-kontent-center">
		<div class="main-kontent-center-left">
			<div class="main-kontent-center-left-title">Восстановление пароля</div>
			<?php if(validation_errors()): ?>
			<div class="row">
				<div class="form-group">
					<div class="col-sm-12" style="color:red; font-size:200%;">
						<?php echo validation_errors(); ?>
					</div>
				</div>
			</div>
			<br />
			<?php endif; ?>
			<div class="main-kontent-center-left-text">
				<form method="post">
					<fieldset>
						<div class="row">
							<label class="block clearfix col-sm-12">
								<input type="password" name="password" class="form-control" placeholder="Пароль" />
							</label>
						</div>
						<div class="row">
							<label class="block clearfix col-sm-12">
								<input type="password" name="password_confirm" class="form-control" placeholder="Подтверждение пароля" />
							</label>
						</div>
						<div class="row">
							<div class="col-xs-12 col-sm-12">
								<button type="submit" class="col-sm-12 btn btn-sm btn-primary">
									<i class="ace-icon fa fa-key"></i>
									<span class="bigger-110">Сменить пароль</span>
								</button>
							</div>
						</div>
					</fieldset>
				</form>
			</div>
		</div>
<?php $this->load->view('layout/banners'); ?>
		<div class="clearfix"></div>
	</div>
	<div class="clearfix"></div>
<script type="text/javascript">
$().ready(function(){
}); 
</script>
<?php $this->load->view('layout/footer'); ?>