<?php $this->load->view('layout/header'); ?>
<div class="main-kontent">
<?php $this->load->view('layout/information'); ?>

	<div class="main-kontent-center">
		<div class="main-kontent-center-left w100">
			<div class="main-kontent-center-left-title">Изменение личных данных</div>
			<div class="main-kontent-center-left-text red">
				<?php echo validation_errors(); ?>
			</div>
			<?php if($user->update): ?>
				<div class="main-kontent-center-left-text" style="color:green; font-size:200%;">
					Успешно сохранено
				</div>
			<?php endif; ?>
			<div class="main-kontent-center-left-text">
			<form method="post" autocomplete="off" action="/user/edit" id="edit_form" enctype="multipart/form-data" autocomplete="off">
				<label>Отображаемое имя <span class="red"><a href="javascript:void(0)" data-toggle="tooltip" title="Это поле обязательно">*</a></span></label>
				<input type="text" name="username" value="<?php echo $user->username; ?>" class="w100" /><br />

				<label>Email <span class="red"><a href="javascript:void(0)" data-toggle="tooltip" title="Это поле обязательно">*</a></span></label>
				<input type="text" name="email" value="<?php echo $user->email; ?>" class="w100" /><br />

				<label>Фамилия</label>
				<input type="text" name="last_name" value="<?php echo $user->last_name; ?>" class="w100" /><br />

				<label>Имя</label>
				<input type="text" name="first_name" value="<?php echo $user->first_name; ?>" class="w100" /><br />

				<label>Отчество</label>
				<input type="text" name="second_name" value="<?php echo $user->second_name; ?>" class="w100" /><br />

				<label>Изображение (размеры 125 на 125 пикселей)</label><br />
				<input type="hidden" name="avatar_id" id="avatar_id" value="<?php echo $user->avatar_id; ?>" />
				<div id="avatar_div">
					<span class="post_images">
						<?php if($avatar): ?>
						<a href="<?php echo $avatar->fileNameOnDisk; ?>" data-toggle="lightbox" data-gallery="avatar">
							<img src="<?php echo $avatar->fileNameOnDisk; ?>" height="200px" />
						</a>
					<?php endif; ?>
					</span>
				</div>
				<div id="avatar_remove_div">
					<span class="post_images">
						<a href="javascript:void(0)" onclick="delete_avatar()">Удалить аватар</a>
					</span>
				</div>
				<div id="avatar_loading_div" style="display: none;">
					<label>Приложите файл</label>
					<div id="loading_result"></div>
					<div id="loading_files"></div>
					<div id="loading_img" style="display:none;"><img src="/assets/images/loading.gif" height="40px" /></div>
					<div id="loading_btn">
						<input id="fileupload" type="file" name="fileupload" class="uploaded_files_input" value="Выберите файл" /><br />
						<p>или вставьте ссылку на файл</p>
						<input type="text" name="fileupload_url" value="" id="fileupload_url" class="w75" />
						<a href="javascript:void(0)" class="w15" onclick="upload_video()">Сохранить ссылку</a>
					</div>
				</div>
				<br />

				<label>Если не хотите изменять пароль, оставьте следущие поля пустыми</label><br />
				<label>Пароль</label>
				<input type="password" name="password" class="w100" /><br />

				<label>Отчество</label>
				<input type="password" name="password_confirm" class="w100" /><br />

				<br /><br /><input type="submit" name="submit" value="Сохранить" class="button-blok col-lg-12 col-md-12 col-sm-12 col-xs-12" />
			</form>
		</div>
	</div>
<?php //$this->load->view('layout/banners'); ?>
		<div class="clearfix"></div>
	</div>
	<div class="clearfix"></div>
<style type="text/css">
a[data-toggle="tooltip"] {
	color:gray;
	text-decoration: none;
}
</style>
<script src="/assets/ckeditor/ckeditor.js"></script>
<script src="/assets/js/ekko-lightbox.js"></script>
<script type="text/javascript">
function delete_avatar()
{
	if(confirm("Вы действительно хотите удалить аватар?")) {
		$("#avatar_id").val(0);
		$("#avatar_div").html('');
		$("#avatar_remove_div").css('display', 'none');
		$("#avatar_loading_div").css('display', 'block');
	}
	return false;
}
function upload_video()
{
	var $url = $("#fileupload_url").val();
	if($url == '') {
		alert('Введите ссылку!');
		return false;
	}
	$.post("/ajax/ajax_upload_url/", {url:$url}, function(data){
			if(data.status == 'ok') {
				var $result = '<span class="post_images">';
					$result += '<a href="'+data.file.fileNameOnDisk+'" data-toggle="lightbox" data-gallery="avatar">';
					$result += '<img src="'+data.file.fileNameOnDisk+'" height="200px" />';
					$result += '</a>';
					$result += '</span>';
				$("#avatar_id").val(data.file.file_id);
				$("#avatar_div").html($result);
				$("#avatar_loading_div").css('display', 'none');
				$("#avatar_remove_div").css('display', 'block');
			}
			else {
				$("#loading_result").addClass('red');
				$("#loading_result").html(data.error.message);
				setTimeout(function(){
					$("#loading_result").removeClass('red');
					$("#loading_result").html('');
				}, 2500);
			}
	},'json');
	$("#count_upload").html(count);

	return false;
}
$().ready(function(){
	if($("#avatar_id").val() == '0') {
		$("#avatar_remove_div").css('display', 'none');
		$("#avatar_loading_div").css('display', 'block');
	}
	$('#fileupload').change(function(){
		var file_data = $('#fileupload').prop('files')[0];
		var form_data = new FormData();
		form_data.append('fileupload', file_data);
		$.ajax({
			url: "/ajax/upload_avatar/",
			type: "POST",
			data: form_data,
			contentType: false,
			cache: false,
			processData:false,
			success: function(data){
				if(data.status == 'ok') {
					var $result = '<span class="post_images">';
						$result += '<a href="'+data.file.fileNameOnDisk+'" data-toggle="lightbox" data-gallery="avatar">';
						$result += '<img src="'+data.file.fileNameOnDisk+'" height="200px" />';
						$result += '</a>';
						$result += '</span>';
					$("#avatar_id").val(data.file.file_id);
					$("#avatar_div").html($result);
					$("#avatar_loading_div").css('display', 'none');
					$("#avatar_remove_div").css('display', 'block');
				}
				else {
					$("#loading_result").addClass('red');
					$("#loading_result").html(data.error.message);
					setTimeout(function(){
						$("#loading_result").removeClass('red');
						$("#loading_result").html('');
					}, 2500);
				}
			}
		});
	});
});
$(document).on('click', '[data-toggle="lightbox"]', function(event){
	event.preventDefault();
	$(this).ekkoLightbox();
});
</script>
<?php $this->load->view('layout/footer'); ?>