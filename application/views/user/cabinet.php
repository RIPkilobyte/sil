<?php $this->load->view('layout/header'); ?>
<div class="main-kontent">
<?php $this->load->view('layout/information'); ?>

	<div class="main-kontent-center">
		<div class="main-kontent-center-left">
			<div class="row center"><h1>Мой профиль</h1></div>
			<div id="user-profile-1" class="user-profile row">

				<div class="col-xs-12 col-sm-12">

					<div class="space-12"></div>
					<div class="col-xs-12 col-sm-12">
						<div class="profile-info-row">
							<div class="">
								<a href="/solutions/add">Добавить запись</a><br />
								<a href="/techfail/add">Добавить техноляп</a><br />
								<a href="/voting">Голосовать за новую тему</a><br />
								<a href="/voting/add">Добавить новую тему</a><br /><br />

								<a href="/user/my_posts">Мои записи (<?php echo $posts; ?>)</a><br />
								<a href="/user/edit">Редактировать личную информацию</a><br />
							</div>
						</div>
					</div>
					<div class="profile-user-info profile-user-info-striped">
						<!--
						<div class="profile-info-row">
							<div class="profile-info-name"> Ваша фотография </div>
							<div class="profile-info-value">
								<span></span>
							</div>
						</div>
						-->
						<div class="profile-info-row">
							<div class="profile-info-name"> Отображаемое имя </div>
							<div class="profile-info-value">
								<span><?php echo $user->username; ?></span>
							</div>
						</div>
						<div class="profile-info-row">
							<div class="profile-info-name"> Фамилия </div>
							<div class="profile-info-value">
								<span><?php echo $user->last_name; ?></span>
							</div>
						</div>

						<div class="profile-info-row">
							<div class="profile-info-name"> Имя </div>
							<div class="profile-info-value">
								<span><?php echo $user->first_name; ?></span>
							</div>
						</div>
						<div class="profile-info-row">
							<div class="profile-info-name"> Отчество </div>
							<div class="profile-info-value">
								<span><?php echo $user->second_name; ?></span>
							</div>
						</div>
						<div class="profile-info-row">
							<div class="profile-info-name"> Последний вход </div>
							<div class="profile-info-value">
								<span><?php echo date("d.m.Y H:i", $user->last_login); ?></span>
							</div>
						</div>
					</div>
					<div class="space-20"></div>
				</div>
			</div>
		</div>
<?php $this->load->view('layout/banners'); ?>
		<div class="clearfix"></div>
	</div>
	<div class="clearfix"></div>
<script type="text/javascript">
$().ready(function(){

});
</script>
<?php $this->load->view('layout/footer'); ?>