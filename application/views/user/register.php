<?php $this->load->view('layout/header'); ?>
<div class="main-kontent">
<?php $this->load->view('layout/information'); ?>
	<div class="main-kontent-center">
		<div class="main-kontent-center-left">
			<div class="main-kontent-center-left-title" id="edit-title" rel="1">Регистрация</div>
			<div class="main-kontent-center-left-text" id="edit-text" rel="1">
				<div id="infoMessage"><?php echo $message;?></div>
				<form method="post" action="/register" autocomplete="off">
					<fieldset>
						<div class="row">
							<label class="block clearfix col-sm-12">
								<?php echo form_input($last_name);?>
							</label>
						</div>
						<div class="row">
							<label class="block clearfix col-sm-12">
								<?php echo form_input($first_name);?>
							</label>
						</div>
						<div class="row">
							<label class="block clearfix col-sm-12">
								<?php echo form_input($second_name);?>
							</label>
						</div>
						<div class="row">
							<label class="block clearfix col-sm-12">
								<?php echo form_input($username);?>
							</label>
						</div>
						<div class="row">
							<label class="block clearfix col-sm-12">
								<?php echo form_input($email);?>
							</label>
						</div>
						<div class="row">
							<label class="block clearfix col-sm-12">
								<?php echo form_input($password);?>
							</label>
						</div>
						<div class="row">
							<label class="block clearfix col-sm-12">
								<?php echo form_input($password_confirm);?>
							</label>
						</div>

						<div class="row">
							<div class="col-xs-12 col-sm-12">
							<button type="submit" class="col-sm-12 btn btn-sm btn-primary">
								<i class="ace-icon fa fa-key"></i>
								<span class="bigger-110">Зарегистрироваться</span>
							</button>
							</div>
						</div>
					</fieldset>
				</form>

			</div>
		</div>
<?php $this->load->view('layout/banners'); ?>
		<div class="clearfix"></div>
	</div>
	<div class="clearfix"></div>
<script type="text/javascript">
$().ready(function(){
}); 
</script>
<?php $this->load->view('layout/footer'); ?>