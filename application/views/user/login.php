<?php $this->load->view('layout/header'); ?>
<div class="main-kontent">
<?php $this->load->view('layout/information'); ?>
	<div class="main-kontent-center">
		<div class="main-kontent-center-left">
			<div class="main-kontent-center-left-title" id="edit-title" rel="1">Вход</div>
			<div class="main-kontent-center-left-text" id="edit-text" rel="1">
				<form method="post" action="/login" autocomplete="off">
					<fieldset>
						<div class="row">
							<div class="red"><?php echo $message;?></div>
						</div>
						<div class="row">
							<label class="block clearfix col-sm-12">
								<input type="text" name="login" class="form-control" placeholder="Email" value="<?php echo $login; ?>" />
							</label>
						</div>
						<div class="row">
							<label class="block clearfix col-sm-12">
								<input type="password" name="password" class="form-control" placeholder="Пароль" value="<?php echo $password; ?>" />
							</label>
						</div>
						<div class="row">
							<label class="inline col-sm-12">
								<input type="checkbox" name="remember" class="ace" />
								<span class="lbl"> Запомнить меня</span>
							</label>
						</div>
						<div class="row">
							<div class="col-xs-12 col-sm-12">
								<button type="submit" class="col-sm-6 btn btn-sm btn-primary">
									<i class="ace-icon fa fa-key"></i>
									<span class="bigger-110">Войти</span>
								</button>
								<button type="button" class="col-sm-6 btn btn-sm btn-warning" id="forgot">
									<span class="bigger-110">Забыл пароль</span>
								</button>
							</div>
						</div>
					</fieldset>
				</form>
			</div>
		</div>
<?php $this->load->view('layout/banners'); ?>
		<div class="clearfix"></div>
	</div>
	<div class="clearfix"></div>
<script type="text/javascript">
$().ready(function(){
	$("#forgot").click(function(){
		window.location.href='/forgot_password';
	});
}); 
</script>
<?php $this->load->view('layout/footer'); ?>