<?php $this->load->view('layout/header'); ?>
<div class="main-kontent">
<?php $this->load->view('layout/information'); ?>
	<div class="main-kontent-center">
		<div class="main-kontent-center-left">
			<div class="main-kontent-center-left-title">Анкета 2</div>
			<div class="main-kontent-center-left-text">
				<div id="infoMessage" class="red"><?php echo $message;?></div>
				<form method="post" action="/anketa2" autocomplete="off">
					<fieldset>
						<div class="row">
							<label class="block clearfix col-sm-12">
								<?php echo form_input($last_name);?>
							</label>
						</div>
						<div class="row">
							<label class="block clearfix col-sm-12">
								<?php echo form_input($first_name);?>
							</label>
						</div>
						<div class="row">
							<label class="block clearfix col-sm-12">
								<?php echo form_input($second_name);?>
							</label>
						</div>
						<div class="row">
							<label class="block clearfix col-sm-12">
								<?php echo form_textarea($register_address);?>
							</label>
						</div>
						<div class="row">
							<label class="block clearfix col-sm-12">
								<?php echo form_textarea($fact_address);?>
							</label>
						</div>
						<div class="row">
							<label class="block clearfix col-sm-12">
								<?php echo form_input($inn);?>
							</label>
						</div>
						<div class="row">
							<label class="block clearfix col-sm-12">
								<?php echo form_input($phone);?>
							</label>
						</div>
						<div class="row">
							<label class="block clearfix col-sm-12">
								<?php echo form_input($email);?>
							</label>
						</div>
						<div class="row">
							<label class="block clearfix col-sm-12">
								<?php echo form_textarea($bank);?>
							</label>
						</div>

						<div class="row">
							<div class="col-xs-12 col-sm-12">
							<button type="submit" class="col-sm-12 btn btn-sm btn-primary">
								<i class="ace-icon fa fa-key"></i>
								<span class="bigger-110">Отправить</span>
							</button>
							</div>
						</div>
					</fieldset>
				</form>

			</div>
		</div>
<?php $this->load->view('layout/banners'); ?>
		<div class="clearfix"></div>
	</div>
	<div class="clearfix"></div>
<script type="text/javascript">
$().ready(function(){

}); 
</script>
<?php $this->load->view('layout/footer'); ?>