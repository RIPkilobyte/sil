<div id="sidebar" class="sidebar responsive ace-save-state">
	<ul class="nav nav-list">
		<li class="<?php if($active == 'home') echo 'active'; ?>">
			<a href="/admin"><i class="menu-icon fa fa-home home-icon"></i><span class="menu-text">Администрирование</span></a>
		</li>

		<li class="<?php if(in_array($active, ['pages', 'edit_page'])) echo 'active open'; ?>">
			<a href="#" class="dropdown-toggle">
				<i class="menu-icon fa fa-file-o"></i>
				<span class="menu-text">Страницы</span>
				<b class="arrow fa fa-angle-down"></b>
			</a>
			<b class="arrow"></b>
			<ul class="submenu">
				<li class="<?php if($active == 'pages') echo 'active'; ?>">
					<a href="/admin/pages">
						<i class="menu-icon fa fa-caret-right"></i>Все страницы
					</a>
				</li>
				<li class="<?php if($active == 'edit_page') echo 'active'; ?>">
					<a href="/admin/edit_page">
						<i class="menu-icon fa fa-caret-right"></i>Добавить
					</a>
				</li>
			</ul>
		</li>

		<li class="<?php if(in_array($active, ['users', 'edit_user', 'groups', 'edit_group'])) echo 'active open'; ?>">
			<a href="#" class="dropdown-toggle">
				<i class="menu-icon fa fa-users"></i>
				<span class="menu-text">Пользователи</span>
				<b class="arrow fa fa-angle-down"></b>
			</a>
			<b class="arrow"></b>
			<ul class="submenu">
				<li class="<?php if($active == 'users') echo 'active'; ?>">
					<a href="/admin/users">
						<i class="menu-icon fa fa-caret-right"></i>Все пользователи
					</a>
				</li>
				<li class="<?php if($active == 'edit_user') echo 'active'; ?>">
					<a href="/admin/edit_user">
						<i class="menu-icon fa fa-caret-right"></i>Добавить
					</a>
				</li>
				<li class="<?php if($active == 'groups') echo 'active'; ?>">
					<a href="/admin/groups">
						<i class="menu-icon fa fa-caret-right"></i>Группы
					</a>
				</li>
				<li class="<?php if($active == 'edit_group') echo 'active'; ?>">
					<a href="/admin/edit_group">
						<i class="menu-icon fa fa-caret-right"></i>Добавить группу
					</a>
				</li>
			</ul>
		</li>

		<li class="<?php if(in_array($active, ['categories', 'category_edit'])) echo 'active open'; ?>">
			<a href="#" class="dropdown-toggle">
				<i class="menu-icon fa fa-book"></i>
				<span class="menu-text">Темы</span>
				<b class="arrow fa fa-angle-down"></b>
			</a>
			<b class="arrow"></b>
			<ul class="submenu">
				<li class="<?php if($active == 'categories') echo 'active'; ?>">
					<a href="/admin/categories">
						<i class="menu-icon fa fa-caret-right"></i>Темы
					</a>
				</li>
				<li class="<?php if($active == 'category_edit') echo 'active'; ?>">
					<a href="/admin/category_edit">
						<i class="menu-icon fa fa-caret-right"></i>Добавить тему
					</a>
				</li>
			</ul>
		</li>

		<li class="<?php if(in_array($active, ['solutions', 'moderate_solutions'])) echo 'active open'; ?>">
			<a href="#" class="dropdown-toggle">
				<i class="menu-icon fa fa-pencil-square-o"></i>
				<span class="menu-text">Решения</span>
				<b class="arrow fa fa-angle-down"></b>
			</a>
			<b class="arrow"></b>
			<ul class="submenu">
				<li class="<?php if($active == 'solutions') echo 'active'; ?>">
					<a href="/admin/solutions">
						<i class="menu-icon fa fa-caret-right"></i>Решения
					</a>
				</li>
				<li class="<?php if($active == 'moderate_solutions') echo 'active'; ?>">
					<a href="/admin/moderate_solutions">
						<i class="menu-icon fa fa-caret-right"></i>Модерация
					</a>
				</li>
			</ul>
		</li>

		<li class="<?php if(in_array($active, ['techfail', 'moderate_techfail'])) echo 'active open'; ?>">
			<a href="#" class="dropdown-toggle">
				<i class="menu-icon fa fa-puzzle-piece"></i>
				<span class="menu-text">Техноляп</span>
				<b class="arrow fa fa-angle-down"></b>
			</a>
			<b class="arrow"></b>
			<ul class="submenu">
				<li class="<?php if($active == 'techfail') echo 'active'; ?>">
					<a href="/admin/techfail">
						<i class="menu-icon fa fa-caret-right"></i>Техноляп
					</a>
				</li>
				<li class="<?php if($active == 'moderate_techfail') echo 'active'; ?>">
					<a href="/admin/moderate_techfail">
						<i class="menu-icon fa fa-caret-right"></i>Модерация
					</a>
				</li>
			</ul>
		</li>

		<li class="<?php if(in_array($active, ['comments', 'moderate_comments'])) echo 'active open'; ?>">
			<a href="#" class="dropdown-toggle">
				<i class="menu-icon fa fa-commenting"></i>
				<span class="menu-text">Комментарии</span>
				<b class="arrow fa fa-angle-down"></b>
			</a>
			<b class="arrow"></b>
			<ul class="submenu">
				<li class="<?php if($active == 'comments') echo 'active'; ?>">
					<a href="/admin/comments">
						<i class="menu-icon fa fa-caret-right"></i>Комментарии
					</a>
				</li>
				<li class="<?php if($active == 'moderate_comments') echo 'active'; ?>">
					<a href="/admin/moderate_comments">
						<i class="menu-icon fa fa-caret-right"></i>Модерация
					</a>
				</li>
			</ul>
		</li>

		<li class="<?php if(in_array($active, ['voting_scored', 'voting_not_scored'])) echo 'active open'; ?>">
			<a href="#" class="dropdown-toggle">
				<i class="menu-icon fa fa-tachometer"></i>
				<span class="menu-text">Голосование</span>
				<b class="arrow fa fa-angle-down"></b>
			</a>
			<b class="arrow"></b>
			<ul class="submenu">
				<li class="<?php if($active == 'voting_scored') echo 'active'; ?>">
					<a href="/admin/voting_scored">
						<i class="menu-icon fa fa-caret-right"></i>Набравшие
					</a>
				</li>
				<li class="<?php if($active == 'voting_not_scored') echo 'active'; ?>">
					<a href="/admin/voting_not_scored">
						<i class="menu-icon fa fa-caret-right"></i>Не набравшие
					</a>
				</li>
			</ul>
		</li>

		<li class="<?php if(in_array($active, ['feedbacks', 'moderate_feedbacks'])) echo 'active open'; ?>">
			<a href="#" class="dropdown-toggle">
				<i class="menu-icon fa fa-microphone"></i>
				<span class="menu-text">Обратная связь</span>
				<b class="arrow fa fa-angle-down"></b>
			</a>
			<b class="arrow"></b>
			<ul class="submenu">
				<li class="<?php if($active == 'feedbacks') echo 'active'; ?>">
					<a href="/admin/feedbacks">
						<i class="menu-icon fa fa-caret-right"></i>Все
					</a>
				</li>
				<li class="<?php if($active == 'moderate_feedbacks') echo 'active'; ?>">
					<a href="/admin/moderate_feedbacks">
						<i class="menu-icon fa fa-caret-right"></i>Новые
					</a>
				</li>
			</ul>
		</li>

	</ul>
	<div class="sidebar-toggle sidebar-collapse" id="sidebar-collapse">
		<i id="sidebar-toggle-icon" class="ace-icon fa fa-angle-double-left ace-save-state" data-icon1="ace-icon fa fa-angle-double-left" data-icon2="ace-icon fa fa-angle-double-right"></i>
	</div>
</div>