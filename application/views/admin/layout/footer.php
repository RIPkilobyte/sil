		</div><!-- /.page-content -->
	</div>
</div><!-- /.main-content -->
<div class="footer" style="margin-top:30px;">
	<div class="footer-inner">
		<div class="footer-content">
			<span class="bigger-120">
				<?php echo $title; ?> &copy; <?php echo date('Y'); ?>
			</span>
			&nbsp; &nbsp;
		</div>
	</div>
	<a href="#" id="btn-scroll-up" class="btn-scroll-up btn btn-sm btn-inverse">
		<i class="ace-icon fa fa-angle-double-up icon-only bigger-110"></i>
	</a>
</div>
</body>
</html>
