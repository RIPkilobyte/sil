<!DOCTYPE html>
<html lang="en">
	<head>
		<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />
		<meta charset="utf-8" />
		<title><?php echo $title; ?></title>
		<meta name="description" content="" />
		<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0" />

		<link rel="stylesheet" href="/assets/css/bootstrap.min.css" />
		<link rel="stylesheet" href="/assets/font-awesome/4.5.0/css/font-awesome.min.css" />
		<link rel="stylesheet" href="/assets/css/fonts.googleapis.com.css" />
		<link rel="stylesheet" href="/assets/css/ace.min.css" class="ace-main-stylesheet" id="main-ace-style" />
		<!--[if lte IE 9]>
		<link rel="stylesheet" href="/assets/css/ace-part2.min.css" class="ace-main-stylesheet" />
		<link rel="stylesheet" href="/assets/css/ace-ie.min.css" />
		<![endif]-->
		<link rel="stylesheet" href="/assets/css/ace-skins.min.css" />
		<link rel="stylesheet" href="/assets/css/ace-rtl.min.css" />
		<script src="/assets/js/ace-extra.min.js"></script>
		<!--[if lte IE 8]>
		<script src="/assets/js/html5shiv.min.js"></script>
		<script src="/assets/js/respond.min.js"></script>
		<![endif]-->

		<!--[if !IE]> -->
		<script src="/assets/js/jquery-2.1.4.min.js"></script>
		<!-- <![endif]-->
		<!--[if IE]>
		<script src="/assets/js/jquery-1.11.3.min.js"></script>
		<![endif]-->
		<script src="/assets/js/bootstrap.min.js"></script>
		<!--[if lte IE 8]>
		<script src="/assets/js/excanvas.min.js"></script>
		<![endif]-->


		<script src="/assets/js/ace-elements.min.js"></script>
		<script src="/assets/js/ace.min.js"></script>
	</head>
<body class="no-skin">
<?php $this->load->view('admin/layout/navbar'); ?>
	<div class="main-container ace-save-state" id="main-container">
<?php $this->load->view('admin/layout/sidebar'); ?>
		<div class="main-content">
			<div class="main-content-inner">
<?php $this->load->view('admin/layout/breadcrumbs'); ?>
				<div class="page-content">
					<div class="page-header">
						<h1><?php echo $page_header; ?></h1>
					</div>

