<div id="navbar" class="navbar navbar-default ace-save-state">
	<div class="navbar-container ace-save-state" id="navbar-container">
		<div class="navbar-header pull-left">
			<a href="/" class="navbar-brand">
				<small><i class="fa fa-mail-reply"></i>&nbsp;Сайт</small>
			</a>
		</div>

		<div class="navbar-buttons navbar-header pull-right" role="navigation">
			<ul class="nav ace-nav">
				<li class="gray dropdown-modal">
					<a class="dropdown-toggle" href="/admin/users" title="Активных пользователей">
						<i class="ace-icon fa fa-users"></i>
						<span class="badge badge-grey"><?php echo $users_count; ?></span>
					</a>
				</li>

				<li class="purple dropdown-modal">
					<a class="dropdown-toggle" href="/admin/moderate_solutions" title="Модерация решений">
						<i class="ace-icon fa fa-pencil-square-o"></i>
						<span class="badge badge-important"><?php echo $solutions_count; ?></span>
					</a>
				</li>

				<li class="purple dropdown-modal">
					<a class="dropdown-toggle" href="/admin/moderate_techfail" title="Модерация Техноляпов">
						<i class="ace-icon fa fa-puzzle-piece"></i>
						<span class="badge badge-important"><?php echo $techfail_count; ?></span>
					</a>
				</li>

				<li class="purple dropdown-modal">
					<a class="dropdown-toggle" href="/admin/moderate_comments" title="Модерация комментариев">
						<i class="ace-icon fa fa-commenting"></i>
						<span class="badge badge-success"><?php echo $comments_count; ?></span>
					</a>
				</li>

				<li class="green dropdown-modal">
					<a class="dropdown-toggle" href="/admin/voting_scored" title="Темы набравшие голоса">
						<i class="ace-icon fa fa-tachometer"></i>
						<span class="badge badge-success"><?php echo $voting_count; ?></span>
					</a>
				</li>

				<li class="green dropdown-modal">
					<a class="dropdown-toggle" href="/admin/moderate_feedbacks" title="Новые обращения">
						<i class="ace-icon fa fa-microphone"></i>
						<span class="badge badge-success"><?php echo $feeback_count; ?></span>
					</a>
				</li>

				<li class="light-blue dropdown-modal">
					<a data-toggle="dropdown" href="#" class="dropdown-toggle">
						<img class="nav-user-photo" width="32" height="32" src="<?php echo $avatar; ?>" />
						<span class="user-info"><small>Привет, </small><?php echo $username; ?></span>
						<i class="ace-icon fa fa-caret-down"></i>
					</a>

					<ul class="user-menu dropdown-menu-right dropdown-menu dropdown-yellow dropdown-caret dropdown-close">
						<li><a href="/cabinet"><i class="ace-icon fa fa-user"></i>Профиль</a></li>
						<li class="divider"></li>
						<li><a href="/logout"><i class="ace-icon fa fa-power-off"></i>Выход</a></li>
					</ul>
				</li>
			</ul>
		</div>
	</div>
</div>