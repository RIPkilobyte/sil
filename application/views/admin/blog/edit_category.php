<?php $this->load->view('admin/layout/header'); ?>
<script src="/assets/js/jquery-ui.custom.min.js"></script>
<script src="/assets/js/jquery.ui.touch-punch.min.js"></script>
<script src="/assets/js/markdown.min.js"></script>
<script src="/assets/js/bootstrap-markdown.min.js"></script>
<script src="/assets/js/jquery.hotkeys.index.min.js"></script>
<script src="/assets/js/bootstrap-wysiwyg.min.js"></script>
<script src="/assets/js/bootbox.js"></script>
<div class="row">
	<div class="col-xs-12">
		<form method="post" id="post_form" enctype="multipart/form-data">
			<?php if(validation_errors()): ?>
			<div class="row">
				<div class="form-group">
					<div class="col-sm-12" style="color:red; font-size:200%;">
						<?php echo validation_errors(); ?>
					</div>
				</div>
			</div>
			<br />
			<?php endif; ?>
			<?php if($record->update): ?>
			<div class="row">
				<div class="form-group">
					<div class="col-sm-12" style="color:green; font-size:200%;">
						Успешно сохранено
					</div>
				</div>
			</div>
			<br />
			<?php endif; ?>
			<div class="row">
				<div class="form-group">
					<div class="col-sm-12">
						<label style="font-size:150%;">Заголовок</label>
					</div>
					<div class="col-sm-12">
						<input type="text" name="title" id="title" placeholder="Введите отображаемое имя" value="<?php echo $record->title; ?>" class="col-sm-12" />
					</div>
				</div>
			</div>
			<br />
			<div class="row">
				<div class="form-group">
					<div class="col-sm-12">
						<label style="font-size:150%;">Адрес</label>
					</div>
					<div class="col-sm-12">
						<input type="text" name="alias" id="alias" placeholder="Введите отображаемое имя" value="<?php echo $record->alias; ?>" class="col-sm-12" />
					</div>
				</div>
			</div>
			<br />

			<div class="row">
				<div class="form-group">
					<div class="col-sm-12">
						<label style="font-size:150%;">Родительская категория</label>
					</div>
					<div class="col-sm-12">
						<select class="form-control" name="parent_id">
						<option value="0">Нет родительской категории</option>
						<?php foreach ($categories as $k => $v) {
							$selected = '';
							if($v->blog_category_id == $record->parent_id) {
								$selected="selected='selected'";
							}
							echo '<option '.$selected.' value="'.$v->blog_category_id.'">'.$v->title.'</option>';
							if(isset($v->childs) && $v->childs) {
								foreach($v->childs as $k1=>$v1) {
									$selected = '';
									if($v1->blog_category_id == $record->parent_id) {
										$selected="selected='selected'";
									}
									echo '<option '.$selected.' value="'.$v1->blog_category_id.'">-'.$v1->title.'</option>';
									if(isset($v1->childs) && $v1->childs) {
										foreach($v1->childs as $k2=>$v2) {
											$selected = '';
											if($v2->blog_category_id == $record->parent_id) {
												$selected="selected='selected'";
											}
											echo '<option '.$selected.' value="'.$v2->blog_category_id.'">--'.$v2->title.'</option>';
											if(isset($v2->childs) && $v2->childs) {
												foreach($v2->childs as $k3=>$v3) {
													$selected = '';
													if($v3->blog_category_id == $record->parent_id) {
														$selected="selected='selected'";
													}
													echo '<option '.$selected.' value="'.$v3->blog_category_id.'">---'.$v3->title.'</option>';
												}
											}
										}
									}
								}
							}
						} ?>
						</select>
					</div>
				</div>
			</div>
			<br />
			<div class="row">
				<div class="form-group">
					<div class="col-sm-12">
						<textarea class="form-control" name="description" id="description" placeholder="Текст страницы"><?php echo $record->description; ?></textarea>
					</div>
				</div>
			</div>
			<br />
			<?php if(isset($record->blog_category_id) && $record->blog_category_id): ?>
				<div class="row">
					<div class="form-group">
						<div class="col-sm-12">
							<label>Создал: <?php echo $author->username; ?> <?php echo date("d.m.Y H:i", strtotime($record->created_at)); ?></label>
						</div>
					</div>
				</div>
				<br />
				<div class="row">
					<div class="form-group">
						<div class="col-sm-12">
							<label>Изменил: <?php echo $editor->username; ?> <?php echo date("d.m.Y H:i", strtotime($record->updated_at)); ?></label>
						</div>
					</div>
				</div>
				<br />
			<?php endif; ?>

			<div class="clearfix form-actions">
				<div class="col-md-offset-5 col-md-6">
					<button class="btn btn-info" type="submit"><i class="ace-icon fa fa-check bigger-110"></i>Сохранить</button>
				</div>
			</div>
		</form>
	</div>
</div>
<script src="/assets/ckeditor/ckeditor.js"></script>
<script type="text/javascript">
$().ready(function(){
	if($("#description").length) {
		CKEDITOR.replace('description', {
			height: 600
		});
	}
});
</script>
<?php $this->load->view('admin/layout/footer'); ?>
