<?php $this->load->view('admin/layout/header'); ?>
<script src="/assets/js/jquery-ui.custom.min.js"></script>
<script src="/assets/js/jquery.ui.touch-punch.min.js"></script>
<script src="/assets/js/markdown.min.js"></script>
<script src="/assets/js/bootstrap-markdown.min.js"></script>
<script src="/assets/js/jquery.hotkeys.index.min.js"></script>
<script src="/assets/js/bootstrap-wysiwyg.min.js"></script>
<script src="/assets/js/bootbox.js"></script>
<div class="row">
	<div class="col-xs-12">
		<form method="post" id="post_form" enctype="multipart/form-data">
			<?php if(validation_errors()): ?>
			<div class="row">
				<div class="form-group">
					<div class="col-sm-12" style="color:red; font-size:200%;">
						<?php echo validation_errors(); ?>
					</div>
				</div>
			</div>
			<br />
			<?php endif; ?>
			<?php if($group->update): ?>
			<div class="row">
				<div class="form-group">
					<div class="col-sm-12" style="color:green; font-size:200%;">
						Успешно сохранено
					</div>
				</div>
			</div>
			<br />
			<?php endif; ?>
			<div class="row">
				<div class="form-group">
					<div class="col-sm-12">
						<label style="font-size:150%;">Имя</label>
					</div>
					<div class="col-sm-12">
						<input type="text" name="name" id="name" placeholder="Введите название группы" value="<?php echo $group->name; ?>" class="col-sm-12" />
					</div>
				</div>
			</div>
			<br />
			<div class="row">
				<div class="form-group">
					<div class="col-sm-12">
						<label style="font-size:150%;">Описание</label>
					</div>
					<div class="col-sm-12">
						<input type="text" name="description" id="description" placeholder="Введите описание группы" value="<?php echo $group->description; ?>" class="col-sm-12" />
					</div>
				</div>
			</div>
			<br />

			<div class="clearfix form-actions">
				<div class="col-md-offset-5 col-md-6">
					<button class="btn btn-info" type="submit"><i class="ace-icon fa fa-check bigger-110"></i>Сохранить</button>
				</div>
			</div>
		</form>
	</div>
</div>
<script type="text/javascript">
$().ready(function(){
});
</script>
<?php $this->load->view('admin/layout/footer'); ?>
