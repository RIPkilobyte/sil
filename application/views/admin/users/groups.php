<?php $this->load->view('admin/layout/header'); ?>
		<script src="/assets/js/jquery.dataTables.min.js"></script>
		<script src="/assets/js/jquery.dataTables.bootstrap.min.js"></script>
		<script src="/assets/js/dataTables.buttons.min.js"></script>
		<script src="/assets/js/buttons.flash.min.js"></script>
		<script src="/assets/js/buttons.html5.min.js"></script>
		<script src="/assets/js/buttons.print.min.js"></script>
		<script src="/assets/js/buttons.colVis.min.js"></script>
		<script src="/assets/js/dataTables.select.min.js"></script>
<div class="row">
	<div class="col-xs-12">
		<div class="row">
			<div class="col-xs-12">
					<div>
						<table id="dynamic-table" class="table table-striped table-bordered table-hover">
							<thead>
								<tr>
									<th>ID</th>
									<th>Название</th>
									<th>Описание</th>
									<th>Пользователи</th>
									<th></th>
								</tr>
							</thead>
							<tbody>
							</tbody>
						</table>
					</div>
			</div>
		</div>
	</div>
</div>
<script type="text/javascript">
$().ready(function(){
	var myTable = $('#dynamic-table').DataTable({
		ajax: {
			url: '/admin/ajax_get_groups',
			dataSrc: '',
		},
		language: {
			url: '/assets/datatable_ru.json',
		},
		columns: [
			null,
			null,
			null,
			null,
			null,
		],
		searching: true,
		"order": [[ 1, "asc" ]],
	});
});
</script>
<?php $this->load->view('admin/layout/footer'); ?>
