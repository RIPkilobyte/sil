<?php $this->load->view('admin/layout/header'); ?>
		<script src="/assets/js/jquery.dataTables.min.js"></script>
		<script src="/assets/js/jquery.dataTables.bootstrap.min.js"></script>
		<script src="/assets/js/dataTables.buttons.min.js"></script>
		<script src="/assets/js/buttons.flash.min.js"></script>
		<script src="/assets/js/buttons.html5.min.js"></script>
		<script src="/assets/js/buttons.print.min.js"></script>
		<script src="/assets/js/buttons.colVis.min.js"></script>
		<script src="/assets/js/dataTables.select.min.js"></script>
<div class="row">
	<div class="col-xs-12">
		<div class="row">
			<div class="col-xs-12">
					<div>
						<table id="dynamic-table" class="table table-striped table-bordered table-hover">
							<thead>
								<tr>
									<th>ID</th>
									<th>Имя пользователя</th>
									<th>Email</th>
									<th>Фамилия</th>
									<th>Имя</th>
									<th>Отчество</th>
									<th>Активность</th>
									<th>Последний вход</th>
									<th>Дата регистрации</th>
									<th>Группы</th>
									<th></th>
								</tr>
							</thead>
							<tbody>
							</tbody>
						</table>
					</div>
			</div>
		</div>
	</div>
</div>
<script type="text/javascript">
$().ready(function(){
	var myTable = $('#dynamic-table').DataTable({
		ajax: {
			url: '/admin/ajax_get_users_on_group',
			dataSrc: '',
			data: '',
		},
		language: {
			url: '/assets/datatable_ru.json',
		},
		columns: [
			null,
			null,
			null,
			null,
			null,
			null,
			null,
			null,
			null,
			null,
			null,
		],
		searching: true,
	});
});
</script>
<?php $this->load->view('admin/layout/footer'); ?>
