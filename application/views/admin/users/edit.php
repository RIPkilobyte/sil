<?php $this->load->view('admin/layout/header'); ?>
<script src="/assets/js/jquery-ui.custom.min.js"></script>
<script src="/assets/js/jquery.ui.touch-punch.min.js"></script>
<script src="/assets/js/markdown.min.js"></script>
<script src="/assets/js/bootstrap-markdown.min.js"></script>
<script src="/assets/js/jquery.hotkeys.index.min.js"></script>
<script src="/assets/js/bootstrap-wysiwyg.min.js"></script>
<script src="/assets/js/bootbox.js"></script>
<div class="row">
	<div class="col-xs-12">
		<form method="post" id="post_form" enctype="multipart/form-data" action="/admin/edit_user/<?php echo $user->id; ?>">
			<?php if(validation_errors()): ?>
			<div class="row">
				<div class="form-group">
					<div class="col-sm-12" style="color:red; font-size:200%;">
						<?php echo validation_errors(); ?>
					</div>
				</div>
			</div>
			<br />
			<?php endif; ?>
			<?php if($user->update): ?>
			<div class="row">
				<div class="form-group">
					<div class="col-sm-12" style="color:green; font-size:200%;">
						Успешно сохранено
					</div>
				</div>
			</div>
			<br />
			<?php endif; ?>
			<div class="row">
				<div class="form-group">
					<div class="col-sm-12">
						<label style="font-size:150%;">Отображаемое имя</label>
					</div>
					<div class="col-sm-12">
						<input type="text" name="username" id="username" placeholder="Введите отображаемое имя" value="<?php echo $user->username; ?>" class="col-sm-12" />
					</div>
				</div>
			</div>
			<br />
			<div class="row">
				<div class="form-group">
					<div class="col-sm-12">
						<label style="font-size:150%;">Email</label>
					</div>
					<div class="col-sm-12">
						<input type="text" name="email" id="email" placeholder="Введите Email" value="<?php echo $user->email; ?>" class="col-sm-12" />
					</div>
				</div>
			</div>
			<br />
			<div class="row">
				<div class="form-group">
					<div class="col-sm-12">
						<label style="font-size:150%;">Фамилия</label>
					</div>
					<div class="col-sm-12">
						<input type="text" name="last_name" id="last_name" placeholder="Введите имя пользователя" value="<?php echo $user->last_name; ?>" class="col-sm-12" />
					</div>
				</div>
			</div>
			<br />
			<div class="row">
				<div class="form-group">
					<div class="col-sm-12">
						<label style="font-size:150%;">Имя</label>
					</div>
					<div class="col-sm-12">
						<input type="text" name="first_name" id="first_name" placeholder="Введите имя пользователя" value="<?php echo $user->first_name; ?>" class="col-sm-12" />
					</div>
				</div>
			</div>
			<br />
			<div class="row">
				<div class="form-group">
					<div class="col-sm-12">
						<label style="font-size:150%;">Отчество</label>
					</div>
					<div class="col-sm-12">
						<input type="text" name="second_name" id="second_name" placeholder="Введите имя пользователя" value="<?php echo $user->second_name; ?>" class="col-sm-12" />
					</div>
				</div>
			</div>
			<br />
			<div class="row">
				<div class="form-group">
					<div class="col-sm-12">
						<div class="checkbox">
							<label>
								<?php
									$checked = 'checked="checked"';
									if(!$user->active) {
										$checked = '';
									}
								?>
								<input type="checkbox" name="active" id="active" <?php echo $checked; ?> /> Пользователь активен
							</label>
						</div>
					</div>
				</div>
			</div>
			<br />

			<div class="row">
				<div class="form-group">
					<div class="col-sm-12">
						<label style="font-size:150%;">Группы пользователя</label>
					</div>
				</div>
			</div>
			<br />
			<div class="row">
				<div class="form-group">
					<?php foreach($groups as $group): ?>
					<div class="col-sm-12">
						<div class="checkbox">
							<label>
								<?php
									$checked = '';
									foreach($user_groups as $ug) {
										if($ug->id == $group->id) {
											$checked = 'checked="checked"';
										}
									}
								?>
								<input type="checkbox" name="group[<?php echo $group->id; ?>]" id="active" <?php echo $checked; ?> /> <?php echo $group->name; ?>
							</label>
						</div>
					</div>
					<?php endforeach; ?>
				</div>
			</div>
			<br />

			<div class="row">
				<div class="form-group">
					<div class="col-sm-12">
						<label style="font-size:150%;">Пароль</label>
					</div>
					<div class="col-sm-12">
						<input type="password" name="password" id="password" placeholder="Введите пароль" class="col-sm-12" />
					</div>
				</div>
			</div>
			<br />
			<div class="row">
				<div class="form-group">
					<div class="col-sm-12">
						<label style="font-size:150%;">Подтверждение пароля</label>
					</div>
					<div class="col-sm-12">
						<input type="password" name="password_confirm" id="password_confirm" placeholder="Введите подтверждение пароля" class="col-sm-12" />
					</div>
				</div>
			</div>
			<br />

			<div class="clearfix form-actions">
				<div class="col-md-offset-5 col-md-6">
					<button class="btn btn-info" type="submit"><i class="ace-icon fa fa-check bigger-110"></i>Сохранить</button>
				</div>
			</div>
		</form>
	</div>
</div>
<script type="text/javascript">
$().ready(function(){
});
</script>
<?php $this->load->view('admin/layout/footer'); ?>
