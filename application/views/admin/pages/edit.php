<?php $this->load->view('admin/layout/header'); ?>
<script src="/assets/js/jquery-ui.custom.min.js"></script>
<script src="/assets/js/jquery.ui.touch-punch.min.js"></script>
<script src="/assets/js/markdown.min.js"></script>
<script src="/assets/js/bootstrap-markdown.min.js"></script>
<script src="/assets/js/jquery.hotkeys.index.min.js"></script>
<script src="/assets/js/bootstrap-wysiwyg.min.js"></script>
<script src="/assets/js/bootbox.js"></script>
<div class="row">
	<div class="col-xs-12">
		<form method="post" id="post_form" enctype="multipart/form-data" action="/admin/edit_page/<?php echo $page->id; ?>">
			<?php if(validation_errors()): ?>
			<div class="row">
				<div class="form-group">
					<div class="col-sm-12" style="color:red; font-size:200%;">
						<?php echo validation_errors(); ?>
					</div>
				</div>
			</div>
			<br />
			<?php endif; ?>
			<?php if($page->update): ?>
			<div class="row">
				<div class="form-group">
					<div class="col-sm-12" style="color:green; font-size:200%;">
						Успешно сохранено
					</div>
				</div>
			</div>
			<br />
			<?php endif; ?>
			<div class="row">
				<div class="form-group">
					<div class="col-sm-12">
						<label style="font-size:150%;">Заголовок страницы</label>
					</div>
					<div class="col-sm-12">
						<input type="text" name="title" id="title" placeholder="Введите заголовок страницы" value="<?php echo $page->title; ?>" class="col-sm-12" />
					</div>
				</div>
			</div>
			<br />
			<div class="row">
				<div class="form-group">
					<div class="col-sm-12">
						<label style="font-size:150%;">Адрес страницы</label>
					</div>
					<div class="col-sm-12">
						<input type="text" name="link" id="link" placeholder="Введите адрес страницы" value="<?php echo $page->link; ?>" class="col-sm-12" />
					</div>
				</div>
			</div>
			<br />
			<div class="row">
				<div class="form-group">
					<div class="col-sm-12">
						<label style="font-size:150%;">Заголовок на странице</label>
					</div>
					<div class="col-sm-12">
						<input type="text" name="text_title" id="text_title" placeholder="Введите заголовок на странице" value="<?php echo $page->text_title; ?>" class="col-sm-12" />
					</div>
				</div>
			</div>
			<br />
			<div class="row">
				<div class="form-group">
					<div class="col-sm-12">
						<textarea class="form-control" name="body" id="body" placeholder="Текст страницы"><?php echo $page->body; ?></textarea>
					</div>
				</div>
			</div>
			<br />
			<div class="row">
				<div class="form-group">
					<div class="col-sm-12">
						<label style="font-size:150%;">Meta-описание</label>
					</div>
					<div class="col-sm-12">
						<textarea class="form-control" name="description" id="description" placeholder="Meta-описание"><?php echo $page->description; ?></textarea>
					</div>
				</div>
			</div>
			<br />
			<div class="row">
				<div class="form-group">
					<div class="col-sm-12">
						<label style="font-size:150%;">Ключевые слова</label>
					</div>
					<div class="col-sm-12">
						<textarea class="form-control" name="keywords" id="keywords" placeholder="Ключевые слова"><?php echo $page->keywords; ?></textarea>
					</div>
				</div>
			</div>
			<div class="clearfix form-actions">
				<div class="col-md-offset-5 col-md-6">
					<button class="btn btn-info" type="submit"><i class="ace-icon fa fa-check bigger-110"></i>Сохранить</button>
				</div>
			</div>
		</form>
	</div>
</div>
<script src="/assets/ckeditor/ckeditor.js"></script>
<script type="text/javascript">
$().ready(function(){
	if($("#body").length) {
		CKEDITOR.replace('body', {
			height: 600
		});
	}
});
</script>
<?php $this->load->view('admin/layout/footer'); ?>
