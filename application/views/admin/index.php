<?php $this->load->view('admin/layout/header'); ?>
<div class="row">
	<div class="col-xs-12" style="font-size: 200%;">

		<div class="row">
			<div class="col-sm-2">
				<h2><u>Статистика сайта</u></h2>
				<p>Пользователей: <?php echo $users; ?></p>
				<p>Тем: <?php echo $categories; ?></p>
				<p>Решений: <?php echo $solutions; ?></p>
				<p>Техноляпов: <?php echo $techfail; ?></p>
				<p>Комментариев: <?php echo $total_comments; ?></p>
				<p>Тем на голосовании: <?php echo $voting; ?></p>
				<p>Отзывов: <?php echo $feedbacks; ?></p>
			</div>
			<div class="col-sm-5">
				<h2><u>Последние записи</u></h2>
				<div id="profile-feed-1" class="profile-feed">
					<?php 
					foreach($records as $record) {
						echo "<div class='profile-activity clearfix'>";
						echo "<div>";
						if($record->updated_at == $record->created_at) {
							echo "<img class='pull-left' src='".$record->author->avatar."' />";
							echo "<a class='user' href='/admin/edit_user/".$record->author->id."'> ".$record->author->username." </a>";
							echo " разместил ";
							if($record->type == 'solution') {
								echo "решение ";
							}
							elseif($record->type == 'techfail') {
								echo "техноляп ";
							}
						}
						else {
							echo "<img class='pull-left' src='".$record->editor->avatar."' />";
							echo "<a class='user' href='/admin/edit_user/".$record->editor->id."'> ".$record->editor->username." </a>";
							echo " изменил ";
							if($record->type == 'solution') {
								echo "решение ";
							}
							elseif($record->type == 'techfail') {
								echo "техноляп ";
							}
						}
						if($record->type == 'solution') {
							echo "<a href='/admin/solution_edit/".$record->blog_id."'>".$record->title."</a>";
						}
						elseif($record->type == 'techfail') {
							echo "<a href='/admin/techfail_edit/".$record->blog_id."'>".$record->title."</a>";
						}
						echo "<div class='time'>";
						echo "<i class='ace-icon fa fa-clock-o bigger-110'></i>";
						if($record->updated_at == $record->created_at) {
							echo " ".date("d.m.Y H:i", strtotime($record->created_at))." ";
						}
						else {
							echo " ".date("d.m.Y H:i", strtotime($record->updated_at))." ";
						}
						echo "</div>";
						echo "</div>";
						echo "</div>";
					}
					?>
				</div>

			</div>
			<div class="col-sm-5">
				<h2><u>Последние комментарии</u></h2>
				<div id="profile-feed-2" class="profile-feed">
					<?php 
					foreach($comments as $record) {
						echo "<div class='profile-activity clearfix'>";
						echo "<div>";
						if($record->updated_at == $record->created_at) {
							echo "<img class='pull-left' src='".$record->author->avatar."' />";
							echo "<a class='user' href='/admin/edit_user/".$record->author->id."'> ".$record->author->username." </a>";
							echo " прокомментировал ";
							if($record->post->type == 'solution') {
								echo "решение ";
							}
							elseif($record->post->type == 'techfail') {
								echo "техноляп ";
							}
						}
						else {
							echo "<img class='pull-left' src='".$record->editor->avatar."' />";
							echo "<a class='user' href='/admin/edit_user/".$record->editor->id."'> ".$record->editor->username." </a>";
							echo " изменил комментарий к ";
							if($record->post->type == 'solution') {
								echo "решению ";
							}
							elseif($record->post->type == 'techfail') {
								echo "техноляпу ";
							}
						}
						if($record->post->type == 'solution') {
							echo "<a href='/admin/solution_edit/".$record->blog_id."'>".$record->post->title."</a>";
						}
						elseif($record->post->type == 'techfail') {
							echo "<a href='/admin/techfail_edit/".$record->blog_id."'>".$record->post->title."</a>";
						}
						echo "<div class='time'>";
						echo "<i class='ace-icon fa fa-clock-o bigger-110'></i>";
						if($record->updated_at == $record->created_at) {
							echo " ".date("d.m.Y H:i", strtotime($record->created_at))." ";
						}
						else {
							echo " ".date("d.m.Y H:i", strtotime($record->updated_at))." ";
						}
						echo "</div>";
						echo "</div>";
						echo "</div>";
					}
					?>
				</div>
			</div>
		</div>

	</div>
</div>
<script type="text/javascript">
$().ready(function(){
	$('#profile-feed-11').ace_scroll({
		height: '500px',
	});
	$('#profile-feed-21').ace_scroll({
		height: '500px',
	});
});
</script>
<?php $this->load->view('admin/layout/footer'); ?>