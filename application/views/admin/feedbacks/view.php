<?php $this->load->view('admin/layout/header'); ?>
<script src="/assets/js/jquery-ui.custom.min.js"></script>
<script src="/assets/js/jquery.ui.touch-punch.min.js"></script>
<script src="/assets/js/markdown.min.js"></script>
<script src="/assets/js/bootstrap-markdown.min.js"></script>
<script src="/assets/js/jquery.hotkeys.index.min.js"></script>
<script src="/assets/js/bootstrap-wysiwyg.min.js"></script>
<script src="/assets/js/bootbox.js"></script>
<div class="row">
	<div class="col-xs-12">
		<form method="post" id="post_form" enctype="multipart/form-data">
			<?php if(validation_errors()): ?>
			<div class="row">
				<div class="form-group">
					<div class="col-sm-12" style="color:red; font-size:200%;">
						<?php echo validation_errors(); ?>
					</div>
				</div>
			</div>
			<br />
			<?php endif; ?>
			<?php if($record->update): ?>
			<div class="row">
				<div class="form-group">
					<div class="col-sm-12" style="color:green; font-size:200%;">
						Успешно сохранено
					</div>
				</div>
			</div>
			<br />
			<?php endif; ?>
			<div class="row">
				<div class="form-group">
					<div class="col-sm-12">
						<label style="font-size:150%;">Сообщение от: <?php echo $record->name; ?></label><br />
						<label style="font-size:150%;">Адрес: <?php echo $record->email; ?></label><br />
						<label style="font-size:150%;">Получено: <?php echo date("d.m.Y H:i", strtotime($record->created_at)); ?></label><br />
						<label style="font-size:150%;">Текст сообщения:<br /><?php echo $record->body; ?></label><br />
					</div>
				</div>
			</div>
			<br />


			<div class="row">
				<div class="form-group">
					<div class="col-sm-12">
						<div class="checkbox">
							<label>
								<?php
									$checked = 'checked="checked"';
									if(!$record->moderated) {
										$checked = '';
									}
								?>
								<input type="checkbox" name="moderated" value="1" id="moderated" <?php echo $checked; ?> /> Модерация проведена
							</label>
						</div>
					</div>
				</div>
			</div>
			<br />

			<div class="row">
				<div class="form-group">
					<div class="col-sm-12">
						<div class="">
							<label>
								<?php
									$checked = 'checked="checked"';
									if(!$record->deleted) {
										$checked = '';
									}
								?>
								<input type="checkbox" name="deleted" value="1" id="deleted" <?php echo $checked; ?> /> Запись удалена
							</label>
						</div>
					</div>
				</div>
			</div>
			<br />

			<div class="clearfix form-actions">
				<div class="col-md-offset-5 col-md-6">
					<button class="btn btn-info" type="submit"><i class="ace-icon fa fa-check bigger-110"></i>Сохранить</button>
				</div>
			</div>
		</form>
	</div>
</div>
<script type="text/javascript">
$().ready(function(){
});
</script>
<?php $this->load->view('admin/layout/footer'); ?>
