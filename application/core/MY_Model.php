<?php
class MY_Model extends CI_Model
{
	protected $meta = [
		'table' => null,
		'columns' => [
			'id' => null,
			'created' => null,
			'deleted' => null,
		],
	];
	public function __construct()
	{
		parent::__construct();
	}
	public function get($id = '', $options = [])
	{
		if(!$this->meta['table']) {
			throw new Exception('Can not complete operation because meta-table not defined.');
		}
		if(is_null($this->meta['columns']['id'])) {
			throw new Exception('ID column is not set.');
		}
		if(!preg_match('#^\d+$#', $id)) {
			throw new Exception('ID column is not correct.');
		}

		$this->db->select('*');
		$this->db->from($this->meta['table']);
		$this->db->where($this->meta['columns']['deleted'], '0');
		$this->db->where($this->meta['columns']['id'], $id);
		$this->db->limit(1);
		$rowSql = $this->db->get_compiled_select();
		$this->db->reset_query();
		if($rowSql) {
			$query = $this->db->query($rowSql);
		}
		else {
			throw new Exception('Can not complete rowSql operation.');
		}
		if($query->num_rows() == 0) {
			$row = null;
		}
		else {
			if(isset($options['result_type']) && $options['result_type'] == 'array') {
				$row = $query->row_array();
			} 
			else {
				$row = $query->result();
			}
		}
		return $row;
	}
	public function add($data = [])
	{
		if(!$this->meta['table']) {
			throw new Exception('Can not complete operation because meta-table not defined.');
		}
		$this->db->insert($this->meta['table'], $data);
		return $this->db->insert_id();
	}
	public function update($id, $data = [])
	{
		if(!$this->meta['table']) {
			throw new Exception('Can not complete operation because meta-table not defined.');
		}
		if(is_null($this->meta['columns']['id'])) {
			throw new Exception('ID column is not set.');
		}
		if(!preg_match('#^\d+$#', $id)) {
			throw new Exception('ID column is not correct.');
		}
		$this->db->where($this->meta['columns']['id'], $id);
		$this->db->update($this->meta['table'], $data);
		return $this->db->affected_rows();
	}
	public function delete($id = '', $erase = false)
	{
		if(!$this->meta['table']) {
			throw new Exception('Can not complete operation because meta-table not defined.');
		}
		if(is_null($this->meta['columns']['id'])) {
			throw new Exception('ID column is not set.');
		}
		if(!preg_match('#^\d+$#', $id)) {
			throw new Exception('ID column is not correct.');
		}
		if($erase) {
			return $this->db->delete($this->meta['table'], [$this->meta['columns']['id'] => $id]);
		}
		else {
			return $this->update($id, [$this->meta['columns']['deleted'] => 1]);
		}
	}
	public function row($filter = [], $options = [], $rowSql = '')
	{
		return $this->_row($filter, $options, $rowSql);
	}
	protected function _row($filter = [], $options = [], $rowSql = '')
	{
		if(!$this->meta['table']) {
			throw new Exception('Can not complete ROW operation because meta-table not defined.');
		}

		if(!$rowSql) {
			$where = $filter;
			if(!isset($options['with_deleted']) && isset($this->meta['columns']['deleted'])) {
				$where[$this->meta['columns']['deleted']] = 0;
			}
			$this->db->select('*');
			$this->db->from($this->meta['table']);
			$this->prepareFilterWithoutCustomSql($where);
			$this->db->limit(1);
			$rowSql = $this->db->get_compiled_select();
			$this->db->reset_query();
		}

		if($rowSql) {
			$query = $this->db->query($rowSql);
		}
		else {
			throw new Exception('Can not complete rowSql operation.');
		}

		if($query->num_rows() == 0) {
			$row = null;
		}
		else {
			if(isset($options['result_type']) && $options['result_type'] == 'array') {
				$row = $query->row_array()[0];
			} 
			else {
				$row = $query->result()[0];
			}
		}
		return $row;
	}
	protected function prepareFilterWithoutCustomSql($where)
	{
		$compare2 = [' >', ' <', '<>', '!=', '>=', '<='];
		foreach ($where as $field => $value) {
			if (is_array($value)) {
				$this->db->where_in($field, $value);
				unset($where[$field]);
			} elseif ($value === "''") {
				$this->db->where($field.' = '.$value, null, false);
				unset($where[$field]);
			} elseif ($value === 'IS NULL' || $value === 'IS NOT NULL' || $value === "''" || in_array(substr($field, -2), $compare2)) {
				$this->db->where($field.' '.$value, null, false);
				unset($where[$field]);
			}
		}
		$this->db->where($where);
		return true;
	}
}