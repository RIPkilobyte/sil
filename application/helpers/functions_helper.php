<?php

/**
 * @return boolean TRUE if page was requested throughout POST request, false otherwise
 */
function isPost()
{
	if (empty($_SERVER['REQUEST_METHOD'])) {
		return false;
	}

	return ('POST' == $_SERVER['REQUEST_METHOD']);
}
/**
 * @return boolean TRUE if page was requested throughout GET request, false otherwise
 */
function isGet()
{
	if (empty($_SERVER['REQUEST_METHOD'])) {
		return false;
	}

	return ('GET' == $_SERVER['REQUEST_METHOD']);
}
/**
 * @return boolean TRUE if page was requested throughout AJAX request, false otherwise
 */
function isAjax()
{
	return ('XMLHttpRequest' == iVal($_SERVER, 'HTTP_X_REQUESTED_WITH', 'unknown') || !empty($_REQUEST['controllerTestMode']));
}
/**
 * shortcut for (!empty($data[$key]) ? $data[$key] : $defaultValue;)
 * or (!empty($data->$key) ? $data->$key : $defaultValue;)
 * @param mixed $data
 * @param string $key
 * @param mixed $defaultValue
 */
function val($data, $key = '', $defaultValue = '')
{
	if (is_object($data)) {
		// if data is object
		return !empty($data->$key) ? $data->$key : $defaultValue;
	}
	else if (is_array($data)) {
		// if data is array
		return !empty($data[$key]) ? $data[$key] : $defaultValue;
	}
	return $defaultValue;
}
/**
 * shortcut for (isset($data[$key]) ? $data[$key] : $defaultValue;)
 * or (isset($data->$key) ? $data->$key : $defaultValue;)
 * @param mixed $data
 * @param string $key
 * @param mixed $defaultValue
 */
function iVal($data, $key = '', $defaultValue = '')
{
	if (is_object($data)) {
		// if data is object
		return isset($data->$key) ? $data->$key : $defaultValue;
	}
	else if (is_array($data)) {
		// if data is array
		return isset($data[$key]) ? $data[$key] : $defaultValue;
	}
	return $defaultValue;
}
/**
 * @return boolean TRUE if client user agent is mobile device
 */
function isMobileDevice()
{
	return preg_match('/(Android)|(webOS)|(iPad)|(iPod)|(iPhone)|(SymbianOS)/i', val($_SERVER, 'HTTP_USER_AGENT', ''));
}
/**
 * outputs passed object as json string
 *
 * @param	object	$obj
 * @param	boolean	$headers boolean - if it's true server response returned wrapped in tag <pre>
 * @return	boolean	always true
 */
function format_date($format, $date)
{
    if (empty($date)) {
        return '';
    }
}
function json_echo($obj, $headers = true, $options = 0) 
{
	$string = json_encode($obj, $options);
	if ($headers) {
		// calculate the correct content length
		$length = strlen($string) + (int)ob_get_length();
		header('Content-Type: application/json');
		header('Content-Length: '.$length);
	}
	echo $string;
	return true;
}
function build_settings($data)
{
	$out = [];
	if(is_object($data)) {
		foreach($data as $k => $v) {
			$out[$v->code] = $v->value;
		}
	}
	elseif(is_array($data)) {
		foreach($data as $k => $v) {
			if(is_object($v)) {
				$out[$v->code] = $v->value;
			}
			elseif(is_array($data)) {
				$out[$v['code']] = $v['value'];
			}
		}
	}
	return $out;
}
function build_categories($data)
{
	$out = [];
	foreach($data as $k=>$v) {
		if($v->parent_id == '0') {
			$out[$v->blog_category_id] = $v;
			unset($data[$k]);
		}
	}
	if($data) {
		foreach($data as $k=>$v) {
			if($v->parent_id != '0') {
				if(isset($out[$v->parent_id])) {
					$out[$v->parent_id]->childs[$v->blog_category_id] = $v;
					unset($data[$k]);
				}
			}
		}
	}
	if($data) {
		foreach($data as $k0=>$v0) {
			if($v->parent_id != '0') {
				foreach ($out as $k1 => $v1) {
					if(isset($v1->childs) && $v1->childs) {
						foreach ($v1->childs as $k2 => $v2) {
							if($v0->parent_id == $v2->blog_category_id) {
								$out[$v1->blog_category_id]->childs[$v2->blog_category_id]->childs[$v0->blog_category_id] = $v0;
								unset($data[$k0]);
							}
						}
					}
				}
			}
		}
	}
	if($data) {
		foreach($data as $k0=>$v0) {
			if($v->parent_id != '0') {
				foreach ($out as $k1 => $v1) {
					if(isset($v1->childs) && $v1->childs) {
						foreach ($v1->childs as $k2 => $v2) {
							if(isset($v2->childs) && $v2->childs) {
								foreach ($v2->childs as $k3 => $v3) {
									if($v0->parent_id == $v3->blog_category_id) {
										$out[$v1->blog_category_id]->childs[$v2->blog_category_id]->childs[$v3->blog_category_id]->childs[$v0->blog_category_id] = $v0;
										unset($data[$k0]);
									}
								}
							}
							
						}
					}
				}
			}
		}
	}
	return $out;
}
function rus_to_lat($string='')
{
	$cyr = ['а','б','в','г','д','е','ё','ж','з','и','й','к','л','м','н','о','п','р','с','т','у','ф','х','ц','ч','ш','щ','ъ','ы','ь','э','ю','я','А','Б','В','Г','Д','Е','Ё','Ж','З','И','Й','К','Л','М','Н','О','П','Р','С','Т','У','Ф','Х','Ц','Ч','Ш','Щ','Ъ','Ы','Ь','Э','Ю','Я'];
	$lat = ['a','b','v','g','d','e','io','zh','z','i','y','k','l','m','n','o','p','r','s','t','u','f','h','ts','ch','sh','sht','a','i','y','e','yu','ya','A','B','V','G','D','E','Io','Zh','Z','I','Y','K','L','M','N','O','P','R','S','T','U','F','H','Ts','Ch','Sh','Sht','A','I','Y','e','Yu','Ya'];
	$string = str_replace(' ', '_', $string);
	$string = str_replace('.', '', $string);
	$string = str_replace('/', '', $string);
	$string = str_replace('\\', '', $string);
	$string = str_replace('?', '', $string);
	$string = str_replace('*', '', $string);
	$string = str_replace('+', '', $string);
	$string = str_replace($cyr, $lat, trim($string));
	return $string;
}