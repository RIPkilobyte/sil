<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Search extends CI_Controller {
	function __construct()
	{
		parent::__construct();
	}
	public function index()
	{
		$data = [];
		$data['breadcrumbs'] = 'Поиск';
		$data['site_title'] = 'Поиск';
		$settings = $this->Settings_model->list_all();
		$data['settings'] = build_settings($settings);
		$data['information_pages'] = $this->Pages_model->get_information_pages();
		$categories = $this->Blog_model->get_all_categories();
		$data['categories'] = build_categories($categories);
		$this->load->view('layout/search', $data);
	}
}