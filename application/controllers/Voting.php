<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Voting extends CI_Controller {
	function __construct()
	{
		parent::__construct();
	}
	public function index()
	{
		$data = [];
		
		$settings = $this->Settings_model->list_all();
		$data['settings'] = build_settings($settings);
		$data['information_pages'] = $this->Pages_model->get_information_pages();
		$categories = $this->Blog_model->get_all_categories();
		$data['categories'] = build_categories($categories);
		$data['voting'] = $this->Blog_model->get_all_votings();
		$data['breadcrumbs'] = 'Голосование за новую категорию';
		$data['site_title'] = 'Голосование за новую категорию';
		if($data['voting']) {
			foreach($data['voting'] as $k=>$v) {
				if($v->parent_id) {
					$data['voting'][$k]->category = $this->Blog_model->get_category_by_id($v->parent_id);
					$data['voting'][$k]->title = html_entity_decode($v->title);
					$data['voting'][$k]->description = html_entity_decode($v->description);
				}
			}
		}
		$this->load->view('blog/voting', $data);
	}
	public function add()
	{
		if(!$this->ion_auth->logged_in()) {
			$this->load->view('user/not_logged', $data);
			return;
		}
		$data = [];
		$data['breadcrumbs'] = 'Добавление новой категории';
		$data['site_title'] = 'Добавление новой категории';
		$settings = $this->Settings_model->list_all();
		$data['settings'] = build_settings($settings);
		$data['information_pages'] = $this->Pages_model->get_information_pages();
		$categories = $this->Blog_model->get_all_categories();
		$data['categories'] = build_categories($categories);

		$this->load->library('form_validation');
		$this->form_validation->set_rules('title', 'Заголовок', 'trim|required');
   		$this->form_validation->set_rules('description', 'Описание', 'trim|required');
   		$this->form_validation->set_rules('category_id', 'Категория', 'trim');
		$data['title'] = htmlentities($this->input->post('title', true));
		$data['description'] = htmlentities($this->input->post('description', true));
		$data['category_id'] = $this->input->post('category_id', true);
		if($this->form_validation->run() == TRUE) {
			$add = [
				'parent_id' => $data['category_id'],
				'title' => $data['title'],
				'description' => $data['description'],
				'created_at' => date("Y-m-d H:i:s"),
				'created_id' => $this->ion_auth->get_user_id(),
				'updated_at' => date("Y-m-d H:i:s"),
				'updated_id' => $this->ion_auth->get_user_id(),
			];
			$id = $this->Blog_model->add_voting_category($add);
			if($id) {
				$this->load->view('blog/add_ok', $data);
			}
			else {
				$this->load->view('blog/add_fail', $data);
			}
			return;
		}
		$this->load->view('blog/voting_add', $data);
	}
	public function vote($id)
	{
		//TODO
		$data = [];
		$data['breadcrumbs'] = 'Голосование за новую категорию';
		$data['site_title'] = 'Голосование за новую категорию';
		$settings = $this->Settings_model->list_all();
		$data['settings'] = build_settings($settings);
		$data['information_pages'] = $this->Pages_model->get_information_pages();
		$categories = $this->Blog_model->get_all_categories();
		$data['categories'] = build_categories($categories);
		$data['category_id'] = $id;
		if(!$this->ion_auth->logged_in()) {
			$this->load->view('user/not_logged', $data);
			return;
		}
		if(!$id) {
			redirect('/404');
		}
		$vote = $this->Blog_model->check_vote($this->ion_auth->get_user_id(), $id);
		if(!$vote) {
			$this->Blog_model->vote($id, $this->ion_auth->get_user_id());
			$this->load->view('blog/vote_ok', $data);
		}
		else {
			$this->load->view('blog/voting_already', $data);
		}
	}
}