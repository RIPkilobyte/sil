<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Admin extends CI_Controller {
	public $data = [];
	protected $title = 'Административная панель';
	protected $avatar = '/assets/images/avatars/avatar2.png';
	protected $username = 'Админ';
	protected $breadcrumb = 'Главная';
	protected $active = 'home';

	function __construct()
	{
		parent::__construct();
		if(!$this->ion_auth->logged_in()) {
			redirect('/login');
		}
		$this->data['title'] = $this->title;
		$this->data['avatar'] = $this->avatar;
		$this->data['username'] = $this->username;
		$this->data['breadcrumb'] = $this->breadcrumb;
		$this->data['active'] = $this->active;
		$this->data['page_header'] = $this->title;
		$user = $this->ion_auth->user($this->ion_auth->get_user_id())->row();
		if($user->avatar_id) {
			$avatar = $this->Blog_model->get_avatar($user->avatar_id);
			if($avatar) {
				$this->data['avatar'] = $avatar->fileNameOnDisk;
			}
		}
		$this->data['username'] = $user->username;
		$this->data['users_count'] = $this->All_model->count_active_users();
		$this->data['solutions_count'] = $this->Blog_model->count_moderate_solutions();
		$this->data['techfail_count'] = $this->Blog_model->count_moderate_techfail();
		$this->data['comments_count'] = $this->Blog_model->count_moderate_comments();
		$this->data['voting_count'] = $this->Blog_model->count_scored_votings();
		$this->data['feeback_count'] = $this->All_model->count_moderate_feebacks();
	}
	public function index()
	{
		$this->data['records'] = $this->Blog_model->get_last_blogs(10);
		foreach($this->data['records'] as $k=>$v) {
			$author = $this->data['records'][$k]->author = $this->ion_auth->user($v->created_id)->row();
			$editor = $this->data['records'][$k]->editor = $this->ion_auth->user($v->updated_id)->row();
			$this->data['records'][$k]->author->avatar = $this->avatar;
			if($author->avatar_id) {
				$avatar = $this->Blog_model->get_avatar($author->avatar_id);
				if($avatar) {
					$this->data['records'][$k]->author->avatar = $avatar->fileNameOnDisk;
				}
			}
			$this->data['records'][$k]->editor->avatar = $this->avatar;
			if($editor->avatar_id) {
				$avatar = $this->Blog_model->get_avatar($editor->avatar_id);
				if($avatar) {
					$this->data['records'][$k]->editor->avatar = $avatar->fileNameOnDisk;
				}
			}
		}
		$this->data['comments'] = $this->Blog_model->get_last_comments(10);
		foreach($this->data['comments'] as $k=>$v) {
			$author = $this->data['comments'][$k]->author = $this->ion_auth->user($v->created_id)->row();
			$editor = $this->data['comments'][$k]->editor = $this->ion_auth->user($v->updated_id)->row();
			$this->data['comments'][$k]->post = $this->Blog_model->row(['blog_id'=>$v->blog_id], ['with_deleted'=>1]);
			$this->data['comments'][$k]->author->avatar = $this->avatar;
			if($author->avatar_id) {
				$avatar = $this->Blog_model->get_avatar($author->avatar_id);
				if($avatar) {
					$this->data['comments'][$k]->author->avatar = $avatar->fileNameOnDisk;
				}
			}
			$this->data['comments'][$k]->editor->avatar = $this->avatar;
			if($editor->avatar_id) {
				$avatar = $this->Blog_model->get_avatar($editor->avatar_id);
				if($avatar) {
					$this->data['comments'][$k]->editor->avatar = $avatar->fileNameOnDisk;
				}
			}
		}
		$this->data['users'] = $this->All_model->count_all_users();
		$this->data['categories'] = $this->All_model->count_all_categories();
		$this->data['solutions'] = $this->All_model->count_all_solutions();
		$this->data['techfail'] = $this->All_model->count_all_techfail();
		$this->data['total_comments'] = $this->All_model->count_all_comments();
		$this->data['voting'] = $this->All_model->count_all_voting();
		$this->data['feedbacks'] = $this->All_model->count_all_feedbacks();
		$this->load->view('admin/index', $this->data);
	}

	public function ajax_get_pages()
	{
		$pages = $this->Pages_model->get_all_pages();
		$data = [];
		if($pages) {
			foreach($pages as $v) {
				$status = '<p class="bg-success">Активна</p>';
				if($v->deleted) {
					$status = '<p class="bg-danger">Удалена</p>';
				}
				$data[] = [
					''.html_entity_decode($v->title).'',
					''.date("d.m.Y H:i", strtotime($v->updated_at)).'',
					''.date("d.m.Y H:i", strtotime($v->created_at)).'',
					$status,
					'<div class="hidden-sm hidden-xs action-buttons">
						<a class="green" href="/admin/edit_page/'.$v->page_id.'"><i class="ace-icon fa fa-pencil bigger-130"></i></a>
						<a class="red" href="/admin/change_page/'.$v->page_id.'"><i class="ace-icon fa fa-trash-o bigger-130"></i></a>
						<a href="/'.$v->link.'" target="_blank"><i class="ace-icon fa fa-external-link bigger-130"></i></a>
					</div>',
				];
			}
		}

		json_echo($data);
	}
	public function pages()
	{
		$this->data['page_header'] = 'Информационные страницы';
		$this->data['breadcrumb'] = 'Страницы';
		$this->data['active'] = 'pages';
		$this->load->view('admin/pages/list', $this->data);
	}
	public function edit_page($id=0)
	{
		$this->data['page_header'] = 'Добавить страницу';
		$this->data['page'] = new stdClass();
		$this->data['page']->id = $id;
		$this->data['page']->link = '';
		$this->data['page']->title = '';
		$this->data['page']->description = '';
		$this->data['page']->keywords = '';
		$this->data['page']->text_title = '';
		$this->data['page']->body = '';
		$update = 0;
		if(isPost()) {
			$this->load->library('form_validation');
			$this->form_validation->set_rules('link', 'Адрес страницы', 'trim|alpha_numeric|required');
			$this->form_validation->set_rules('title', 'Заголовок', 'trim|required');
			$this->form_validation->set_rules('description', 'Мета-описание', 'trim');
			$this->form_validation->set_rules('keywords', 'Мета-теги', 'trim');
			$this->form_validation->set_rules('text_title', 'Заголовок на странице', 'trim');
			$this->form_validation->set_rules('body', 'Текст', 'trim|required');
			
			$this->data['page']->link = $this->input->post('link', true);
			$this->data['page']->title = htmlentities($this->input->post('title', true));
			$this->data['page']->description = $this->input->post('description', true);
			$this->data['page']->keywords = $this->input->post('keywords', true);
			$this->data['page']->text_title = html_entity_decode($this->input->post('text_title', true));
			$this->data['page']->body = $this->input->post('body', true);
			if($this->form_validation->run() == TRUE) {
				$page = [
					'link' => $this->data['page']->link,
					'title' => $this->data['page']->title,
					'description' => $this->data['page']->description,
					'keywords' => $this->data['page']->keywords,
					'text_title' => $this->data['page']->text_title,
					'body' => $this->data['page']->body,
					'updated_at' => date("Y-m-d H:i:s"),
					'updated_id' => $this->ion_auth->get_user_id(),
				];
				if(!$id) {
					$page['created_at'] = date("Y-m-d H:i:s");
					$page['created_id'] = $this->ion_auth->get_user_id();
				}
				if($id) {
					$this->Pages_model->update($id, $page);
				}
				else {
					$id = $this->Pages_model->add($page);
				}
				$update = 1;
			}
		}
		if($id) {
			$this->data['page_header'] = 'Редактирование страницы';
			$this->data['page'] = $this->Pages_model->row(['page_id'=>$id]);
			$this->data['page']->id = $id;
		}
		$this->data['page']->update = $update;
		$this->data['breadcrumb'] = 'Страницы';
		$this->data['active'] = 'edit_page';
		$this->load->view('admin/pages/edit', $this->data);
	}
	public function change_page($id=0)
	{
		if($id) {
			$page = $this->Pages_model->row(['page_id'=>$id], ['with_deleted'=>1]);
			if($page->deleted) {
				$this->Pages_model->update($id,['deleted'=>0]);
			}
			else {
				$this->Pages_model->delete($id);
			}
		}
		redirect('/admin/pages');
	}

	public function ajax_get_users()
	{
		$users = $this->ion_auth->users()->result();
		$data = [];
		if($users) {
			foreach ($users as $k => $v) {
				$groups = $this->ion_auth->get_users_groups($v->id)->result();
				$grp = '';
				if($groups) {
					foreach($groups as $g) {
						$grp .= '<p>'.$g->name.'</p>';
					}
				}
				$active = '<p class="bg-danger">Нет</p>';
				if($v->active) {
					$active = '<p class="bg-success">Да</p>';
				}
				$last_login = '';
				if($v->last_login) {
					$last_login = date("d.m.Y H:i", $v->last_login);
				}
				$data[] = [
					$v->id,
					$v->username,
					$v->email,
					$v->last_name,
					$v->first_name,
					$v->second_name,
					$active,
					$last_login,
					date("d.m.Y H:i", $v->created_on),
					$grp,
					'<div class="hidden-sm hidden-xs action-buttons">
						<a class="green" href="/admin/edit_user/'.$v->id.'"><i class="ace-icon fa fa-pencil bigger-130"></i></a>
						<a class="red" href="/admin/change_user/'.$v->id.'"><i class="ace-icon fa fa-trash-o bigger-130"></i></a>
					</div>',
				];
			}
		}
		json_echo($data);
	}
	public function users()
	{
		$this->data['page_header'] = 'Пользователи';
		$this->data['breadcrumb'] = 'Пользователи';
		$this->data['active'] = 'users';
		$this->load->view('admin/users/list', $this->data);
	}
	public function edit_user($id=0)
	{
		$this->data['page_header'] = 'Добавить пользователя';
		$this->data['user'] = new stdClass();
		$this->data['user']->id = $id;
		$this->data['user']->username = '';
		$this->data['user']->email = '';
		$this->data['user']->last_name = '';
		$this->data['user']->first_name = '';
		$this->data['user']->second_name = '';
		$this->data['user']->active = 1;
		$this->data['user_groups'] = new stdClass();
		$update = 0;
		if(isPost()) {
			$this->load->library('form_validation');
			$this->form_validation->set_rules('username', 'Отображаемое имя', 'trim|required');
			$this->form_validation->set_rules('email', 'Email', 'trim|valid_email|required');
			$this->form_validation->set_rules('last_name', 'Фамилия', 'trim');
			$this->form_validation->set_rules('first_name', 'Имя', 'trim');
			$this->form_validation->set_rules('second_name', 'Отчество', 'trim');
			$this->form_validation->set_rules('active', 'Признак активности', 'trim');
			if($this->input->post('password', true) || $id==0) {
				$this->form_validation->set_rules('password', 'Пароль', 'required|min_length['.$this->config->item('min_password_length', 'ion_auth').']|matches[password_confirm]');
				$this->form_validation->set_rules('password_confirm', 'Подтверждение пароля', 'required');
			}
			$this->data['user']->username = $this->input->post('username', true);
			$this->data['user']->email = $this->input->post('email', true);
			$this->data['user']->last_name = $this->input->post('last_name', true);
			$this->data['user']->first_name = $this->input->post('first_name', true);
			$this->data['user']->second_name = $this->input->post('second_name', true);
			$this->data['user']->active = (int)$this->input->post('active', true);
			$this->data['user']->password = $this->input->post('password', true);
			if($this->form_validation->run() == TRUE) {
				$additional_data = [
					'username' => $this->data['user']->username,
					'email' => $this->data['user']->email,
					'last_name' => $this->data['user']->last_name,
					'first_name' => $this->data['user']->first_name,
					'second_name' => $this->data['user']->second_name,
					'active' => $this->data['user']->active,
					'password' => $this->data['user']->password,
				];
				if(!$id) {
					$page['created_on'] = time();
				}
				if($id) {
					$this->ion_auth->update($id, $additional_data);
				}
				else {
					$id = $this->ion_auth->register($this->data['user']->username, $this->data['user']->password, $this->data['user']->email, $additional_data);
					if(!$this->data['user']->active) {
						$this->ion_auth->deactivate($id);
					}
				}
				$groups = $this->input->post('group', true);
				$this->ion_auth->remove_from_group('', $id);
				if($groups) {
					foreach ($groups as $k=>$v) {
						$this->ion_auth->add_to_group($k, $id);
					}
				}
				$update = 1;
			}
		}
		if($id) {
			$this->data['page_header'] = 'Редактирование пользователя';
			$this->data['user'] = $this->ion_auth->user($id)->row();
		}
		$this->data['user']->update = $update;
		$this->data['groups'] = $this->ion_auth->groups()->result();
		if($id) {
			$this->data['user_groups'] = $this->ion_auth->get_users_groups($id)->result();
		}
		$this->data['breadcrumb'] = 'Пользователи';
		$this->data['active'] = 'edit_user';
		$this->load->view('admin/users/edit', $this->data);
	}
	public function change_user($id=0)
	{
		if($id) {
			$user = $this->ion_auth->user($id)->row();
			if($user->active) {
				$this->ion_auth->deactivate($id);
			}
			else {
				$this->ion_auth->activate($id);
			}
		}
		redirect('/admin/users');
	}

	public function ajax_get_groups()
	{
		$records = $this->ion_auth->groups()->result_array();
		$data = [];
		if($records) {
			foreach ($records as $k => $v) {
				$users = $this->All_model->count_users_on_group($v['id']);
				$data[] = [
					$v['id'],
					$v['name'],
					$v['description'],
					$users,
					'<div class="hidden-sm hidden-xs action-buttons">
						<a class="green" href="/admin/edit_group/'.$v['id'].'"><i class="ace-icon fa fa-pencil bigger-130"></i></a>
						<a class="red" href="/admin/delete_group/'.$v['id'].'"><i class="ace-icon fa fa-trash-o bigger-130"></i></a>
					</div>',
				];
			}
		}
		json_echo($data);
	}
	public function groups()
	{
		$this->data['page_header'] = 'Группы';
		$this->data['breadcrumb'] = 'Группы';
		$this->data['active'] = 'groups';
		$this->load->view('admin/users/groups', $this->data);
	}
	public function edit_group($id=0)
	{
		$this->data['page_header'] = 'Добавить группу';
		$this->data['group'] = new stdClass();
		$this->data['group']->id = $id;
		$this->data['group']->name = '';
		$this->data['group']->description = '';
		$update = 0;
		if(isPost()) {
			$this->load->library('form_validation');
			$this->form_validation->set_rules('name', 'Имя', 'trim|required');
			$this->data['group']->name = $this->input->post('name', true);
			$this->data['group']->description = $this->input->post('description', true);
			if($this->form_validation->run() == TRUE) {
				if($id) {
					$this->ion_auth->update_group($id, $this->input->post('name', true), $this->input->post('description', true));
				}
				else {
					$id = $this->ion_auth->create_group($this->input->post('name', true), $this->input->post('description', true));
				}
				$update = 1;
			}
		}
		if($id) {
			$this->data['page_header'] = 'Редактирование группы';
			$this->data['group'] = $this->ion_auth->group($id)->row();
		}
		$this->data['group']->update = $update;
		$this->data['breadcrumb'] = 'Группы';
		$this->data['active'] = 'edit_group';
		$this->load->view('admin/users/edit_group', $this->data);
	}
	public function delete_group($id=0)
	{
		if($id && !in_array($id, [1,2])) {
			$this->ion_auth->delete_group($id);
		}
		redirect('/admin/groups');
	}

	public function ajax_get_users_on_group()
	{
		$users = $this->ion_auth->users()->result();
		$data = [];
		if($users) {
			foreach ($users as $k => $v) {
				$groups = $this->ion_auth->get_users_groups($v->id)->result();
				$grp = '';
				if($groups) {
					foreach($groups as $g) {
						$grp .= '<p>'.$g->name.'</p>';
					}
				}
				$active = '<p class="bg-danger">Нет</p>';
				if($v->active) {
					$active = '<p class="bg-success">Да</p>';
				}
				$last_login = '';
				if($v->last_login) {
					$last_login = date("d.m.Y H:i", $v->last_login);
				}
				$data[] = [
					$v->id,
					$v->username,
					$v->email,
					$v->last_name,
					$v->first_name,
					$v->second_name,
					$active,
					$last_login,
					date("d.m.Y H:i", $v->created_on),
					$grp,
					'<div class="hidden-sm hidden-xs action-buttons">
						<a class="green" href="/admin/edit_user/'.$v->id.'"><i class="ace-icon fa fa-pencil bigger-130"></i></a>
						<a class="red" href="/admin/change_user/'.$v->id.'"><i class="ace-icon fa fa-trash-o bigger-130"></i></a>
					</div>',
				];
			}
		}
		json_echo($data);
	}
	public function users_on_group($id=0)
	{
		$this->data['page_header'] = 'Пользователи группы';
		$this->data['breadcrumb'] = 'Пользователи группы';
		$this->data['active'] = 'groups';
		$this->load->view('admin/users/users_on_group', $this->data);
	}

	public function ajax_get_categories()
	{
		$records = $this->Blog_model->get_all_categories(1);
		$data = [];
		if($records) {
			foreach ($records as $k => $v) {
				$parent = '';
				if($v->parent_id) {
					foreach($records as $record) {
						if($v->parent_id == $record->blog_category_id) {
							$parent = $record->title;
						}
					}
				}
				$author = $this->ion_auth->user($v->created_id)->row();
				$auser = $author->username;
				$deleted = '<p class="bg-success">Активно</p>';
				if($v->deleted) {
					$deleted = '<p class="bg-danger">Удалено</p>';
				}
				$data[] = [
					$v->blog_category_id,
					$v->title,
					$parent,
					$v->count,
					$auser,
					date("d.m.Y H:i", strtotime($v->created_at)),
					$deleted,
					'<div class="hidden-sm hidden-xs action-buttons">
						<a class="green" href="/admin/category_edit/'.$v->blog_category_id.'"><i class="ace-icon fa fa-pencil bigger-130"></i></a>
						<a class="red" href="/admin/category_change/'.$v->blog_category_id.'"><i class="ace-icon fa fa-trash-o bigger-130"></i></a>
					</div>',
				];
			}
		}
		json_echo($data);
	}
	public function categories()
	{
		$this->data['page_header'] = 'Темы';
		$this->data['breadcrumb'] = 'Темы';
		$this->data['active'] = 'categories';
		$this->load->view('admin/blog/categories', $this->data);
	}
	public function category_edit($id=0)
	{
		$this->data['page_header'] = 'Добавить тему';
		$this->data['record'] = new stdClass();
		$this->data['record']->id = $id;
		$this->data['record']->title = '';
		$this->data['record']->alias = '';
		$this->data['record']->description = '';
		$this->data['record']->parent_id = 0;
		$update = 0;
		$categories = $this->Blog_model->get_all_categories();
		$this->data['categories'] = build_categories($categories);
		if(isPost()) {
			$this->load->library('form_validation');
			$this->form_validation->set_rules('title', 'Заголовок', 'trim|required');
			$this->form_validation->set_rules('alias', 'Адрес', 'trim|alpha|required');
			$this->form_validation->set_rules('description', 'Описание', 'trim');
			$this->form_validation->set_rules('parent_id', 'Родительская категория', 'trim');
			$this->data['record']->title = htmlentities($this->input->post('title', true));
			$this->data['record']->alias = htmlentities($this->input->post('alias', true));
			$this->data['record']->description = htmlentities($this->input->post('description', true));
			$this->data['record']->parent_id = $this->input->post('parent_id', true);
			if($this->form_validation->run() == TRUE) {
				$array = [
					'title' => $this->data['record']->title,
					'alias' => $this->data['record']->alias,
					'description' => $this->data['record']->description,
					'parent_id' => $this->data['record']->parent_id,
					'updated_id' => $this->ion_auth->get_user_id(),
					'updated_at' => date("Y-m-d H:i:s"),
				];
				if($id) {
					$this->Blog_model->update_category($id, $array);
				}
				else {
					$array['created_id'] = $this->ion_auth->get_user_id();
					$array['created_at'] = date("Y-m-d H:i:s");
					$id = $this->Blog_model->add_category($array);
				}
				$update = 1;
			}
		}
		if($id) {
			$this->data['record'] = $this->Blog_model->get_category_by_id($id, 1);
			$this->data['author'] = new stdClass();
			$this->data['author'] = $this->ion_auth->user($this->data['record']->created_id)->row();
			$this->data['editor'] = new stdClass();
			$this->data['editor'] = $this->ion_auth->user($this->data['record']->updated_id)->row();
			$this->data['page_header'] = 'Редактирование темы';
			
		}
		$this->data['record']->update = $update;

		$this->data['breadcrumb'] = 'Решения';
		$this->data['active'] = 'category_edit';
		$this->load->view('admin/blog/edit_category', $this->data);
	}
	public function category_change($id)
	{
		if($id) {
			$record = $this->Blog_model->get_category_by_id($id, 1);
			if($record->deleted) {
				$this->Blog_model->undelete_category($id);
			}
			else {
				$this->Blog_model->delete_category($id);
			}
		}
		redirect('/admin/categories');
	}

	public function ajax_get_solutions()
	{
		$records = $this->Blog_model->get_all_solutions();
		$data = [];
		if($records) {
			foreach ($records as $k => $v) {
				$categories = $this->Blog_model->get_all_categories_by_blog_id($v->blog_id);
				$cats = '';
				if($categories) {
					foreach($categories as $g) {
						$cats .= '<p>'.$g->title.'</p>';
					}
				}
				$author = $this->ion_auth->user($v->created_id)->row();
				$auser = $author->username;

				$editor = $this->ion_auth->user($v->updated_id)->row();
				$euser = $editor->username;

				$moderated = '<p class="bg-danger">Нет</p>';
				if($v->moderated) {
					$moderated = '<p class="bg-success">Да</p>';
				}
				$deleted = '<p class="bg-success">Активно</p>';
				if($v->deleted) {
					$deleted = '<p class="bg-danger">Удалено</p>';
				}
				$data[] = [
					$v->blog_id,
					$v->title,
					$cats,
					$auser,
					date("d.m.Y H:i", strtotime($v->created_at)),
					$moderated,
					$euser,
					date("d.m.Y H:i", strtotime($v->updated_at)),
					$deleted,
					'<div class="hidden-sm hidden-xs action-buttons">
						<a class="green" href="/admin/solution_edit/'.$v->blog_id.'"><i class="ace-icon fa fa-pencil bigger-130"></i></a>
						<a class="red" href="/admin/solution_change/'.$v->blog_id.'"><i class="ace-icon fa fa-trash-o bigger-130"></i></a>
					</div>',
				];
			}
		}
		json_echo($data);
	}
	public function solutions()
	{
		$this->data['page_header'] = 'Решения';
		$this->data['breadcrumb'] = 'Решения';
		$this->data['active'] = 'solutions';
		$this->load->view('admin/blog/solutions', $this->data);
	}
	public function solution_edit($id=0)
	{
		if(!$id) {
			redirect('/admin/solutions');
		}
		$this->data['page_header'] = 'Редактирование решения';
		$update = 0;
		$categories = $this->Blog_model->get_all_categories();
		$this->data['categories'] = build_categories($categories);
		if(isPost()) {
			$this->load->library('form_validation');
			$this->form_validation->set_rules('title', 'Заголовок', 'trim|required');
			$this->form_validation->set_rules('description', 'Текст', 'trim|required');
			$this->form_validation->set_rules('category_id', 'Категория', 'trim|required');
			$this->form_validation->set_rules('owner', 'Кто придумал', 'trim|required');
			$this->form_validation->set_rules('moderated', 'Сообщение модерировано', 'trim');
			$this->form_validation->set_rules('deleted', 'Удалено', 'trim');
			$this->data['record'] = new stdClass();
			$this->data['record']->title = htmlentities($this->input->post('title', true));
			$this->data['record']->description = $this->input->post('description', true);
			$this->data['record']->owner = (int)$this->input->post('owner', true);
			$this->data['record']->moderated = (int)$this->input->post('moderated', true);
			$this->data['record']->deleted = (int)$this->input->post('deleted', true);
			$this->data['category_id'] = (int)$this->input->post('category_id', true);
			$this->data['category_id2'] = (int)$this->input->post('category_id2', true);
			$this->data['category_id3'] = (int)$this->input->post('category_id3', true);

			if($this->form_validation->run() == TRUE) {
				$record_categories = $this->Blog_model->get_all_categories_by_blog_id($id);
				if($record_categories) {
					foreach($record_categories as $v) {
						$this->Blog_model->remove_blog_from_category($id, $v->blog_category_id);
						$this->Blog_model->reduce_count_category($v->blog_category_id);
					}
				}
				$array = [
					'title' => $this->data['record']->title,
					'description' => $this->data['record']->description,
					'owner' => $this->data['record']->owner,
					'moderated' => $this->data['record']->moderated,
					'deleted' => $this->data['record']->deleted,
				];
				$this->Blog_model->update($id, $array);
				$this->Blog_model->add_blog_to_category($id, $this->data['category_id']);
				$this->Blog_model->increase_count_category($this->data['category_id']);
				if(isset($this->data['category_id2']) && $this->data['category_id2']) {
					$this->Blog_model->add_blog_to_category($id, $this->data['category_id2']);
					$this->Blog_model->increase_count_category($this->data['category_id2']);
				}
				if(isset($this->data['category_id3']) && $this->data['category_id3']) {
					$this->Blog_model->add_blog_to_category($id, $this->data['category_id3']);
					$this->Blog_model->increase_count_category($this->data['category_id3']);
				}

				$files = $this->Blog_model->get_files_by_entity($id);
				$input_files = $this->input->post('files', true);
				if($input_files) {
					$input_files = array_flip($input_files);
				}
				if($files) {
					foreach($files as $file) {
						if(isset($input_files[$file->attached_file_id])) {
							unset($input_files[$file->attached_file_id]);
						}
						else {
							$this->Blog_model->update_file($file->attached_file_id,['deleted' => '1']);
							unset($input_files[$file->attached_file_id]);
						}
					}
				}
				if($input_files) {
					foreach($input_files as $file_id => $v) {
						$file = $this->Blog_model->get_file($file_id);
						$update = [
							'entity_id' => $id,
							'entity' => 'blog',
						];
						$updated = $this->Blog_model->update_file($file_id, $update);
					}
				}
				$update = 1;
			}
		}
		$this->data['record'] = $this->Blog_model->row(['blog_id'=>$id], ['with_deleted'=>1]);
		$this->data['record']->update = $update;
		$this->data['files'] = $this->Blog_model->get_files_by_entity($id);
		$this->data['files_count'] = 5 - count($this->data['files']);

		$record_categories = $this->Blog_model->get_all_categories_by_blog_id($id);
		$this->data['category_id1'] = 1;
		$this->data['category_id2'] = 0;
		$this->data['category_id3'] = 0;
		$this->data['record_categories'] = [];
		$i = 1;
		if($record_categories) {
			foreach($record_categories as $v) {
				$this->data['category_id'.$i] = $v->blog_category_id;
				$i++;
			}
		}

		$this->data['breadcrumb'] = 'Решения';
		$this->data['active'] = 'solutions';
		$this->load->view('admin/blog/edit_solution', $this->data);
	}
	public function solution_change($id=0)
	{
		if($id) {
			$record = $this->Blog_model->row(['blog_id'=>$id], ['with_deleted'=>1]);
			if($record->deleted) {
				$this->Blog_model->update($id,['deleted'=>0]);
			}
			else {
				$this->Blog_model->delete($id);
			}
		}
		redirect('/admin/solutions');
	}

	public function ajax_get_moderate_solutions()
	{
		$records = $this->Blog_model->get_moderate_solutions();
		$data = [];
		if($records) {
			foreach ($records as $k => $v) {
				$categories = $this->Blog_model->get_all_categories_by_blog_id($v->blog_id);
				$cats = '';
				if($categories) {
					foreach($categories as $g) {
						$cats .= '<p>'.$g->title.'</p>';
					}
				}
				$author = $this->ion_auth->user($v->created_id)->row();
				$auser = $author->username;

				$editor = $this->ion_auth->user($v->updated_id)->row();
				$euser = $editor->username;
				$moderated = '<p class="bg-danger">Нет</p>';
				if($v->moderated) {
					$moderated = '<p class="bg-success">Да</p>';
				}
				$deleted = '<p class="bg-success">Активно</p>';
				if($v->deleted) {
					$deleted = '<p class="bg-danger">Удалено</p>';
				}
				$data[] = [
					$v->blog_id,
					$v->title,
					$cats,
					$auser,
					date("d.m.Y H:i", strtotime($v->created_at)),
					$moderated,
					$euser,
					date("d.m.Y H:i", strtotime($v->updated_at)),
					$deleted,
					'<div class="hidden-sm hidden-xs action-buttons">
						<a class="green" href="/admin/solution_edit/'.$v->blog_id.'"><i class="ace-icon fa fa-pencil bigger-130"></i></a>
						<a class="red" href="/admin/solution_change/'.$v->blog_id.'"><i class="ace-icon fa fa-trash-o bigger-130"></i></a>
					</div>',
				];
			}
		}
		json_echo($data);
	}
	public function moderate_solutions()
	{
		$this->data['page_header'] = 'Решения';
		$this->data['breadcrumb'] = 'Решения';
		$this->data['active'] = 'moderate_solutions';
		$this->load->view('admin/blog/moderate_solutions', $this->data);
	}

	public function ajax_get_techfail()
	{
		$records = $this->Blog_model->get_all_techfail();
		$data = [];
		if($records) {
			foreach ($records as $k => $v) {
				$categories = $this->Blog_model->get_all_categories_by_blog_id($v->blog_id);
				$cats = '';
				if($categories) {
					foreach($categories as $g) {
						$cats .= '<p>'.$g->title.'</p>';
					}
				}
				$author = $this->ion_auth->user($v->created_id)->row();
				$auser = $author->username;

				$editor = $this->ion_auth->user($v->updated_id)->row();
				$euser = $editor->username;

				$moderated = '<p class="bg-danger">Нет</p>';
				if($v->moderated) {
					$moderated = '<p class="bg-success">Да</p>';
				}
				$deleted = '<p class="bg-success">Активно</p>';
				if($v->deleted) {
					$deleted = '<p class="bg-danger">Удалено</p>';
				}
				$data[] = [
					$v->blog_id,
					$v->title,
					$cats,
					$auser,
					date("d.m.Y H:i", strtotime($v->created_at)),
					$moderated,
					$euser,
					date("d.m.Y H:i", strtotime($v->updated_at)),
					$deleted,
					'<div class="hidden-sm hidden-xs action-buttons">
						<a class="green" href="/admin/techfail_edit/'.$v->blog_id.'"><i class="ace-icon fa fa-pencil bigger-130"></i></a>
						<a class="red" href="/admin/techfail_change/'.$v->blog_id.'"><i class="ace-icon fa fa-trash-o bigger-130"></i></a>
					</div>',
				];
			}
		}
		json_echo($data);
	}
	public function techfail()
	{
		$this->data['page_header'] = 'Техноляп';
		$this->data['breadcrumb'] = 'Техноляп';
		$this->data['active'] = 'techfail';
		$this->load->view('admin/blog/techfail', $this->data);
	}
	public function techfail_edit($id=0)
	{
		if(!$id) {
			redirect('/admin/techfail');
		}
		$this->data['page_header'] = 'Редактирование техноляпа';
		$update = 0;
		$categories = $this->Blog_model->get_all_categories();
		$this->data['categories'] = build_categories($categories);
		if(isPost()) {
			$this->load->library('form_validation');
			$this->form_validation->set_rules('title', 'Заголовок', 'trim|required');
			$this->form_validation->set_rules('description', 'Текст', 'trim|required');
			$this->form_validation->set_rules('category_id', 'Категория', 'trim|required');
			$this->form_validation->set_rules('owner', 'Кто придумал', 'trim|required');
			$this->form_validation->set_rules('moderated', 'Сообщение модерировано', 'trim');
			$this->form_validation->set_rules('deleted', 'Удалено', 'trim');
			$this->data['record'] = new stdClass();
			$this->data['record']->title = htmlentities($this->input->post('title', true));
			$this->data['record']->description = $this->input->post('description', true);
			$this->data['record']->owner = (int)$this->input->post('owner', true);
			$this->data['record']->moderated = (int)$this->input->post('moderated', true);
			$this->data['record']->deleted = (int)$this->input->post('deleted', true);
			$this->data['category_id'] = (int)$this->input->post('category_id', true);
			$this->data['category_id2'] = (int)$this->input->post('category_id2', true);
			$this->data['category_id3'] = (int)$this->input->post('category_id3', true);
			if($this->form_validation->run() == TRUE) {
				$record_categories = $this->Blog_model->get_all_categories_by_blog_id($id);
				if($record_categories) {
					foreach($record_categories as $v) {
						$this->Blog_model->remove_blog_from_category($id, $v->blog_category_id);
						$this->Blog_model->reduce_count_category($v->blog_category_id);
					}
				}
				$array = [
					'title' => $this->data['record']->title,
					'description' => $this->data['record']->description,
					'owner' => $this->data['record']->owner,
					'moderated' => $this->data['record']->moderated,
					'deleted' => $this->data['record']->deleted,
				];
				$this->Blog_model->update($id, $array);
				$this->Blog_model->add_blog_to_category($id, $this->data['category_id']);
				$this->Blog_model->increase_count_category($this->data['category_id']);
				if(isset($this->data['category_id2']) && $this->data['category_id2']) {
					$this->Blog_model->add_blog_to_category($id, $this->data['category_id2']);
					$this->Blog_model->increase_count_category($this->data['category_id2']);
				}
				if(isset($this->data['category_id3']) && $this->data['category_id3']) {
					$this->Blog_model->add_blog_to_category($id, $this->data['category_id3']);
					$this->Blog_model->increase_count_category($this->data['category_id3']);
				}
				$files = $this->Blog_model->get_files_by_entity($id);
				$input_files = $this->input->post('files', true);
				if($input_files) {
					$input_files = array_flip($input_files);
				}
				if($files) {
					foreach($files as $file) {
						if(isset($input_files[$file->attached_file_id])) {
							unset($input_files[$file->attached_file_id]);
						}
						else {
							$this->Blog_model->update_file($file->attached_file_id,['deleted' => '1']);
							unset($input_files[$file->attached_file_id]);
						}
					}
				}
				if($input_files) {
					foreach($input_files as $file_id => $v) {
						$file = $this->Blog_model->get_file($file_id);
						$update = [
							'entity_id' => $id,
							'entity' => 'blog',
						];
						$updated = $this->Blog_model->update_file($file_id, $update);
					}
				}
				$update = 1;
			}
		}
		$this->data['record'] = $this->Blog_model->row(['blog_id'=>$id], ['with_deleted'=>1]);
		$this->data['record']->update = $update;
		$this->data['files'] = $this->Blog_model->get_files_by_entity($id);
		$this->data['files_count'] = 5 - count($this->data['files']);
		$record_categories = $this->Blog_model->get_all_categories_by_blog_id($id);
		$this->data['category_id1'] = 1;
		$this->data['category_id2'] = 0;
		$this->data['category_id3'] = 0;
		$this->data['record_categories'] = [];
		$i = 1;
		if($record_categories) {
			foreach($record_categories as $v) {
				$this->data['category_id'.$i] = $v->blog_category_id;
				$i++;
			}
		}

		$this->data['breadcrumb'] = 'Техноляп';
		$this->data['active'] = 'techfail';
		$this->load->view('admin/blog/edit_techfail', $this->data);
	}
	public function techfail_change($id=0)
	{
		if($id) {
			$record = $this->Blog_model->row(['blog_id'=>$id], ['with_deleted'=>1]);
			if($record->deleted) {
				$this->Blog_model->update($id,['deleted'=>0]);
			}
			else {
				$this->Blog_model->delete($id);
			}
		}
		redirect('/admin/techfail');
	}

	public function ajax_get_moderate_techfail()
	{
		$records = $this->Blog_model->get_moderate_techfail();
		$data = [];
		if($records) {
			foreach ($records as $k => $v) {
				$categories = $this->Blog_model->get_all_categories_by_blog_id($v->blog_id);
				$cats = '';
				if($categories) {
					foreach($categories as $g) {
						$cats .= '<p>'.$g->title.'</p>';
					}
				}
				$author = $this->ion_auth->user($v->created_id)->row();
				$auser = $author->username;

				$editor = $this->ion_auth->user($v->updated_id)->row();
				$euser = $editor->username;
				$moderated = '<p class="bg-danger">Нет</p>';
				if($v->moderated) {
					$moderated = '<p class="bg-success">Да</p>';
				}
				$deleted = '<p class="bg-success">Активно</p>';
				if($v->deleted) {
					$deleted = '<p class="bg-danger">Удалено</p>';
				}
				$data[] = [
					$v->blog_id,
					$v->title,
					$cats,
					$auser,
					date("d.m.Y H:i", strtotime($v->created_at)),
					$moderated,
					$euser,
					date("d.m.Y H:i", strtotime($v->updated_at)),
					$deleted,
					'<div class="hidden-sm hidden-xs action-buttons">
						<a class="green" href="/admin/solution_edit/'.$v->blog_id.'"><i class="ace-icon fa fa-pencil bigger-130"></i></a>
						<a class="red" href="/admin/solution_change/'.$v->blog_id.'"><i class="ace-icon fa fa-trash-o bigger-130"></i></a>
					</div>',
				];
			}
		}
		json_echo($data);
	}
	public function moderate_techfail()
	{
		$this->data['page_header'] = 'Техноляп';
		$this->data['breadcrumb'] = 'Техноляп';
		$this->data['active'] = 'moderate_techfail';
		$this->load->view('admin/blog/moderate_techfail', $this->data);
	}

	public function ajax_get_comments()
	{
		$records = $this->Blog_model->get_all_comments();
		$data = [];
		if($records) {
			foreach ($records as $k => $v) {
				$title = mb_substr($v->text, 0, 100);
				$rec = $this->Blog_model->row(['blog_id'=>$v->blog_id],['with_deleted'=>1]);
				$blog = mb_substr($rec->title, 0, 100);

				$author = $this->ion_auth->user($v->created_id)->row();
				$auser = $author->username;

				$editor = $this->ion_auth->user($v->updated_id)->row();
				$euser = $editor->username;

				$moderated = '<p class="bg-danger">Нет</p>';
				if($v->moderated) {
					$moderated = '<p class="bg-success">Да</p>';
				}
				$deleted = '<p class="bg-success">Активно</p>';
				if($v->deleted) {
					$deleted = '<p class="bg-danger">Удалено</p>';
				}
				$data[] = [
					$v->blog_comment_id,
					$blog,
					$title,
					$moderated,
					$auser,
					date("d.m.Y H:i", strtotime($v->created_at)),
					$euser,
					date("d.m.Y H:i", strtotime($v->updated_at)),
					$deleted,
					'<div class="hidden-sm hidden-xs action-buttons">
						<a class="green" href="/admin/comment_edit/'.$v->blog_comment_id.'"><i class="ace-icon fa fa-pencil bigger-130"></i></a>
						<a class="red" href="/admin/comment_change/'.$v->blog_comment_id.'"><i class="ace-icon fa fa-trash-o bigger-130"></i></a>
					</div>',
				];
			}
		}
		json_echo($data);
	}
	public function comments()
	{
		$this->data['page_header'] = 'Комментарии';
		$this->data['breadcrumb'] = 'Комментарии';
		$this->data['active'] = 'comments';
		$this->load->view('admin/blog/comments', $this->data);
	}
	public function comment_edit($id=0)
	{
		if(!$id) {
			redirect('/admin/solutions');
		}
		$this->data['page_header'] = 'Редактирование комментария';
		$update = 0;
		if(isPost()) {
			$this->load->library('form_validation');
			$this->form_validation->set_rules('text', 'Текст сообщения', 'trim|required');
			$this->form_validation->set_rules('blog_id', 'Решение', 'trim|required');
			$this->form_validation->set_rules('moderated', 'Сообщение модерировано', 'trim');
			$this->form_validation->set_rules('deleted', 'Удалено', 'trim');
			$this->data['record'] = new stdClass();
			$this->data['record']->text = htmlentities($this->input->post('text', true));
			$this->data['record']->moderated = (int)$this->input->post('moderated', true);
			$this->data['record']->deleted = (int)$this->input->post('deleted', true);
			$this->data['record']->blog_id = (int)$this->input->post('blog_id', true);
			if($this->form_validation->run() == TRUE) {
				$array = [
					'blog_id' => $this->data['record']->blog_id,
					'text' => $this->data['record']->text,
					'moderated' => $this->data['record']->moderated,
					'deleted' => $this->data['record']->deleted,
				];
				$this->Blog_model->update_comment($id, $array);
				$files = $this->Blog_model->get_files_by_entity($id);
				$input_files = $this->input->post('files', true);
				if($input_files) {
					$input_files = array_flip($input_files);
				}
				if($files) {
					foreach($files as $file) {
						if(isset($input_files[$file->attached_file_id])) {
							unset($input_files[$file->attached_file_id]);
						}
						else {
							$this->Blog_model->update_file($file->attached_file_id,['deleted' => '1']);
							unset($input_files[$file->attached_file_id]);
						}
					}
				}
				if($input_files) {
					foreach($input_files as $file_id => $v) {
						$file = $this->Blog_model->get_file($file_id);
						$update = [
							'entity_id' => $id,
							'entity' => 'blog',
						];
						$updated = $this->Blog_model->update_file($file_id, $update);
					}
				}
				$update = 1;
			}
		}
		$this->data['blogs'] = $this->Blog_model->get_solutions_title();
		foreach($this->data['blogs'] as $k=>$v) {
			$this->data['blogs'][$k]->title = mb_substr($v->title, 0, 100);
		}
		$this->data['record'] = $this->Blog_model->get_comment($id, 1);
		$this->data['record']->update = $update;
		$this->data['files'] = $this->Blog_model->get_files_by_entity($id, 'comment');
		$this->data['files_count'] = 5 - count($this->data['files']);

		$this->data['breadcrumb'] = 'Комментарии';
		$this->data['active'] = 'comments';
		$this->load->view('admin/blog/edit_comment', $this->data);
	}
	public function comment_change($id=0)
	{
		if($id) {
			$record = $this->Blog_model->get_comment($id);
			if($record->deleted) {
				$this->Blog_model->undelete_comment($id);
			}
			else {
				$this->Blog_model->delete_comment($id);
			}
		}
		redirect('/admin/comments');
	}

	public function ajax_get_moderate_comments()
	{
		$records = $this->Blog_model->get_moderate_comments();
		$data = [];
		if($records) {
			foreach ($records as $k => $v) {
				$title = mb_substr($v->text, 0, 100);
				$rec = $this->Blog_model->row(['blog_id'=>$v->blog_id],['with_deleted'=>1]);
				$blog = mb_substr($rec->title, 0, 100);

				$author = $this->ion_auth->user($v->created_id)->row();
				$auser = $author->username;

				$editor = $this->ion_auth->user($v->updated_id)->row();
				$euser = $editor->username;

				$moderated = '<p class="bg-danger">Нет</p>';
				if($v->moderated) {
					$moderated = '<p class="bg-success">Да</p>';
				}
				$deleted = '<p class="bg-success">Активно</p>';
				if($v->deleted) {
					$deleted = '<p class="bg-danger">Удалено</p>';
				}
				$data[] = [
					$v->blog_comment_id,
					$blog,
					$title,
					$moderated,
					$auser,
					date("d.m.Y H:i", strtotime($v->created_at)),
					$euser,
					date("d.m.Y H:i", strtotime($v->updated_at)),
					$deleted,
					'<div class="hidden-sm hidden-xs action-buttons">
						<a class="green" href="/admin/comment_edit/'.$v->blog_comment_id.'"><i class="ace-icon fa fa-pencil bigger-130"></i></a>
						<a class="red" href="/admin/comment_change/'.$v->blog_comment_id.'"><i class="ace-icon fa fa-trash-o bigger-130"></i></a>
					</div>',
				];
			}
		}
		json_echo($data);
	}
	public function moderate_comments()
	{
		$this->data['page_header'] = 'Комментарии';
		$this->data['breadcrumb'] = 'Комментарии';
		$this->data['active'] = 'moderate_comments';
		$this->load->view('admin/blog/moderate_comments', $this->data);
	}

	public function ajax_get_voting_scored()
	{
		$records = $this->Blog_model->get_scored_votings();
		$data = [];
		if($records) {
			foreach ($records as $k => $v) {
				$cat = '';
				if($v->parent_id) {
					$category = $this->Blog_model->get_category_by_id($v->parent_id);
					if($category) {
						$cat = $category->title;
					}
				}
				$author = $this->ion_auth->user($v->created_id)->row();
				$auser = $author->username;
				$deleted = '<p class="bg-success">Активно</p>';
				if($v->deleted) {
					$deleted = '<p class="bg-danger">Удалено</p>';
				}
				$data[] = [
					$v->title,
					$cat,
					$v->votes,
					$auser,
					date("d.m.Y H:i", strtotime($v->created_at)),
					$deleted,
					'<div class="hidden-sm hidden-xs action-buttons">
						<a class="green" href="/admin/voting_edit/'.$v->voting_id.'"><i class="ace-icon fa fa-pencil bigger-130"></i></a>
						<a class="red" href="/admin/voting_scored_change/'.$v->voting_id.'"><i class="ace-icon fa fa-trash-o bigger-130"></i></a>
						<a class="blue" title="Утвердить категорию" href="javascript:void(0)" onclick="apply_voting('.$v->voting_id.')"><i class="ace-icon fa fa-thumbs-up bigger-130"></i></a>
					</div>',
				];
			}
		}
		json_echo($data);
	}
	public function voting_scored()
	{
		$records = $this->Blog_model->get_scored_votings();
		$this->data['page_header'] = 'Голосование';
		$this->data['breadcrumb'] = 'Голосование';
		$this->data['active'] = 'voting_scored';
		$this->load->view('admin/blog/voting_scored', $this->data);
	}
	public function voting_edit($id=0)
	{
		if(!$id) {
			redirect('/admin/voting_scored');
		}
		$this->data['page_header'] = 'Редактирование голосования';
		$update = 0;
		$categories = $this->Blog_model->get_all_categories();
		$this->data['categories'] = build_categories($categories);
		if(isPost()) {
			$this->load->library('form_validation');
			$this->form_validation->set_rules('title', 'Заголовок', 'trim|required');
			$this->form_validation->set_rules('description', 'Описание', 'trim|required');
			$this->form_validation->set_rules('max_votes', 'Максимальное число голосов', 'trim|required');
			$this->form_validation->set_rules('parent_id', 'Тема', 'trim');
			$this->data['record'] = new stdClass();
			$this->data['record']->title = htmlentities($this->input->post('title', true));
			$this->data['record']->description = htmlentities($this->input->post('description', true));
			$this->data['record']->parent_id = (int)$this->input->post('parent_id', true);
			$this->data['record']->votes = (int)$this->input->post('votes', true);
			$this->data['record']->max_votes = (int)$this->input->post('max_votes', true);
			$this->data['record']->deleted = (int)$this->input->post('deleted', true);
			if($this->form_validation->run() == TRUE) {
				$record = $this->Blog_model->get_voting_by_id($id);
				$votes = $this->data['record']->votes;
				if($this->data['record']->votes > $record->max_votes) {
					$votes = $record->max_votes;
				}
				$array = [
					'title' => $this->data['record']->title,
					'description' => $this->data['record']->description,
					'parent_id' => $this->data['record']->parent_id,
					'votes' => $votes,
					'max_votes' => $this->data['record']->max_votes,
					'deleted' => $this->data['record']->deleted,
				];
				$this->Blog_model->update_voting($id, $array);
				$update = 1;
			}
		}
		$this->data['record'] = $this->Blog_model->get_voting_by_id($id);
		$this->data['record']->update = $update;

		$this->data['breadcrumb'] = 'Голосование';
		$this->data['active'] = 'voting_scored';
		$this->load->view('admin/blog/edit_voting', $this->data);
	}
	public function voting_scored_change($id=0)
	{
		if($id) {
			$record = $this->Blog_model->get_voting_by_id($id);
			if($record->deleted) {
				$this->Blog_model->undelete_voting($id);
			}
			else {
				$this->Blog_model->delete_voting($id);
			}
		}
		redirect('/admin/voting_scored');
	}
	public function voting_not_scored_change($id=0)
	{
		if($id) {
			$record = $this->Blog_model->get_voting_by_id($id);
			if($record->deleted) {
				$this->Blog_model->undelete_voting($id);
			}
			else {
				$this->Blog_model->delete_voting($id);
			}
		}
		redirect('/admin/voting_not_scored');
	}
	public function ajax_get_voting_not_scored()
	{
		$records = $this->Blog_model->get_not_scored_votings();
		$data = [];
		if($records) {
			foreach ($records as $k => $v) {
				$cat = '';
				if($v->parent_id) {
					$category = $this->Blog_model->get_category_by_id($v->parent_id);
					if($category) {
						$cat = $category->title;
					}
				}
				$author = $this->ion_auth->user($v->created_id)->row();
				$auser = $author->username;
				$deleted = '<p class="bg-success">Активно</p>';
				if($v->deleted) {
					$deleted = '<p class="bg-danger">Удалено</p>';
				}
				$data[] = [
					$v->title,
					$cat,
					$v->votes,
					$auser,
					date("d.m.Y H:i", strtotime($v->created_at)),
					$deleted,
					'<div class="hidden-sm hidden-xs action-buttons">
						<a class="green" href="/admin/voting_edit/'.$v->voting_id.'"><i class="ace-icon fa fa-pencil bigger-130"></i></a>
						<a class="red" href="/admin/voting_not_scored_change/'.$v->voting_id.'"><i class="ace-icon fa fa-trash-o bigger-130"></i></a>
						<a class="blue" title="Утвердить категорию" href="javascript:void(0)" onclick="apply_voting('.$v->voting_id.')"><i class="ace-icon fa fa-thumbs-up bigger-130"></i></a>
					</div>',
				];
			}
		}
		json_echo($data);
	}
	public function voting_not_scored()
	{
		$records = $this->Blog_model->get_scored_votings();
		$this->data['page_header'] = 'Голосование';
		$this->data['breadcrumb'] = 'Голосование';
		$this->data['active'] = 'voting_not_scored';
		$this->load->view('admin/blog/voting_not_scored', $this->data);
	}

	public function voting_apply($id)
	{
		$out = [
			'changed' => 0,
			'status' => 'fail',
			'error' => ['message' => 'Вы не авторизованы'],
		];
		if($id) {
			$record = $this->Blog_model->get_voting_by_id($id);
			if($record) {
				$alias = rus_to_lat($record->title);
				$insert = [
					'parent_id' => $record->parent_id,
					'alias' => $alias,
					'title' => $record->title,
					'description' => $record->description,
					'created_id' => $record->created_id,
					'created_at' => $record->created_at,
					'updated_id' => $this->ion_auth->get_user_id(),
					'updated_at' => date("Y-m-d H:i:s"),
				];
				$this->Blog_model->add_category($insert);
				$this->Blog_model->real_delete_voting($id);
			}
			$out = [
				'changed' => 1,
				'status' => 'ok',
				'message' => 'Тема успешно подтверждена',
			];
		}
		json_echo($out);
	}

	public function ajax_get_all_feedbacks()
	{
		$records = $this->All_model->get_all_feebacks();
		$data = [];
		if($records) {
			foreach ($records as $k => $v) {
				$moderated = '<p class="bg-danger">Нет</p>';
				if($v->moderated) {
					$moderated = '<p class="bg-success">Да</p>';
				}
				$deleted = '<p class="bg-success">Активно</p>';
				if($v->deleted) {
					$deleted = '<p class="bg-danger">Удалено</p>';
				}
				$body = mb_substr($v->body, 0, 100);
				$data[] = [
					$v->feedback_id,
					$v->name,
					$v->email,
					$moderated,
					$body,
					date("d.m.Y H:i", strtotime($v->created_at)),
					$deleted,
					'<div class="hidden-sm hidden-xs action-buttons">
						<a class="green" href="/admin/feedbacks_edit/'.$v->feedback_id.'"><i class="ace-icon fa fa-pencil bigger-130"></i></a>
						<a class="red" href="/admin/feedbacks_change/'.$v->feedback_id.'"><i class="ace-icon fa fa-trash-o bigger-130"></i></a>
					</div>',
				];
			}
		}
		json_echo($data);
	}
	public function feedbacks()
	{
		$this->data['page_header'] = 'Обратная связь';
		$this->data['breadcrumb'] = 'Обратная связь';
		$this->data['active'] = 'feedbacks';
		$this->load->view('admin/feedbacks/list', $this->data);
	}
	public function feedbacks_edit($id=0)
	{
		$this->data['page_header'] = 'Обратная связь';
		$this->data['breadcrumb'] = 'Обратная связь';
		$this->data['active'] = 'feedbacks';
		$update = 0;
		if(isPost()) {
			$update = 1;
			$array = [
				'moderated' => (int)$this->input->post('moderated', true),
				'deleted' => (int)$this->input->post('deleted', true),
			];
			$this->All_model->update_feedback($id, $array);
		}
		$this->data['record'] = $this->All_model->get_feedback_by_id($id);
		$this->data['record']->update = $update;
		$this->load->view('admin/feedbacks/view', $this->data);
	}
	public function feedbacks_change($id=0)
	{
		if($id) {
			$record = $this->All_model->get_feedback_by_id($id);
			if($record->deleted) {
				$this->All_model->undelete_feedback($id);
			}
			else {
				$this->All_model->delete_feedback($id);
			}
		}
		redirect('/admin/feedbacks');
	}

	public function ajax_get_moderate_feedbacks()
	{
		$records = $this->All_model->get_moderate_feebacks();
		$data = [];
		if($records) {
			foreach ($records as $k => $v) {
				$moderated = '<p class="bg-danger">Нет</p>';
				if($v->moderated) {
					$moderated = '<p class="bg-success">Да</p>';
				}
				$deleted = '<p class="bg-success">Активно</p>';
				if($v->deleted) {
					$deleted = '<p class="bg-danger">Удалено</p>';
				}
				$body = mb_substr($v->body, 0, 100);
				$data[] = [
					$v->feedback_id,
					$v->name,
					$v->email,
					$moderated,
					$body,
					date("d.m.Y H:i", strtotime($v->created_at)),
					$deleted,
					'<div class="hidden-sm hidden-xs action-buttons">
						<a class="green" href="/admin/feedbacks_edit/'.$v->feedback_id.'"><i class="ace-icon fa fa-pencil bigger-130"></i></a>
						<a class="red" href="/admin/feedbacks_change/'.$v->feedback_id.'"><i class="ace-icon fa fa-trash-o bigger-130"></i></a>
					</div>',
				];
			}
		}
		json_echo($data);
	}
	public function moderate_feedbacks()
	{
		$this->data['page_header'] = 'Обратная связь';
		$this->data['breadcrumb'] = 'Обратная связь';
		$this->data['active'] = 'moderate_feedbacks';
		$this->load->view('admin/feedbacks/moderate', $this->data);
	}
}