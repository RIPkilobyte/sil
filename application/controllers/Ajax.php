<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Ajax extends CI_Controller {
	function __construct()
	{
		parent::__construct();
	}
	public function index()
	{
		json_echo('не угадал');
		//https://ckeditor.com/docs/ckeditor4/latest/guide/dev_file_upload.html
	}
	public function upload_image()
	{
		$out = [
			'uploaded' => 0,
			'error' => ['message' => 'Вы не авторизованы'],
		];
		if(!$this->ion_auth->logged_in()) {
			json_echo($out);
			return;
		}
		if(isset($_FILES['upload']) && $_FILES['upload']['name'] != '') {
			$path = $this->config->item('upload_path');
			if (!is_dir($path)) {
				@mkdir($path, true);
				@chmod($path, 0755);
			}
			$config['upload_path'] = $path;
			$config['overwrite'] = false;
			$config['remove_spaces'] = true;
			$config['file_name'] = htmlentities($_FILES['upload']['name']);
			$config['allowed_types'] = 'jpg|jpeg|pdf|png|gif|doc|docx';
			$this->load->library('upload', $config);
			if (!$this->upload->do_upload('upload')) {
				$out = [
					'uploaded' => 0,
					'error' => ['message' => $this->upload->display_errors()],
				];
			} else {
				$file = $this->upload->data();
				$out = [
					'uploaded' => 1,
					'fileName' => $file['file_name'],
					'url' => $this->config->item('download_path').$file['file_name'],
				];
			}
		}
		json_echo($out);
	}
	public function ajax_upload_file()
	{
		$out = [
			'uploaded' => 0,
			'error' => ['message' => 'Вы не авторизованы'],
		];
		if(!$this->ion_auth->logged_in()) {
			json_echo($out);
			return;
		}
		if(isset($_FILES['fileupload']) && $_FILES['fileupload']['name'] != '') {
			$path = $this->config->item('upload_path');
			if (!is_dir($path)) {
				@mkdir($path, true);
				@chmod($path, 0755);
			}
			$config['upload_path'] = $path;
			$config['overwrite'] = false;
			$config['remove_spaces'] = true;
			$config['file_name'] = htmlentities($_FILES['fileupload']['name']);
			$config['allowed_types'] = 'jpg|jpeg|pdf|png|gif|doc|docx';
			$this->load->library('upload', $config);
			if (!$this->upload->do_upload('fileupload')) {
				$out = [
					'status' => 'fail',
					'uploaded' => 0,
					'error' => ['message' => $this->upload->display_errors()],
				];
			} else {
				$file = $this->upload->data();
				$insert = [
					'entity_id' => 0,
					'entity' => '',
					'is_image' => $file['is_image'],
					'file_name' => $file['file_name'],
					'fileNameOnDisk' => $this->config->item('download_path').$file['file_name'],
					'created_id' => $this->ion_auth->get_user_id(),
					'created_at' => date("Y-m-d H:i:s"),
				];
				//$file_id = $this->Files_model->add($insert);
				$file_id = $this->Blog_model->save_file($insert);
				$out = [
					'uploaded' => 1,
					'status' => 'ok',
					'url' => $this->config->item('download_path').$file['file_name'],
					'file' => [
						'file_name' => $file['file_name'],
						'file_id' => $file_id,
						'is_image' => $file['is_image'],
						'fileNameOnDisk' => $insert['fileNameOnDisk'],
					],
					
				];
			}
		}
		json_echo($out);
	}
	public function ajax_upload_url()
	{
		$out = [
			'status' => 'fail',
			'uploaded' => 0,
			'error' => ['message' => 'Вы не авторизованы'],
		];
		if(!$this->ion_auth->logged_in()) {
			json_echo($out);
			return;
		}
		if(isset($_POST['url']) && $_POST['url'] != '') {
			//TODO наверно проверку надо на валидность ссылки
			$valid = 1;
			if (!$valid) {
				$out = [
					'status' => 'fail',
					'uploaded' => 0,
					'error' => ['message' => 'Ошибка загрузки'],
				];
			} else {
				$url = $this->input->post('url', true);
				$parts = explode('/', $url);
				$file_name = end($parts);
				$insert = [
					'entity_id' => 0,
					'entity' => '',
					'is_image' => 0,
					'file_name' => $file_name,
					'fileNameOnDisk' => $url,
					'created_id' => $this->ion_auth->get_user_id(),
					'created_at' => date("Y-m-d H:i:s"),
				];
				$file_id = $this->Blog_model->save_file($insert);
				$out = [
					'status' => 'ok',
					'uploaded' => 1,
					'url' => $url,
					'file' => [
						'file_name' => $file_name,
						'file_id' => $file_id,
						'is_image' => 0,
						'fileNameOnDisk' => $url,
					],
				];
			}
		}
		json_echo($out);
	}
	public function get_user_posts()
	{
		$records = $this->Blog_model->get_blogs_by_user_id($this->ion_auth->get_user_id());
		$out = [];
		if(!$this->ion_auth->logged_in()) {
			json_echo('');
			return;
		}
		if($records) {
			foreach($records as $v) {
				$status = '<p class="bg-success">Активна</p>';
				if($v->deleted) {
					$status = '<p class="bg-danger">Удалена</p>';
				}
				$link = '';
				$type = '';
				if($v->type == 'solution') {
					$link = '/solutions/post/'.$v->blog_id;
					$type = 'Решение';
				}
				elseif($v->type == 'techfail') {
					$link = '/techfail/post/'.$v->blog_id;
					$type = 'Техноляп';
				}
				$categories = $this->Blog_model->get_categories_by_post_id($v->blog_id);
				$data['category1'] = '';
				$data['category2'] = '';
				$data['category3'] = '';
				if($categories) {
					$i=1;
					foreach ($categories as $cat) {
						$category = $this->Blog_model->get_category_by_id($cat->category_id);
						$data['category'.$i] = $category->title;
						$i++;
					}
				}
				$count_comments = $this->Blog_model->count_blog_comments_by_id($v->blog_id);
				$out[] = [
					$v->title,
					$type,
					$count_comments,
					$data['category1'],
					$data['category2'],
					$data['category3'],
					''.date("d.m.Y H:i", strtotime($v->created_at)).'',
					''.date("d.m.Y H:i", strtotime($v->updated_at)).'',
					'<div class="hidden-sm hidden-xs action-buttons">
						<a href="'.$link.'" target="_blank" title="Перейти к записи"><i class="ace-icon fa fa-external-link bigger-130"></i></a>
					</div>',
				];
			}
		}

		json_echo($out);
	}
	public function upload_avatar()
	{
		$out = [
			'uploaded' => 0,
			'error' => ['message' => 'Вы не авторизованы'],
		];
		if(!$this->ion_auth->logged_in()) {
			json_echo($out);
			return;
		}
		if(isset($_FILES['fileupload']) && $_FILES['fileupload']['name'] != '') {
			$path = $this->config->item('upload_path');
			if (!is_dir($path)) {
				@mkdir($path, true);
				@chmod($path, 0755);
			}
			$config['upload_path'] = $path;
			$config['overwrite'] = false;
			$config['remove_spaces'] = true;
			$config['file_name'] = htmlentities($_FILES['fileupload']['name']);
			$config['allowed_types'] = 'jpg|jpeg|pdf|png';
			$this->load->library('upload', $config);
			if (!$this->upload->do_upload('fileupload')) {
				$out = [
					'status' => 'fail',
					'uploaded' => 0,
					'error' => ['message' => $this->upload->display_errors()],
				];
			} else {
				$file = $this->upload->data();
				$insert = [
					'entity_id' => 0,
					'entity' => '',
					'is_image' => $file['is_image'],
					'file_name' => $file['file_name'],
					'fileNameOnDisk' => $this->config->item('download_path').$file['file_name'],
					'created_id' => $this->ion_auth->get_user_id(),
					'created_at' => date("Y-m-d H:i:s"),
				];
				//$file_id = $this->Files_model->add($insert);
				$file_id = $this->Blog_model->save_file($insert);
				$out = [
					'uploaded' => 1,
					'status' => 'ok',
					'url' => $this->config->item('download_path').$file['file_name'],
					'file' => [
						'file_name' => $file['file_name'],
						'file_id' => $file_id,
						'is_image' => $file['is_image'],
						'fileNameOnDisk' => $insert['fileNameOnDisk'],
					],
					
				];
			}
		}
		json_echo($out);
	}
}