<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Techfail_categories extends CI_Controller {
	function __construct()
	{
		parent::__construct();
	}
	public function index()
	{
		$data = [];
		$data['breadcrumbs'] = 'Техноляп';
		$data['site_title'] = 'Техноляп';
		$settings = $this->Settings_model->list_all();
		$data['settings'] = build_settings($settings);
		$categories = $this->Blog_model->get_all_categories();
		$data['categories'] = build_categories($categories);
		
		$parent_categories = [];
		$categories = $this->Blog_model->get_all_categories();

		if($categories) {
			foreach($categories as $k=>$v) {
				if($v->parent_id != '0') {
					$category = $this->Blog_model->get_category_by_id($v->parent_id);
					if($category) {
						$parent_categories[] = $category;
						if($category->parent_id != '0') {
							$category2 = $this->Blog_model->get_category_by_id($category->parent_id);
							if($category2) {
								$parent_categories[] = $category2;
								if($category2->parent_id != '0') {
									$category3 = $this->Blog_model->get_category_by_id($category2->parent_id);
									if($category3) {
										$parent_categories[] = $category3;
										if($category3->parent_id != '0') {
											$category4 = $this->Blog_model->get_category_by_id($category3->parent_id);
											if($category4) {
												$parent_categories[] = $category4;
											}
										}
									}
								}
							}
						}
					}
				}
			}
		}
		$data['all_categories'] = build_categories(array_merge($categories, $parent_categories));
		
		$this->load->view('blog/techfail_categories', $data);
	}
	public function category($alias = '')
	{
		if(!$alias) {
			redirect('/404');
		}
		$data = [];
		$data['breadcrumbs'] = '';
		$data['site_title'] = '';
		$settings = $this->Settings_model->list_all();
		$data['settings'] = build_settings($settings);
		$categories = $this->Blog_model->get_all_categories();
		$data['categories'] = build_categories($categories);

		$category = $this->Blog_model->get_category_by_alias($alias);
		$data['posts'] = [];
		$data['all_categories'] = [];
		if($category) {
			$data['breadcrumbs'] = 'Техноляпы '.$category[0]->title;
			$data['site_title'] = 'Техноляпы '.$category[0]->title;
			$top_category_id = $category[0]->blog_category_id;
			$data['blog_category_id'] = $top_category_id;
			$data['all_categories'][$top_category_id] = $category[0];
			$data['category'] = $category[0];
			$data['all_categories'][$top_category_id]->has_posts = 0;
			$childs = [];
			$posts = [];
			$blog_to_category = $this->Blog_model->get_techfail_blogs_by_category_id($category[0]->blog_category_id);

			if($blog_to_category) {
				$data['all_categories'][$top_category_id]->has_posts = 1;
				$ids = [];
				$data['all_categories'][$top_category_id]->posts = [];
				foreach($blog_to_category as $key=>$value) {
					$posts[$value->blog_id] = $value;
					$data['all_categories'][$top_category_id]->posts[$value->blog_id] = $value;
				}
			}
			$childs = $this->Blog_model->get_categories_by_parent($top_category_id);
			
			if($childs) {
				$data['all_categories'][$top_category_id]->childs = $childs;
				foreach($childs as $k=>$v) {
					$data['all_categories'][$top_category_id]->childs[$k]->has_posts = 0;
					$blog_to_category = $this->Blog_model->get_techfail_blogs_by_category_id($v->blog_category_id);
					if($blog_to_category) {
						$data['all_categories'][$top_category_id]->childs[$k]->has_posts = 1;
						$ids = [];
						$data['all_categories'][$top_category_id]->childs[$k]->posts = [];
						foreach($blog_to_category as $key=>$value) {
							$posts[$value->blog_id] = $value;
							$data['all_categories'][$top_category_id]->childs[$k]->posts[$value->blog_id] = $value;
						}
					}
					$childs = $this->Blog_model->get_categories_by_parent($v->blog_category_id);
					if($childs) {
						$data['all_categories'][$top_category_id]->childs[$k]->childs = $childs;
						foreach($childs as $k1=>$v1) {
							$data['all_categories'][$top_category_id]->childs[$k]->childs[$k1]->has_posts = 0;
							$blog_to_category = $this->Blog_model->get_techfail_blogs_by_category_id($v1->blog_category_id);
							if($blog_to_category) {
								$data['all_categories'][$top_category_id]->childs[$k]->childs[$k1]->has_posts = 1;
								$ids = [];
								$data['all_categories'][$top_category_id]->childs[$k]->childs[$k1]->posts = [];
								foreach($blog_to_category as $key=>$value) {
									$posts[$value->blog_id] = $value;
									$data['all_categories'][$top_category_id]->childs[$k]->childs[$k1]->posts[$value->blog_id] = $value;
								}
							}
							$childs = $this->Blog_model->get_categories_by_parent($v1->blog_category_id);
							if($childs) {
								$data['all_categories'][$top_category_id]->childs[$k]->childs[$k1]->childs = $childs;
								foreach($childs as $k2=>$v2) {
									$data['all_categories'][$top_category_id]->childs[$k]->childs[$k1]->childs[$k2]->has_posts = 0;
									$blog_to_category = $this->Blog_model->get_techfail_blogs_by_category_id($v2->blog_category_id);
									if($blog_to_category) {
										$data['all_categories'][$top_category_id]->childs[$k]->childs[$k1]->childs[$k2]->has_posts = 1;
										$ids = [];
										$data['all_categories'][$top_category_id]->childs[$k]->childs[$k1]->childs[$k2]->posts = [];
										foreach($blog_to_category as $key=>$value) {
											$posts[$value->blog_id] = $value;
											$data['all_categories'][$top_category_id]->childs[$k]->childs[$k1]->childs[$k2]->posts[$value->blog_id] = $value;
										}
									}
								}
							}
						}
					}
				}
			}
			if($posts) {
				$ids = [];
				foreach ($posts as $k => $v) {
					$ids[] = $v->blog_id;
				}
				if($ids) {
					$ids = implode(',', $ids);
					$tmp = $this->Blog_model->get_blog_posts_by_ids($ids);
					if($tmp) {
						foreach($tmp as $k=>$v) {
							$data['posts'][$v->blog_id] = $v;
						}
					}
				}
			}
		}
		else {
			redirect('/404');
		}
		$this->load->view('blog/techfail_categories', $data);
	}
}