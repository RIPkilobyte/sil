<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Solutions extends CI_Controller {
	function __construct()
	{
		parent::__construct();
	}
	public function add($id=0)
	{
		$data = [];
		$data['breadcrumbs'] = 'Добавление полезного средства';
		$data['site_title'] = 'Добавление полезного средства';
		$settings = $this->Settings_model->list_all();
		$data['settings'] = build_settings($settings);
		$categories = $this->Blog_model->get_all_categories();
		$data['information_pages'] = $this->Pages_model->get_information_pages();
		$data['categories'] = build_categories($categories);

		if(!$this->ion_auth->logged_in()) {
			$this->load->view('user/not_logged', $data);
			return;
		}
		$data['files_count'] = 5;
		$this->load->library('form_validation');
		$this->form_validation->set_rules('title', 'Заголовок - ', 'trim|required');
		$this->form_validation->set_rules('description', 'Описание - ', 'trim|required');
		$this->form_validation->set_rules('category_id1', 'Главная тема - ', 'trim|required');
		//$this->form_validation->set_rules('visible', 'Видимость для всех или только для меня и администрации', 'required');
		$this->form_validation->set_rules('owner', 'Это изобретение Ваше или нет - ', 'trim|required');

		$data['title'] = htmlentities($this->input->post('title', true), ENT_QUOTES, "UTF-8", false);
		$data['description'] = htmlentities($this->input->post('description', true));
		$data['category_id1'] = (int)$this->input->post('category_id1', true);
		$data['category_id2'] = (int)$this->input->post('category_id2', true);
		$data['category_id3'] = (int)$this->input->post('category_id3', true);
		//$data['visible'] = (int)$this->input->post('visible', true);
		$data['owner'] = (int)$this->input->post('owner', true);
		if(isGet() && !isPost()) {
			$data['category_id1'] = $this->input->get('category_id', true);
		}
		
		if($this->form_validation->run() == TRUE) {
			$add = [
				'title' => $data['title'],
				'description' => $data['description'],
				'type' => 'solution',
				//'visible' => $data['visible'],
				'owner' => $data['owner'],
				'created_at' => date("Y-m-d H:i:s"),
				'created_id' => $this->ion_auth->get_user_id(),
				'updated_at' => date("Y-m-d H:i:s"),
				'updated_id' => $this->ion_auth->get_user_id(),
			];
			$id = $this->Blog_model->add($add);

			if($id) {
				$files = $this->input->post('files', true);
				if($files) {
					foreach($files as $file) {
						$update = [
							'entity_id' => $id,
							'entity' => 'blog',
						];
						$res = $this->Blog_model->update_file($file, $update);
					}
				}
				$this->Blog_model->add_blog_to_category($id, $data['category_id1']);
				$this->Blog_model->increase_count_category($data['category_id1']);
				if(isset($data['category_id2']) && $data['category_id2']) {
					$this->Blog_model->add_blog_to_category($id, $data['category_id2']);
					$this->Blog_model->increase_count_category($data['category_id2']);
				}
				if(isset($data['category_id3']) && $data['category_id3']) {
					$this->Blog_model->add_blog_to_category($id, $data['category_id3']);
					$this->Blog_model->increase_count_category($data['category_id3']);
				}
				$data['return_url'] = '/solutions/post/'.$id;
				$this->load->view('blog/add_ok', $data);
			}
			else {
				$this->load->view('blog/add_fail', $data);
			}
			return;
		}
		$this->load->view('blog/solution_add', $data);
	}
	public function category($alias = '')
	{
		//показывает только категории с постами
		if(!$alias) {
			redirect('/404');
		}
		$data = [];
		$data['breadcrumbs'] = '';
		$data['site_title'] = '';
		$settings = $this->Settings_model->list_all();
		$data['settings'] = build_settings($settings);
		$data['information_pages'] = $this->Pages_model->get_information_pages();
		$categories = $this->Blog_model->get_all_categories();
		$data['categories'] = build_categories($categories);

		$category = $this->Blog_model->get_category_by_alias($alias);
		$data['childs'] = [];
		$data['posts'] = [];
		if($category) {
			$data['category'] = $category[0];
			$data['breadcrumbs'] = $data['category']->title;
			$data['site_title'] = $data['category']->title;
			$data['category']->has_posts = 0;
			$childs = [];
			$posts = [];
			$blog_to_category = $this->Blog_model->get_solution_blogs_by_category_id($data['category']->blog_category_id);
			if($blog_to_category) {
				$data['category']->has_posts = 1;
				$ids = [];
				$data['category']->posts = [];
				foreach($blog_to_category as $key=>$value) {
					$posts[$value->blog_id] = $value;
					$data['category']->posts[$value->blog_id] = $value;
				}
			}

			$data['childs'] = $this->Blog_model->get_categories_by_parent($data['category']->blog_category_id);
			if($data['childs']) {
				foreach($data['childs'] as $k=>$v) {
					$data['childs'][$k]->has_posts = 0;
					$blog_to_category = $this->Blog_model->get_solution_blogs_by_category_id($v->blog_category_id);
					if($blog_to_category) {
						$data['childs'][$k]->has_posts = 1;
						$ids = [];
						$data['childs'][$k]->posts = [];
						foreach($blog_to_category as $key=>$value) {
							$posts[$value->blog_id] = $value;
							$data['childs'][$k]->posts[$value->blog_id] = $value;
						}
					}
					$childs = $this->Blog_model->get_categories_by_parent($v->blog_category_id);
					if($childs) {
						$data['childs'][$k]->childs = $childs;
						foreach($childs as $k1=>$v1) {
							$data['childs'][$k]->childs[$k1]->has_posts = 0;
							$blog_to_category = $this->Blog_model->get_solution_blogs_by_category_id($v1->blog_category_id);
							if($blog_to_category) {
								$data['childs'][$k]->childs[$k1]->has_posts = 1;
								$data['childs'][$k]->has_posts = 1;
								$data['childs'][$k]->childs[$k1]->posts = $blog_to_category;
								$ids = [];
								$data['childs'][$k]->childs[$k1]->posts = [];
								foreach($blog_to_category as $key=>$value) {
									$posts[$value->blog_id] = $value;
									$data['childs'][$k]->childs[$k1]->posts[$value->blog_id] = $value;
								}
							}
							$childs = $this->Blog_model->get_categories_by_parent($v1->blog_category_id);
							if($childs) {
								$data['childs'][$k]->childs[$k1]->childs = $childs;
								foreach($childs as $k2=>$v2) {
									$data['childs'][$k]->childs[$k1]->childs[$k2]->has_posts = 0;
									$blog_to_category = $this->Blog_model->get_solution_blogs_by_category_id($v2->blog_category_id);
									if($blog_to_category) {
										$data['childs'][$k]->childs[$k1]->childs[$k2]->has_posts = 1;
										$data['childs'][$k]->childs[$k1]->has_posts = 1;
										$data['childs'][$k]->has_posts = 1;
										$ids = [];
										$data['childs'][$k]->childs[$k1]->childs[$k2]->posts = [];
										foreach($blog_to_category as $key=>$value) {
											$posts[$value->blog_id] = $value;
											$data['childs'][$k]->childs[$k1]->childs[$k2]->posts[$value->blog_id] = $value;
										}
									}
									$childs = $this->Blog_model->get_categories_by_parent($v2->blog_category_id);
									if($childs) {
										$data['childs'][$k]->childs[$k1]->childs[$k2]->childs = $childs;
										foreach($childs as $k3=>$v3) {
											$data['childs'][$k]->childs[$k1]->childs[$k2]->childs[$k3]->has_posts = 0;
											$blog_to_category = $this->Blog_model->get_solution_blogs_by_category_id($v3->blog_category_id);
											if($blog_to_category) {
												$data['childs'][$k]->childs[$k1]->childs[$k2]->childs[$k3]->has_posts = 1;
												$data['childs'][$k]->childs[$k1]->childs[$k2]->has_posts = 1;
												$data['childs'][$k]->childs[$k1]->has_posts = 1;
												$data['childs'][$k]->has_posts = 1;
												$ids = [];
												$data['childs'][$k]->childs[$k1]->childs[$k2]->childs[$k3]->posts = [];
												foreach($blog_to_category as $key=>$value) {
													$posts[$value->blog_id] = $value;
													$data['childs'][$k]->childs[$k1]->childs[$k2]->childs[$k3]->posts[$value->blog_id] = $value;
												}
											}
										}
									}
								}
							}
						}
					}
				}
			}
			if($posts) {
				$ids = [];
				foreach ($posts as $k => $v) {
					$ids[] = $v->blog_id;
				}
				if($ids) {
					$ids = implode(',', $ids);
					$tmp = $this->Blog_model->get_blog_posts_by_ids($ids);
					if($tmp) {
						foreach($tmp as $k=>$v) {
							$data['posts'][$v->blog_id] = $v;
						}
					}
				}
			}
		}
		else {
			redirect('/404');
		}
		$this->load->view('blog/solution_category', $data);
	}
	public function index()
	{
		//показывает только категории с постами
		$data = [];
		$data['breadcrumbs'] = 'Полезные решения';
		$data['site_title'] = 'Полезные решения';
		$settings = $this->Settings_model->list_all();
		$data['settings'] = build_settings($settings);
		$categories = $this->Blog_model->get_all_categories();
		$data['categories'] = build_categories($categories);
		
		$parent_categories_with_posts = [];
		$categories_with_posts = $this->Blog_model->get_categories_with_posts();

		if($categories_with_posts) {
			foreach($categories_with_posts as $k=>$v) {
				if($v->parent_id != '0') {
					$category = $this->Blog_model->get_category_by_id($v->parent_id);
					if($category) {
						$parent_categories_with_posts[] = $category;
						if($category->parent_id != '0') {
							$category2 = $this->Blog_model->get_category_by_id($category->parent_id);
							if($category2) {
								$parent_categories_with_posts[] = $category2;
								if($category2->parent_id != '0') {
									$category3 = $this->Blog_model->get_category_by_id($category2->parent_id);
									if($category3) {
										$parent_categories_with_posts[] = $category3;
										if($category3->parent_id != '0') {
											$category4 = $this->Blog_model->get_category_by_id($category3->parent_id);
											if($category4) {
												$parent_categories_with_posts[] = $category4;
											}
										}
									}
								}
							}
						}
					}
				}
			}
		}
		$data['categories_with_posts'] = build_categories(array_merge($categories_with_posts, $parent_categories_with_posts));
		unset($data['categories_with_posts'][11]);

		$this->load->view('blog/solution_index', $data);
	}
	public function post($id = '')
	{
		if(!$id) {
			redirect('/404');
		}
		$data = [];
		$data['breadcrumbs'] = '';
		$data['site_title'] = '';
		$settings = $this->Settings_model->list_all();
		$data['settings'] = build_settings($settings);
		$categories = $this->Blog_model->get_all_categories();
		$data['categories'] = build_categories($categories);
		$post = $this->Blog_model->get($id);
		if($post) {
			$data['post'] = $post[0];
			$this->load->library('form_validation');
			$this->form_validation->set_rules('comment', 'Текст сообщения', 'trim|required');
			if($this->form_validation->run() == TRUE) {
				if($this->ion_auth->logged_in()) {
					$insert = [
						'blog_id' => $data['post']->blog_id,
						'text' => $this->input->post('comment', true),
						'created_id' => $this->ion_auth->get_user_id(),
						'created_at' => date("Y-m-d H:i:s"),
						'updated_id' => $this->ion_auth->get_user_id(),
						'updated_at' => date("Y-m-d H:i:s"),
					];
					$data['comment_id'] = $this->Blog_model->add_comment($insert);
					if($data['comment_id']) {
						$files = $this->input->post('files', true);
						if($files) {
							foreach($files as $k => $file_id) {
								$file = $this->Blog_model->get_file($file_id);
								$update = [
									'entity_id' => $data['comment_id'],
									'entity' => 'comment',
								];
								$updated = $this->Blog_model->update_file($file_id, $update);
							}
						}
					}
				}
				redirect('/solutions/post/'.$id);
			}
		
			$data['breadcrumbs'] = html_entity_decode($data['post']->title);
			$data['site_title'] = html_entity_decode($data['post']->title);
			$data['author'] = $this->ion_auth->user($data['post']->created_id)->row();
			$data['author']->avatar = $this->Blog_model->get_avatar($data['author']->avatar_id);
			$data['post']->title = html_entity_decode($data['post']->title);
			$data['post']->description = html_entity_decode($data['post']->description);
			$data['files_count'] = 5;
			if($data['post']->created_at != $data['post']->updated_at) {
				$data['post']->edited = $data['author'];
				if($data['post']->created_id != $data['post']->updated_id) {
					$data['post']->edited = $this->ion_auth->user($data['post']->updated_id)->row();
				}
			}
			$data['post']->files = $this->Blog_model->get_files_by_entity($id);
			$blogs = $this->Blog_model->count_blogs_by_user_id($data['post']->created_id);
			$comments = $this->Blog_model->count_blog_comments_by_user_id($data['post']->created_id);
			$data['author']->comments = $blogs + $comments;
			$data['count_comments'] = $this->Blog_model->count_blog_comments_by_id($data['post']->blog_id);
			$data['blog_comments'] = $this->Blog_model->get_blog_comments_by_id($data['post']->blog_id);
			if($data['blog_comments']) {
				foreach ($data['blog_comments'] as $k => $v) {
					$data['blog_comments'][$k]->text = html_entity_decode($data['blog_comments'][$k]->text);
					$data['blog_comments'][$k]->files = $this->Blog_model->get_comments_files($v->blog_comment_id);
					$data['blog_comments'][$k]->user = $this->ion_auth->user($v->created_id)->row();
					$data['blog_comments'][$k]->user->avatar = $this->Blog_model->get_avatar($data['blog_comments'][$k]->user->avatar_id);
					$blogs = $this->Blog_model->count_blogs_by_user_id($v->created_id);
					$comments = $this->Blog_model->count_blog_comments_by_user_id($v->created_id);
					$data['blog_comments'][$k]->user->comments = $blogs + $comments;
					if($v->created_at != $v->updated_at) {
						$data['blog_comments'][$k]->user->edited = $data['blog_comments'][$k]->user;
						if($v->created_id != $v->updated_id) {
							$data['blog_comments'][$k]->user->edited = $this->ion_auth->user($v->updated_id)->row();
						}
					}
				}
			}
		}
		else {
			redirect('/404');
		}
		$this->load->view('blog/solution_post', $data);
	}
	public function edit_post($id)
	{
		$data = [];
		$data['breadcrumbs'] = 'Редактирование записи';
		$data['site_title'] = 'Редактирование записи';
		$settings = $this->Settings_model->list_all();
		$data['settings'] = build_settings($settings);
		$data['information_pages'] = $this->Pages_model->get_information_pages();
		$categories = $this->Blog_model->get_all_categories();
		$data['categories'] = build_categories($categories);
		if(!$this->ion_auth->logged_in()) {
			$this->load->view('user/not_logged', $data);
			return;
		}
		$data['return_url'] = '/solutions/post/'.$id;
		$post = $this->Blog_model->get($id);
		if($post) {
			$data['post'] = $post[0];
			$data['blog_id'] = $id;
			$allow = 1;
			if($data['post']->created_id == $this->ion_auth->get_user_id()) {
				$allow = 0;
			}
			elseif($this->ion_auth->is_admin()) {
				$allow = 0;
			}
			if($allow) {
				redirect('/');
			}
			$this->load->library('form_validation');
			$this->form_validation->set_rules('title', 'Заголовок - ', 'trim|required');
			$this->form_validation->set_rules('description', 'Описание - ', 'trim|required');
			$this->form_validation->set_rules('category_id1', 'Тема', 'trim|required');
			$this->form_validation->set_rules('owner', 'Это изобретение - Ваше или нет - ', 'trim|required');
			$categories = $this->Blog_model->get_categories_by_post_id($data['post']->blog_id);
			$data['title'] = $data['post']->title;
			$data['description'] = $data['post']->description;
			$data['owner'] = $data['post']->owner;
			$data['category_id1'] = 1;
			$data['category_id2'] = 0;
			$data['category_id3'] = 0;
			if($categories) {
				$i=1;
				foreach($categories as $category) {
					$data['category_id'.$i] = $category->category_id;
					$i++;
				}
			}
			$data['files'] = $this->Blog_model->get_files_by_entity($id);
			$data['files_count'] = 5 - count($data['files']);
			if(isPost()) {
				$data['title'] = htmlentities($this->input->post('title', true));
				$data['description'] = htmlentities($this->input->post('description'));
				$data['category_id1'] = (int)$this->input->post('category_id1', true);
				$data['category_id2'] = (int)$this->input->post('category_id2', true);
				$data['category_id3'] = (int)$this->input->post('category_id3', true);
				$data['owner'] = (int)$this->input->post('owner', true);
				if($this->form_validation->run() == TRUE) {
					$update = [
						'title' => $data['title'],
						'description' => $data['description'],
						'owner' => $data['owner'],
						'moderated' => 0,
						'updated_at' => date("Y-m-d H:i:s"),
						'updated_id' => $this->ion_auth->get_user_id(),
					];
					$edit = $this->Blog_model->update($id, $update);
					if($edit) {
						foreach($categories as $category) {
							$this->Blog_model->remove_blog_from_category($id, $category->category_id);
							$this->Blog_model->reduce_count_category($category->category_id);
						}
						$this->Blog_model->add_blog_to_category($id, $data['category_id1']);
						$this->Blog_model->increase_count_category($data['category_id1']);
						if(isset($data['category_id2']) && $data['category_id2']) {
							$this->Blog_model->add_blog_to_category($id, $data['category_id2']);
							$this->Blog_model->increase_count_category($data['category_id2']);
						}
						if(isset($data['category_id3']) && $data['category_id3']) {
							$this->Blog_model->add_blog_to_category($id, $data['category_id3']);
							$this->Blog_model->increase_count_category($data['category_id3']);
						}
						$files = $this->Blog_model->get_files_by_entity($id);
						$input_files = $this->input->post('files', true);
						if($input_files) {
							$input_files = array_flip($input_files);
						}
						if($files) {
							foreach($files as $file) {
								if(isset($input_files[$file->attached_file_id])) {
									unset($input_files[$file->attached_file_id]);
								}
								else {
									$this->Blog_model->update_file($file->attached_file_id,['deleted' => '1']);
									unset($input_files[$file->attached_file_id]);
								}
							}
						}
						if($input_files) {
							foreach($input_files as $file_id => $v) {
								$file = $this->Blog_model->get_file($file_id);
								$update = [
									'entity_id' => $id,
									'entity' => 'blog',
								];
								$updated = $this->Blog_model->update_file($file_id, $update);
							}
						}
						$this->load->view('blog/edit_ok', $data);
					}
					else {
						$this->load->view('blog/edit_fail', $data);
					}
					return;
				}
			}
		}
		else {
			redirect('/404');
		}
		$this->load->view('blog/solution_edit_post', $data);
	}
	public function edit_comment($id)
	{
		$data = [];
		$data['breadcrumbs'] = 'Редактирование комментария';
		$data['site_title'] = 'Редактирование комментария';
		$settings = $this->Settings_model->list_all();
		$data['settings'] = build_settings($settings);
		$data['information_pages'] = $this->Pages_model->get_information_pages();
		$categories = $this->Blog_model->get_all_categories();
		$data['categories'] = build_categories($categories);
		if(!$this->ion_auth->logged_in()) {
			$this->load->view('user/not_logged', $data);
			return;
		}
		$data['comment'] = $this->Blog_model->get_comment($id);
		$data['return_url'] = '/solutions/post/'.$data['comment']->blog_id;
		if($data['comment']) {
			$allow = 1;
			if($data['comment']->created_id == $this->ion_auth->get_user_id()) {
				$allow = 0;
			}
			elseif($this->ion_auth->is_admin()) {
				$allow = 0;
			}
			if($allow) {
				redirect('/');
			}
			$this->load->library('form_validation');
			$this->form_validation->set_rules('text', 'Текст комментария - ', 'trim|required');
			$data['files'] = $this->Blog_model->get_files_by_entity($id, 'comment');
			$data['files_count'] = 5 - count($data['files']);
			if(isPost()) {
				$data['comment']->text = htmlentities($this->input->post('text', true));
				if($this->form_validation->run() == TRUE) {
					$update = [
						'text' => $data['comment']->text,
						'moderated' => 0,
						'updated_at' => date("Y-m-d H:i:s"),
						'updated_id' => $this->ion_auth->get_user_id(),
					];
					$edit = $this->Blog_model->update_comment($id, $update);
					if($edit) {
						$files = $this->Blog_model->get_files_by_entity($id, 'comment');
						$input_files = $this->input->post('files', true);
						if($input_files) {
							$input_files = array_flip($input_files);
						}
						if($files) {
							foreach($files as $file) {
								if(isset($input_files[$file->attached_file_id])) {
									unset($input_files[$file->attached_file_id]);
								}
								else {
									$this->Blog_model->update_file($file->attached_file_id,['deleted' => '1']);
									unset($input_files[$file->attached_file_id]);
								}
							}
						}
						if($input_files) {
							foreach($input_files as $file_id => $v) {
								$file = $this->Blog_model->get_file($file_id);
								$update = [
									'entity_id' => $id,
									'entity' => 'comment',
								];
								$updated = $this->Blog_model->update_file($file_id, $update);
							}
						}
						$this->load->view('blog/edit_ok', $data);
					}
					else {
						$this->load->view('blog/edit_fail', $data);
					}
					return;
				}
			}
		}
		else {
			redirect('/404');
		}
		$this->load->view('blog/edit_comment', $data);
	}
	public function delete_comment($id)
	{
		if(!$id) {
			redirect('/404');
		}
		$data = [];
		$data['breadcrumbs'] = 'Удаление комментария';
		$data['site_title'] = 'Удаление комментария';
		$settings = $this->Settings_model->list_all();
		$data['settings'] = build_settings($settings);
		$data['information_pages'] = $this->Pages_model->get_information_pages();
		$categories = $this->Blog_model->get_all_categories();
		$data['categories'] = build_categories($categories);
		if(!$this->ion_auth->logged_in()) {
			$this->load->view('user/not_logged', $data);
			return;
		}
		$record = $this->Blog_model->get_comment($id, true);
		if($record) {
			$allow = 1;
			if($record->created_id == $this->ion_auth->get_user_id()) {
				$allow = 0;
			}
			elseif($this->ion_auth->is_admin()) {
				$allow = 0;
			}
			if($allow) {
				redirect('/');
			}
			$this->Blog_model->update_comment($id, ['deleted'=>'1']);
			$this->load->view('blog/delete_ok', $data);
		}
	}
	public function delete_post($id)
	{
		if(!$id) {
			redirect('/404');
		}
		$data = [];
		$data['breadcrumbs'] = 'Удаление сообщения';
		$data['site_title'] = 'Удаление сообщения';
		$settings = $this->Settings_model->list_all();
		$data['settings'] = build_settings($settings);
		$data['information_pages'] = $this->Pages_model->get_information_pages();
		$categories = $this->Blog_model->get_all_categories();
		$data['categories'] = build_categories($categories);
		if(!$this->ion_auth->logged_in()) {
			$this->load->view('user/not_logged', $data);
			return;
		}
		$record = $this->Blog_model->get_blog_posts_by_ids($id, true);
		if($record) {
			$record = $record[0];
			$allow = 1;
			if($record->created_id == $this->ion_auth->get_user_id()) {
				$allow = 0;
			}
			elseif($this->ion_auth->is_admin()) {
				$allow = 0;
			}
			if($allow) {
				redirect('/');
			}
			$categories = $this->Blog_model->get_categories_by_post_id($id);
			if($categories) {
				foreach($categories as $category) {
					$this->Blog_model->remove_blog_from_category($id, $category->category_id);
					$this->Blog_model->reduce_count_category($category->category_id);
				}
			}
			$this->Blog_model->update($id, ['deleted'=>'1']);
			$this->load->view('blog/delete_ok', $data);
		}
	}
}