<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Feedback extends CI_Controller {
	function __construct()
	{
		parent::__construct();
	}
	public function index()
	{
		$data = [];
		$data['breadcrumbs'] = 'Полезные решения';
		$data['site_title'] = 'Полезные решения';
		$settings = $this->Settings_model->list_all();
		$data['settings'] = build_settings($settings);
		$data['information_pages'] = $this->Pages_model->get_information_pages();
		$categories = $this->Blog_model->get_all_categories();
		$data['categories'] = build_categories($categories);
		
		$this->form_validation->set_rules('name', 'Ваше имя', 'trim|required');
		$this->form_validation->set_rules('email', 'Email', 'trim|required');
		$this->form_validation->set_rules('body', 'Текст сообщения', 'trim|required');

		if($this->form_validation->run() === TRUE) {
			//$from = ['robot@xn--h1agq.xn--c1avg', 'Робот сил'];
			$from = "Робот СИЛ;robot@xn--h1agq.xn--c1avg";
			//$from = 'ripkilobyte@gmail.com';
			$to = 'skino@bk.ru';
			//$to = 'ripkilobyte@gmail.com';
			$subject = 'Сообщение от формы обратной связи';
			$message = '';
			$message .= 'Сообщение от:'.$this->input->post('name', true).'<br />';
			$message .= 'От пользователя '.$this->input->post('email', true).'<br />';
			$message .= 'Пишет:<br />'.$this->input->post('body', true);
			$message = "<html><body><div style=\"padding: 10px; margin: 0 -10px 0 -10px;color: #ffffff;background: -moz-linear-gradient(top,	#79b 0%, #357); background: -webkit-gradient(linear, left top, left bottom, from(#79b), to(#357)); background: -o-linear-gradient(top, #79b 0%, #357); filter: progid:DXImageTransform.Microsoft.gradient(startColorstr='#7799bb', endColorstr='#335577'); border: none; text-shadow: 0px -1px 0px rgba(000,000,000,0.7), 0px 1px 0px rgba(255,255,255,0.3);\"></div><div><p>".$message."</p></body></html>";

			/*
			require_once APPPATH.'libraries/libmail.php';
			$m=new Mail();
			$m->To($to);
			$m->From($from);
			$m->Subject($subject);
			$m->log_on(true);
			$m->Body($message, "html");
			$m->Send();
			//*/

			
			$config['charset'] = 'utf-8';
			$config['mailtype'] = 'html';
			$config['wordwrap'] = FALSE;
			$this->email->initialize($config);
			$this->email->from($from);
			$this->email->subject($subject);
			$this->email->message($message); 
			$this->email->to($to);
			//$this->email->send();
			//*/

			//echo "Показывает исходный текст письма:<br><pre>", $m->Get(), "</pre>";
			//var_dump($m);
			$result = $this->email->send();
			
			$insert = [
				'name' => $this->input->post('name', true),
				'email' => $this->input->post('email', true),
				'body' => $this->input->post('body', true),
				'created_at' => date("Y-m-d H:i:s"),
			];
			$result = $this->All_model->save_feedback($insert);
			if($result) {
				$this->load->view('layout/feedback_ok', $data);
			}
			else {
				$this->load->view('layout/error', $data);
			}
		}
		else {
			$data['message'] = (validation_errors() ? validation_errors() : ($this->ion_auth->errors() ? $this->ion_auth->errors() : $this->session->flashdata('message')));
			$data['name'] = [
				'name' => 'name',
				'id' => 'name',
				'class' => 'form-control w100',
				'placeholder' => 'Как к Вам обращаться?',
				'value' => $this->form_validation->set_value('name'),
			];
			$data['email'] = [
				'name' => 'email',
				'id' => 'email',
				'class' => 'form-control w100',
				'placeholder' => 'E-mail',
				'value' => $this->form_validation->set_value('email'),
			];
			$data['body'] = [
				'name' => 'body',
				'id' => 'body',
				'class' => 'form-control w100',
				'rows' => '14',
				'placeholder' => 'Текст сообщения',
				'value' => $this->form_validation->set_value('body'),
			];
		}

		$this->load->view('layout/feedback', $data);
	}
}