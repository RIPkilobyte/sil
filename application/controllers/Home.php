<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Home extends CI_Controller {
	function __construct()
	{
		parent::__construct();
	}
	public function index()
	{
		if($this->uri->segment(1)) {
			redirect('/');
		}
		$data = [];
		$data['breadcrumbs'] = '';
		$page = $this->Pages_model->row(['link' => 'home']);
		$settings = $this->Settings_model->list_all();
		$data['settings'] = build_settings($settings);
		$data['information_pages'] = $this->Pages_model->get_information_pages();
		$categories = $this->Blog_model->get_all_categories();
		$data['categories'] = build_categories($categories);
		//$user = $this->ion_auth->user()->row();
		if($page) {
			$data['site_title'] = html_entity_decode($page->title);
			$data['text_title'] = html_entity_decode($page->text_title);
			$data['content'] = html_entity_decode($page->body);
			$this->load->view('layout/page', $data);
		}
		else {
			redirect('/404');
		}
	}
}