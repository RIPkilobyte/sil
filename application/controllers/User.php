<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class User extends CI_Controller {
	function __construct()
	{
		parent::__construct();
	}
	public function index()
	{
		if(!$this->ion_auth->logged_in()) {
			redirect('auth/login');
		}
		else {
			redirect('/cabinet');
		}
	}
	public function login()
	{
		if($this->ion_auth->logged_in()) {
			redirect('/cabinet');
		}
		$data = [];
		$this->form_validation->set_rules('login', 'Email', 'required');
		$this->form_validation->set_rules('password', 'Пароль', 'required');
		if($this->form_validation->run() === TRUE) {
			$remember = (bool)$this->input->post('remember');
			if ($this->ion_auth->login($this->input->post('login'), $this->input->post('password'), $remember)) {
				redirect('/cabinet');
			}
			else {
				$data['message'] = $this->ion_auth->errors();
			}
		}
		else {
			$data['message'] = validation_errors();
		}
		$data['login'] = $this->form_validation->set_value('login');
		$data['password'] = $this->form_validation->set_value('password');
		$data['breadcrumbs'] = 'Вход';
		$data['site_title'] = 'Вход';
		$settings = $this->Settings_model->list_all();
		$data['settings'] = build_settings($settings);
		$data['information_pages'] = $this->Pages_model->get_information_pages();
		$categories = $this->Blog_model->get_all_categories();
		$data['categories'] = build_categories($categories);
		$this->load->view('user/login', $data);
	}
	public function register()
	{
		$data = [];
		$data['breadcrumbs'] = 'Регистрация';
		$data['site_title'] = 'Регистрация';
		$settings = $this->Settings_model->list_all();
		$data['settings'] = build_settings($settings);
		$data['information_pages'] = $this->Pages_model->get_information_pages();
		$categories = $this->Blog_model->get_all_categories();
		$data['categories'] = build_categories($categories);

		$tables = $this->config->item('tables', 'ion_auth');
		$identity_column = $this->config->item('identity', 'ion_auth');
		$this->data['identity_column'] = $identity_column;
		$this->form_validation->set_rules('first_name', 'Имя', 'trim|required');
		$this->form_validation->set_rules('last_name', 'Фамилия', 'trim|required');
		$this->form_validation->set_rules('second_name', 'Отчество', 'trim');
		$this->form_validation->set_rules('email', 'Email', 'trim|required|valid_email|is_unique[' . $tables['users'] . '.email]');
		$this->form_validation->set_rules('username', 'Отображаемое имя', 'trim|required|is_unique[' . $tables['users'] . '.username]');
		$this->form_validation->set_rules('password', 'Пароль', 'required|min_length[' . $this->config->item('min_password_length', 'ion_auth') . ']|matches[password_confirm]');
		$this->form_validation->set_rules('password_confirm', 'Подтверждение пароля', 'required');

		if ($this->form_validation->run() === TRUE) {
			$email = strtolower($this->input->post('email', true));
			$password = $this->input->post('password');
			$additional_data = [
				'first_name' => $this->input->post('first_name', true),
				'last_name' => $this->input->post('last_name', true),
				'second_name' => $this->input->post('second_name', true),
				'username' => $this->input->post('username', true),
			];
		}
		if ($this->form_validation->run() === TRUE && $this->ion_auth->register($this->input->post('username'), $password, $email, $additional_data)) {
			$this->ion_auth->login($email, $password, 1);
			$this->session->set_flashdata('message', $this->ion_auth->messages());
			redirect('/cabinet');
		}
		else {
			$data['message'] = (validation_errors() ? validation_errors() : ($this->ion_auth->errors() ? $this->ion_auth->errors() : $this->session->flashdata('message')));
			$data['first_name'] = [
				'name' => 'first_name',
				'id' => 'first_name',
				'type' => 'text',
				'class' => 'form-control',
				'placeholder' => 'Имя',
				'value' => $this->form_validation->set_value('first_name'),
			];
			$data['last_name'] = [
				'name' => 'last_name',
				'id' => 'last_name',
				'type' => 'text',
				'class' => 'form-control',
				'placeholder' => 'Фамилия',
				'value' => $this->form_validation->set_value('last_name'),
			];
			$data['second_name'] = [
				'name' => 'second_name',
				'id' => 'second_name',
				'type' => 'text',
				'class' => 'form-control',
				'placeholder' => 'Отчество',
				'value' => $this->form_validation->set_value('second_name'),
			];
			$data['email'] = [
				'name' => 'email',
				'id' => 'email',
				'type' => 'text',
				'class' => 'form-control',
				'placeholder' => 'E-mail',
				'value' => $this->form_validation->set_value('email'),
			];
			$data['username'] = [
				'name' => 'username',
				'id' => 'username',
				'type' => 'text',
				'class' => 'form-control',
				'placeholder' => 'Отображаемое всем имя на сайте',
				'value' => $this->form_validation->set_value('username'),
			];
			$data['password'] = [
				'name' => 'password',
				'id' => 'password',
				'type' => 'password',
				'class' => 'form-control',
				'placeholder' => 'Пароль',
			];
			$data['password_confirm'] = [
				'name' => 'password_confirm',
				'id' => 'password_confirm',
				'type' => 'password',
				'class' => 'form-control',
				'placeholder' => 'Повторите пароль',
			];
		}
		$this->load->view('user/register', $data);
	}
	public function logout()
	{
		$this->ion_auth->logout();
		redirect('/');
	}
	public function forgot_password()
	{
		$data = [];
		$data['site_title'] = 'Восстановление пароля';
		$data['breadcrumbs'] = 'Восстановление пароля';
		$settings = $this->Settings_model->list_all();
		$data['settings'] = build_settings($settings);
		$data['information_pages'] = $this->Pages_model->get_information_pages();
		$categories = $this->Blog_model->get_all_categories();
		$data['categories'] = build_categories($categories);
		$this->form_validation->set_rules('email', 'Email', 'trim|required|valid_email');
		if ($this->form_validation->run() === TRUE) {
			$email = strtolower($this->input->post('email', true));
			$id = $this->ion_auth->get_user_id_from_identity($email);
			if($id) {
				$user = $this->ion_auth->user($id)->row();
				if($user) {
					$forgotten = $this->ion_auth->forgotten_password($user->email);
					$from = "Робот СИЛ;robot@xn--h1agq.xn--c1avg";
					$to = $email;
					$subject = 'Сброс пароля для пользователя';
					$message = 'Сброс пароля для пользователя '.$email.'<br />';
					$message .= 'Нажмите на ссылку для ';
					$message .= '<a href="http://xn--h1agq.xn--c1avg/user/reset_password/'.$forgotten['forgotten_password_code'].'">восстановления пароля</a>';

					$config['charset'] = 'utf-8';
					$config['mailtype'] = 'html';
					$config['wordwrap'] = FALSE;
					$this->email->initialize($config);
					$this->email->from($from);
					$this->email->subject($subject);
					$this->email->message($message); 
					$this->email->to($to);
					$result = $this->email->send();
					if($result) {
						$this->load->view('user/forgot_send', $data);
						return;
					}
					else {
						$this->load->view('user/error', $data);
						return;
					}
				}
			}
		}
		else {
			$data['message'] = validation_errors();
		}
		$this->load->view('user/forgot', $data);
	}
	public function reset_password($code = NULL)
	{
		$data = [];
		$data['site_title'] = 'Восстановление пароля';
		$data['breadcrumbs'] = 'Восстановление пароля';
		$settings = $this->Settings_model->list_all();
		$data['settings'] = build_settings($settings);
		$data['information_pages'] = $this->Pages_model->get_information_pages();
		$categories = $this->Blog_model->get_all_categories();
		$data['categories'] = build_categories($categories);
		$this->form_validation->set_rules('password', 'Пароль', 'required|min_length[' . $this->config->item('min_password_length', 'ion_auth') . ']|matches[password_confirm]');
		$this->form_validation->set_rules('password_confirm', 'Подтверждение пароля', 'required');
		$user = $this->ion_auth->forgotten_password_check($code);
		if ($this->form_validation->run() === TRUE) {
			$change = $this->ion_auth->reset_password($user->email, $this->input->post('password'));
			if($change) {
				$this->load->view('user/reset_ok', $data);
				return;
			}
			else {
				$this->load->view('user/error', $data);
				return;
			}
		}
		else {
			$data['message'] = validation_errors();
		}
		$this->load->view('user/reset_pass', $data);
	}
	public function cabinet()
	{
		if(!$this->ion_auth->logged_in()) {
			redirect('auth/login');
		}
		$data = [];
		$data['breadcrumbs'] = 'Личный кабинет';
		$data['site_title'] = 'Личный кабинет';
		$settings = $this->Settings_model->list_all();
		$data['settings'] = build_settings($settings);
		$data['information_pages'] = $this->Pages_model->get_information_pages();
		$categories = $this->Blog_model->get_all_categories();
		$data['categories'] = build_categories($categories);
		$data['user'] = $this->ion_auth->user()->row();
		$data['posts'] = $this->Blog_model->count_blogs_by_user_id($this->ion_auth->get_user_id());
		$this->load->view('user/cabinet', $data);
	}
	public function my_posts()
	{
		$data = [];
		$data['breadcrumbs'] = 'Мои сообщения';
		$data['site_title'] = 'Мои сообщения';
		$settings = $this->Settings_model->list_all();
		$data['settings'] = build_settings($settings);
		$data['information_pages'] = $this->Pages_model->get_information_pages();
		$categories = $this->Blog_model->get_all_categories();
		$data['categories'] = build_categories($categories);
		if(!$this->ion_auth->logged_in()) {
			$this->load->view('user/not_logged', $data);
			return;
		}
		$this->load->view('user/my_posts', $data);
	}
	public function anketa2()
	{
		if(!$this->ion_auth->logged_in()) {
			redirect('auth/login');
		}
		$data = [];
		$data['breadcrumbs'] = 'Анкета 2';
		$data['site_title'] = 'Анкета 2';
		$settings = $this->Settings_model->list_all();
		$data['settings'] = build_settings($settings);
		$data['information_pages'] = $this->Pages_model->get_information_pages();
		$categories = $this->Blog_model->get_all_categories();
		$data['categories'] = build_categories($categories);
		$data['user'] = $this->ion_auth->user()->row();
		$anketa = $this->Settings_model->get_anketa($this->ion_auth->get_user_id());

		$this->form_validation->set_rules('first_name', 'Имя', 'trim|required');
		$this->form_validation->set_rules('last_name', 'Фамилия', 'trim|required');
		$this->form_validation->set_rules('second_name', 'Отчество', 'trim|required');
		$this->form_validation->set_rules('register_address', 'Адрес регистрации', 'trim|required');
		$this->form_validation->set_rules('fact_address', 'Адрес фактического  проживания (для оперативной доставки корреспонденции)', 'trim|required');
		$this->form_validation->set_rules('inn', 'ИНН', 'trim|required|numeric');
		$this->form_validation->set_rules('phone', 'Контактный телефон', 'trim|required');
		$this->form_validation->set_rules('email', 'Контактный адрес электронной почты', 'trim|required');
		$this->form_validation->set_rules('bank', 'Банковские реквизиты', 'trim|required');

		if ($this->form_validation->run() === TRUE) {
			$array = [
				'user_id' => $this->ion_auth->get_user_id(),
				'first_name' => $this->input->post('first_name'),
				'last_name' => $this->input->post('last_name'),
				'second_name' => $this->input->post('second_name'),
				'register_address' => $this->input->post('register_address'),
				'fact_address' => $this->input->post('fact_address'),
				'inn' => $this->input->post('inn'),
				'phone' => $this->input->post('phone'),
				'email' => $this->input->post('email'),
				'bank' => $this->input->post('bank'),
				'created_at' => date("Y-m-d H:i:s"),
				'created_id' => $this->ion_auth->get_user_id(),
				'updated_at' => date("Y-m-d H:i:s"),
				'updated_id' => $this->ion_auth->get_user_id(),
			];
			if($this->Settings_model->add_anketa($array)) {
				$this->load->view('user/anketa2_ok', $data);
			}
			else {
				$this->load->view('user/anketa2_fail', $data);
			}
		}
		else {
			$data['message'] = (validation_errors() ? validation_errors() : ($this->ion_auth->errors() ? $this->ion_auth->errors() : $this->session->flashdata('message')));
			$first_name = $this->form_validation->set_value('first_name') ? $this->form_validation->set_value('first_name') : $data['user']->first_name;
			$last_name = $this->form_validation->set_value('last_name') ? $this->form_validation->set_value('last_name') : $data['user']->last_name;
			$second_name = $this->form_validation->set_value('second_name') ? $this->form_validation->set_value('second_name') : $data['user']->second_name;
			$register_address = '';
			$fact_address = '';
			$inn = '';
			$phone = $this->form_validation->set_value('phone') ? $this->form_validation->set_value('phone') : $data['user']->phone;
			$email = $this->form_validation->set_value('email') ? $this->form_validation->set_value('email') : $data['user']->email;
			$bank = '';
			if($anketa) {
				$first_name = $anketa->first_name;
				$last_name = $anketa->last_name;
				$second_name = $anketa->second_name;
				$register_address = $anketa->register_address;
				$fact_address = $anketa->fact_address;
				$inn = $anketa->inn;
				$phone = $anketa->phone;
				$email = $anketa->email;
				$bank = $anketa->bank;
			}
			$data['first_name'] = [
				'name' => 'first_name',
				'id' => 'first_name',
				'class' => 'form-control w100',
				'placeholder' => 'Имя',
				'value' => $first_name,
			];
			$data['last_name'] = [
				'name' => 'last_name',
				'id' => 'last_name',
				'class' => 'form-control w100',
				'placeholder' => 'Фамилия',
				'value' => $last_name,
			];
			$data['second_name'] = [
				'name' => 'second_name',
				'id' => 'second_name',
				'class' => 'form-control w100',
				'placeholder' => 'Отчество',
				'value' => $second_name,
			];
			$data['register_address'] = [
				'name' => 'register_address',
				'id' => 'register_address',
				'class' => 'form-control w100',
				'rows' => '4',
				'placeholder' => 'Адрес регистрации',
				'value' => $register_address,
			];
			$data['fact_address'] = [
				'name' => 'fact_address',
				'id' => 'fact_address',
				'class' => 'form-control w100',
				'rows' => '4',
				'placeholder' => 'Адрес фактического  проживания (для оперативной доставки корреспонденции)',
				'value' => $fact_address,
			];
			$data['inn'] = [
				'name' => 'inn',
				'id' => 'inn',
				'class' => 'form-control w100',
				'placeholder' => 'ИНН',
				'value' => $inn,
			];
			$data['phone'] = [
				'name' => 'phone',
				'id' => 'phone',
				'class' => 'form-control w100',
				'placeholder' => 'Контактный телефон',
				'value' => $phone,
			];
			$data['email'] = [
				'name' => 'email',
				'id' => 'email',
				'class' => 'form-control w100',
				'placeholder' => 'Контактный адрес электронной почты',
				'value' => $email,
			];
			$data['bank'] = [
				'name' => 'bank',
				'id' => 'bank',
				'class' => 'form-control w100',
				'rows' => '7',
				'placeholder' => 'Банковские реквизиты',
				'value' => $bank,
			];
		}

		$this->load->view('user/anketa2', $data);
	}
	public function edit()
	{
		if(!$this->ion_auth->logged_in()) {
			redirect('auth/login');
		}
		$data = [];
		$data['breadcrumbs'] = 'Редактирование личной информации';
		$data['site_title'] = 'Редактирование личной информации';
		$settings = $this->Settings_model->list_all();
		$data['settings'] = build_settings($settings);
		$data['information_pages'] = $this->Pages_model->get_information_pages();
		$categories = $this->Blog_model->get_all_categories();
		$data['categories'] = build_categories($categories);
		$update = 0;
		$data['user'] = $this->ion_auth->user()->row();
		if(isPost()) {
			$this->form_validation->set_rules('username', 'Отображаемое имя', 'trim|required');
			$this->form_validation->set_rules('email', 'Email', 'trim|valid_email|required');
			$this->form_validation->set_rules('last_name', 'Фамилия', 'trim');
			if($this->input->post('password', true)) {
				$this->form_validation->set_rules('password', 'Пароль', 'required|min_length['.$this->config->item('min_password_length', 'ion_auth').']|matches[password_confirm]');
				$this->form_validation->set_rules('password_confirm', 'Подтверждение пароля', 'required');
			}
			$data['user']->username = $this->input->post('username', true);
			$data['user']->email = $this->input->post('email', true);
			$data['user']->last_name = $this->input->post('last_name', true);
			$data['user']->first_name = $this->input->post('first_name', true);
			$data['user']->second_name = $this->input->post('second_name', true);
			$data['user']->password = $this->input->post('password', true);
			if($this->form_validation->run() == TRUE) {
				$avatar_id = $this->input->post('avatar_id', true);
				if($avatar_id != $data['user']->avatar_id) {
					$update = [
						'entity_id' => $this->ion_auth->get_user_id(),
						'entity' => 'avatar',
					];
					$res = $this->Blog_model->update_file($data['user']->avatar_id, ['deleted'=>'1']);
					$res = $this->Blog_model->update_file($avatar_id, $update);
				}
				$additional_data = [
					'username' => $data['user']->username,
					'email' => $data['user']->email,
					'last_name' => $data['user']->last_name,
					'first_name' => $data['user']->first_name,
					'second_name' => $data['user']->second_name,
					'active' => $data['user']->active,
					'password' => $data['user']->password,
					'avatar_id' => $avatar_id,
				];
				$this->ion_auth->update($this->ion_auth->get_user_id(), $additional_data);
				$update = 1;
			}
		}
		$data['user'] = $this->ion_auth->user()->row();
		$data['user']->update = $update;
		$data['avatar'] = $this->Blog_model->get_file($data['user']->avatar_id);
		$this->load->view('user/edit', $data);
	}
	public function save_anketa()
	{
		if(!$this->ion_auth->logged_in()) {
			redirect('auth/login');
		}
	}
}