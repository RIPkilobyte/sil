<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Router extends CI_Controller {
	function __construct()
	{
		parent::__construct();
	}
	public function index()
	{
		$data = [];
		$data['breadcrumbs'] = '';
		$segment = $this->uri->segment(1);
		$page = $this->Pages_model->row(['link' => $segment]);
		$settings = $this->Settings_model->list_all();
		$data['settings'] = build_settings($settings);
		$data['information_pages'] = $this->Pages_model->get_information_pages();
		$categories = $this->Blog_model->get_all_categories();
		$data['categories'] = build_categories($categories);
		$data['site_title'] = 'Страница не найдена';
		if($page) {
			$data['site_title'] = html_entity_decode($page->title);
			$data['breadcrumbs'] = html_entity_decode($page->title);
			$data['text_title'] = html_entity_decode($page->text_title);
			$data['content'] = html_entity_decode($page->body);
			$this->load->view('layout/page', $data);
		}
		else {
			$this->load->view('layout/404', $data);
		}
	}
}